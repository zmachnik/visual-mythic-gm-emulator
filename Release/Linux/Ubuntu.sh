#! /bin/bash

CURRENT_DIR=$(pwd)

sudo apt-get update && sudo apt-get install g++ make qt5-default qtbase5-dev qtbase5-private-dev && cd "$(dirname `which $0`)/../../VisualMythicGME" && qmake -makefile && make && echo -ne "\n\nVisual Mythic GME built successfully:\n" && echo -ne "$(pwd)/VisualMythicGME\n\n"

cd "$CURRENT_DIR"

