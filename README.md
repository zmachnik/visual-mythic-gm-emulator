Visual Mythic Game Master Emulator

This is a software tool for the Mythic: Game Master Emulator (+extensions) described in the following books:

- Mythic Game Master Emulator, Tom Pigeon, Word Mill Publishing, 2006
- Mythic Variations, Tom Pigeon, Word Mill Publishing, 2007
- Mythic Variations 2, Tana Pigeon, Word Mill Publishing, 2017
- The Location Crafter, Tana Pigeon, Word Mill Publishing, 2014

For more information on those books please look here: http://www.wordmillgames.com

My intent is to implement the original rules as accurately as possible, while also giving the users a high degree of freedom. 
I hope this application will be a useful utility for roleplaying sessions and story writing.

Full source code for this project is available here: https://bitbucket.org/zmachnik

Zbigniew Machnik
