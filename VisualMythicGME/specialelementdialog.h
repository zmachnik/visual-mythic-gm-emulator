#ifndef SPECIALELEMENTDIALOG_H
#define SPECIALELEMENTDIALOG_H

#include <QDialog>
#include "region.h"

namespace Ui {
class SpecialElementDialog;
}

class SpecialElementDialog : public QDialog {

    Q_OBJECT

public:

    explicit SpecialElementDialog(QWidget *parent, const Region & region, Region::Element & element);
    virtual ~SpecialElementDialog() override;

    QString interpretation() const;
    QString specialType() const;
    QString returnLocation() const;

protected slots:

    virtual void accept() override;

private:

    Ui::SpecialElementDialog *ui;

    const Region & r_region;
    Region m_region;
    Region::Element & r_element;
    Region::Element m_element;
    QString m_interpretation;
    QString m_specialType;
    QString m_returnLocation;

    void rollElement(bool isSecond = false);
    void rollsMade();

private slots:

    void rollSpecialElement();
    void rollAgain();
    void rollSecond();
    void rollLocation();
    void yesMakesSense();
    void makesNoSenseReroll();
    void treatAsExpected();
    void rollDescription();
    void rollAction();
    void checkInput();
    void updateButtons();
};

#endif // SPECIALELEMENTDIALOG_H
