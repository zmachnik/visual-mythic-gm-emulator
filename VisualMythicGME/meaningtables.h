#ifndef MEANINGTABLES_H
#define MEANINGTABLES_H

#include <QString>
#include <vector>

class MeaningTables
{
public:
    MeaningTables() = delete;

    const static std::vector<QString> actionTable1;
    const static std::vector<QString> actionTable2;

    const static std::vector<QString> descriptionTable1;
    const static std::vector<QString> descriptionTable2;
};

#endif // MEANINGTABLES_H
