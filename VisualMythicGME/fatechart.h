#ifndef FATECHART_H
#define FATECHART_H

#include <memory>
#include <map>
#include <set>

#include "randomevent.h"
#include "dice.h"

class FateChart {

public:

    enum class Odds {
        Impossible,
        NoWay,
        VeryUnlikely,
        Unlikely,
        FiftyFifty,
        SomewhatLikely,
        Likely,
        VeryLikely,
        NearSureThing,
        ASureThing,
        HasToBe
    };

    using ChaosFactor = int;

    struct Fate {

        enum class Answer {
            ExceptionalNo,
            No,
            Yes,
            ExceptionalYes
        } answer;

        std::shared_ptr<RandomEvent::Event> event;

        static QString answerText(Answer answer);
    };

    struct Thresholds {
        const int low;
        const int middle;
        const int high;
    };

    static Fate askFateChart(Odds odds = Odds::FiftyFifty, ChaosFactor chaos = 0);

    static int increaseChaosFactor();
    static int decreaseChaosFactor();
    static void setChaosFactor(int chaosFactor);

    static int chaosFactor();

private:

    static ChaosFactor s_chaosFactor;
    static Dice s_d100;
    const static std::map<const Odds, const std::map<const ChaosFactor, const Thresholds>> s_thresholds;
    const static std::set<int> s_diceValuesForRandomEvent;
};

#endif // FATECHART_H
