#include "randomdescriptiondialog.h"
#include "ui_randomdescriptiondialog.h"

#include "randomevent.h"
#include "utils.h"

RandomDescriptionDialog::RandomDescriptionDialog(QWidget *parent, bool adventureActive, bool canCancelAfterRoll) :
    QDialog(parent),
    ui(new Ui::RandomDescriptionDialog),
    m_adventureActive { adventureActive },
    m_canCancelAfterRoll {canCancelAfterRoll} {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    if (!canCancelAfterRoll) setWindowFlags(windowFlags() & ~Qt::WindowCloseButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
}

RandomDescriptionDialog::~RandomDescriptionDialog() {

    delete ui;
}

QString RandomDescriptionDialog::randomDescription() const {

    return m_randomDescription;
}

void RandomDescriptionDialog::accept() {

    if (m_adventureActive) m_randomDescription = "RANDOM DESCRIPTION: [ " + ui->descriptionLE->text() + " ]";

    QDialog::accept();
}

void RandomDescriptionDialog::rollRandomDescription() {

    const auto description { RandomEvent::getDescription() };

    ui->descriptionLE->setText(description.adverb + " : " + description.adjective);

    ui->acceptPB->setEnabled(m_adventureActive);
    ui->closePB->setEnabled(m_canCancelAfterRoll);

    ui->randomEventPB->setEnabled(false);
}
