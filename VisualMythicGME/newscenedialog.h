#ifndef NEWSCENEDIALOG_H
#define NEWSCENEDIALOG_H

#include <QDialog>

namespace Ui {
class NewSceneDialog;
}

class NewSceneDialog : public QDialog {

    Q_OBJECT

public:
    explicit NewSceneDialog(QWidget *parent = nullptr, bool firstScene = false);
    virtual ~NewSceneDialog() override;

    const QString & scene() const;
    QString newSceneEvent() const;

signals:
    void chaosFactorChanged();

protected slots:
    virtual void accept() override;
    virtual void reject() override;

private:

    Ui::NewSceneDialog *ui;
    QString m_scene;
    QString m_newSceneEvent;
    bool m_checkedAgainstChaosFactor = false;
    int m_originalChaosFactor;

private slots:

    void enableNewSceneWidget();
    void checkSceneSetup();
    void rollRandomEvent();
    void askFateChart();
    void detailCheck();
    void RPGSystemCheck();
    void checkAgainstChaosFactor();
    void updateNewSceneEventText();
};

#endif // NEWSCENEDIALOG_H
