#ifndef DETAILCHECKTABLE_H
#define DETAILCHECKTABLE_H

#include <QString>
#include "memory"

#include "dice.h"

class DetailCheckTable {

public:
    DetailCheckTable() = delete;

    struct Result {
        QString tableResult;
        std::shared_ptr<QString> target = nullptr;
        std::pair<QString, QString> meaning = {};
    };

    static Result detailCheckTableResult();

private:

    static Dice s_d10;
};

#endif // DETAILCHECKTABLE_H
