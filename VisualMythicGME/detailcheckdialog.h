#ifndef DETAILCHECKDIALOG_H
#define DETAILCHECKDIALOG_H

#include <QDialog>

#include "detailchecktable.h"
#include "dice.h"

namespace Ui {
class DetailCheckDialog;
}

class DetailCheckDialog : public QDialog {

    Q_OBJECT

public:

    explicit DetailCheckDialog(QWidget *parent = nullptr);
    virtual ~DetailCheckDialog() override;

    const QString & question() const;
    const DetailCheckTable::Result & result() const;

protected slots:

    virtual void accept() override;

private slots:

    void checkQuestion();
    void detailCheck();
    void rollDescription();
    void rollAction();

private:

    Ui::DetailCheckDialog *ui;
    QString m_description;
    QString m_action;
    QString m_question;
    DetailCheckTable::Result m_result;
    Dice m_d100;
};

#endif // DETAILCHECKDIALOG_H
