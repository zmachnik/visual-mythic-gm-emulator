#include "dicedialog.h"
#include "ui_dicedialog.h"

#include "dice.h"
#include "utils.h"

DiceDialog::DiceDialog(QWidget *parent) :
    QDialog { parent },
    ui { new Ui::DiceDialog } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->diceThrowResultsLW);
}

DiceDialog::~DiceDialog() {

    delete ui;
}

void DiceDialog::updateUI() {

    ui->diceLE->clear();

    QString diceSetDescription;

    for (auto const & diceOfOneType : diceSet) {
        auto type { diceOfOneType.first };
        auto number { diceOfOneType.second };

        if (!number) continue;

        if (number > 0 && !diceSetDescription.isEmpty()) diceSetDescription += "+ ";
        else if (number < 0) diceSetDescription += "- ";

        diceSetDescription += "D" + QString::number(type) + "✕" + QString::number(abs(number)) + " ";
    }

    if (!diceSetDescription.isEmpty()) {
        ui->diceLE->setText(diceSetDescription);
        ui->throwDicePB->setEnabled(true);
        ui->resetPB->setEnabled(true);
    } else {
        ui->throwDicePB->setEnabled(false);
        ui->resetPB->setEnabled(false);
    }
}

void DiceDialog::addDice() {

    diceSet[static_cast<unsigned>(ui->diceTypeSB->value())] += ui->diceNumberSB->value();

    updateUI();
}

void DiceDialog::throwDice() {

    int result { 0 };

    for (auto const & diceOfOneType : diceSet) {

        auto type { diceOfOneType.first };
        auto number { diceOfOneType.second };

        Dice dice {static_cast<unsigned>(type), true};

        int sum { 0 };
        for (int i = 0; i < abs(number); ++i) sum += dice.getInt();
        if (number < 0) sum = -sum;

        result += sum;
    }

    ui->diceThrowResultsLW->addItem(ui->diceLE->text() + "» " + QString::number(result));
    ui->diceThrowResultsLW->scrollToBottom();
}

void DiceDialog::reset() {

    diceSet.clear();
    updateUI();
}
