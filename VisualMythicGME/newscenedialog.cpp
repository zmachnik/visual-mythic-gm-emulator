#include "newscenedialog.h"
#include "ui_newscenedialog.h"

#include <QSettings>

#include "randomevent.h"
#include "dice.h"
#include "infodialog.h"
#include "fatechartdialog.h"
#include "detailcheckdialog.h"
#include "rpgsystemcheckdialog.h"
#include "utils.h"

NewSceneDialog::NewSceneDialog(QWidget *parent, bool firstScene) :
    QDialog { parent },
    ui { new Ui::NewSceneDialog },
    m_originalChaosFactor { FateChart::chaosFactor() } {
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->sceneSetupPTE);

    if (firstScene) {
        ui->keepChaosFactorPB->setChecked(true);
        enableNewSceneWidget();
    } else {
        ui->chaosFactorModBG->setExclusive(false);

        ui->decreaseChaosFactorPB->setChecked(false);
        ui->keepChaosFactorPB->setChecked(false);
        ui->increaseChaosFactorPB->setChecked(false);

        ui->chaosFactorModBG->setExclusive(true);
    }

    if (FateChart::chaosFactor() == 9) {
        ui->keepChaosFactorPB->setEnabled(true);
        ui->increaseChaosFactorPB->setEnabled(false);
        ui->decreaseChaosFactorPB->setEnabled(true);
    } else if (FateChart::chaosFactor() == 1) {
        ui->keepChaosFactorPB->setEnabled(true);
        ui->increaseChaosFactorPB->setEnabled(true);
        ui->decreaseChaosFactorPB->setEnabled(false);
    }

    QSettings settings;

    ui->RPGSystemCheckPB->setVisible(settings.value("rpg_check_enabled").toBool());
    ui->detailCheckPB->setVisible(settings.value("detail_check_enabled").toBool());
    // ui->behaviorCheckPB->setVisible(settings.value("behavior_check_enabled").toBool());

    updateNewSceneEventText();
}

NewSceneDialog::~NewSceneDialog() {

    delete ui;
}

const QString & NewSceneDialog::scene() const {

    return m_scene;
}

QString NewSceneDialog::newSceneEvent() const
{
    return m_newSceneEvent;
}

void NewSceneDialog::accept()
{
    m_scene = ui->sceneSetupPTE->toPlainText();

    m_newSceneEvent += "\nSCENE DESCRIPTION: " + m_scene + "\nCHAOS FACTOR: ";
    m_newSceneEvent += QString::number(FateChart::chaosFactor());

    QDialog::accept();
}

void NewSceneDialog::reject()
{
    FateChart::setChaosFactor(m_originalChaosFactor);

    emit chaosFactorChanged();

    QDialog::reject();
}

void NewSceneDialog::enableNewSceneWidget() {

    ui->chaosFactorGB->setEnabled(false);
    ui->randomEventGB->setEnabled(true);
    ui->newSceneW->setEnabled(true);

    if (ui->increaseChaosFactorPB->isChecked()) FateChart::increaseChaosFactor();
    if (ui->decreaseChaosFactorPB->isChecked()) FateChart::decreaseChaosFactor();

    emit chaosFactorChanged();
}

void NewSceneDialog::checkSceneSetup() {

    const QString & sceneSetup = ui->sceneSetupPTE->toPlainText();

    if (m_checkedAgainstChaosFactor) ui->createPB->setDisabled(sceneSetup.isEmpty());
    else ui->checkAgainstChaosFactorPB->setDisabled(sceneSetup.isEmpty());
}

void NewSceneDialog::rollRandomEvent() {

    const RandomEvent::Event event = RandomEvent::getEvent();

    ui->eventFocusLE->setText(event.focusType);
    ui->targetLE->setText(event.focusTarget);
    ui->eventMeaningLE->setText(event.meaningAction + " : " + event.meaningSubject);

    ui->randomEventPB->setEnabled(false);
    ui->sceneSetupPTE->setEnabled(true);

    m_newSceneEvent += "    [ EVENT MEANING » " + ui->eventMeaningLE->text() +
            + " ]\n    [ EVENT FOCUS » " + ui->eventFocusLE->text() + ", TARGET » "
            + ui->targetLE->text() + " ]\n";

}

void NewSceneDialog::askFateChart()
{
    auto fcd { new FateChartDialog };

    connect(fcd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto fate { fcd->fate() };

        m_newSceneEvent += "    FATE CHART QUESTION: \"" + fcd->question()
                + "\" » RESULT: " + FateChart::Fate::answerText(fate.answer) + "\n";

        if (fate.event)
            m_newSceneEvent += "         RANDOM EVENT:\n            [ EVENT MEANING » "
                    + fate.event->meaningAction + " : " + fate.event->meaningSubject
                    +  " ]\n            [ EVENT FOCUS » " + fate.event->focusType + ", TARGET » "
                    + fate.event->focusTarget + " ]\n";
    });

    fcd->exec();
}

void NewSceneDialog::detailCheck()
{
    auto dcd { new DetailCheckDialog };

    connect(dcd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto result { dcd->result() };

        m_newSceneEvent += "    DETAIL CHECK QUESTION: \"" + dcd->question()
                + "\"\n        [ RESULT » " + result.tableResult + " : " + *result.target + " ]\n";
        if (!result.meaning.first.isEmpty())
            m_newSceneEvent += "        [ MEANING: » " + result.meaning.first + " : " + result.meaning.second + " ]\n";
    });

    dcd->exec();
}

void NewSceneDialog::RPGSystemCheck()
{
    auto scd { new RPGSystemCheckDialog };

    connect(scd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        m_newSceneEvent += "    CHECK: "  + scd->check() + " » RESULT: " + scd->result() + "\n";
    });

    scd->exec();
}

void NewSceneDialog::checkAgainstChaosFactor() {

    m_checkedAgainstChaosFactor = true;
    ui->checkAgainstChaosFactorPB->setEnabled(false);

    Dice d10(10, true);
    int diceValue { d10.getInt() };

    QString title;
    QString message;

    if (d10.getInt() <= FateChart::chaosFactor()) {

        m_newSceneEvent += "\n    ORIGINAL SCENE SETUP: \"" + ui->sceneSetupPTE->toPlainText() + "\"\n";

        title = "D10 ROLL: " + QString::number(diceValue);

        if (diceValue % 2) {
            message = "THE SCENE SETUP IS MODIFIED";
        } else {
            message = "AN INTERRUPT SCENE OCCURS";
            ui->eventFocusLE->clear();
            ui->eventMeaningLE->clear();
            ui->targetLE->clear();
            ui->sceneSetupPTE->clear();

            if (ui->randomEventRB->isChecked()) {
                ui->sceneSetupPTE->setEnabled(false);
                ui->randomEventPB->setEnabled(true);
            }
        }

        m_newSceneEvent += "    " + message + ":\n\n";

    } else {
        ui->createPB->setEnabled(true);
        return;
    }

    auto id { new InfoDialog };
    id->setTitle(title);
    id->setMessage(message);
    id->exec();
}

void NewSceneDialog::updateNewSceneEventText()
{

    m_newSceneEvent = "\nNEW SCENE ";
    if (ui->customSceneRB->isChecked()) m_newSceneEvent += "(CUSTOM)\n";
    else m_newSceneEvent += "(RANDOM EVENT)\n";
}

