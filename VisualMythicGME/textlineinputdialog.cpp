#include "textlineinputdialog.h"
#include "ui_textlineinputdialog.h"

#include "utils.h"

TextLineInputDialog::TextLineInputDialog(QWidget *parent, bool allowEmpty) :
    QDialog { parent },
    ui { new Ui::TextLineInputDialog },
    m_allowEmpty { allowEmpty } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);

    checkInput(ui->textLineLE->text());
}

TextLineInputDialog::~TextLineInputDialog() {

    delete ui;
}

void TextLineInputDialog::setTitle(const QString &title) {

#ifdef Q_OS_ANDROID
    ui->titleL->setText(title);
#else
    setWindowTitle(title);
    ui->titleL->setVisible(false);
#endif
}

void TextLineInputDialog::setLabel(const QString &label) {

    ui->labelL->setText(label);
}

void TextLineInputDialog::setDefaultText(const QString &text) {

    ui->textLineLE->setText(text);
}

QString TextLineInputDialog::inputText() const {

    return m_textLine;
}

void TextLineInputDialog::accept() {

    m_textLine = ui->textLineLE->text();
    QDialog::accept();
}

void TextLineInputDialog::checkInput(QString text) {

    ui->acceptPB->setDisabled(!m_allowEmpty && text.isEmpty());
}
