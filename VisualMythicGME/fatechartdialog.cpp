#include "fatechartdialog.h"
#include "ui_fatechartdialog.h"

#include "utils.h"

FateChartDialog::FateChartDialog(QWidget *parent) :
    QDialog { parent },
    ui { new Ui::FateChartDialog } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->questionPTE);
}

FateChartDialog::~FateChartDialog() {

    delete ui;
}

QString FateChartDialog::question() const {

    return m_question;
}

FateChart::Fate FateChartDialog::fate() const {

    return m_fate;
}

void FateChartDialog::accept()
{


    QDialog::accept();
}

void FateChartDialog::askFateChart() {

    m_question = ui->questionPTE->toPlainText();
    m_fate = FateChart::askFateChart(static_cast<FateChart::Odds>(ui->oddsHS->value()));

    ui->resultLE->setText(FateChart::Fate::answerText(m_fate.answer));

    if (m_fate.event) {
        ui->randomEventGB->setEnabled(true);
        ui->eventMeaningLE->setText(m_fate.event->meaningAction + " : " + m_fate.event->meaningSubject);
        ui->eventFocusLE->setText(m_fate.event->focusType);
        ui->targetLE->setText(m_fate.event->focusTarget);
    } else {
        ui->randomEventL->setText(ui->randomEventL->text() + " NONE");
    }

    ui->questionGB->setEnabled(false);

    ui->randomEventL->setEnabled(true);
    ui->resultGB->setEnabled(true);

    ui->acceptPB->setEnabled(true);
}

void FateChartDialog::checkQuestion() {

    ui->askPB->setDisabled(ui->questionPTE->toPlainText().isEmpty());
}

void FateChartDialog::updateOddsDescription(int oddsNumber) {

    QString description;

    auto odds {static_cast<FateChart::Odds>(oddsNumber)};

    switch (odds) {
    case FateChart::Odds::Impossible:
        description = "IMPOSSIBLE";
        break;
    case FateChart::Odds::NoWay:
        description = "NO WAY";
        break;
    case FateChart::Odds::VeryUnlikely:
        description = "VERY UNLIKELY";
        break;
    case FateChart::Odds::Unlikely:
        description = "UNLIKELY";
        break;
    case FateChart::Odds::FiftyFifty:
        description = "50/50";
        break;
    case FateChart::Odds::SomewhatLikely:
        description = "SOMEWHAT LIKELY";
        break;
    case FateChart::Odds::Likely:
        description = "LIKELY";
        break;
    case FateChart::Odds::VeryLikely:
        description = "VERY LIKELY";
        break;
    case FateChart::Odds::NearSureThing:
        description = "NEAR SURE THING";
        break;
    case FateChart::Odds::ASureThing:
        description = "A SURE THING";
        break;
    case FateChart::Odds::HasToBe:
        description = "HAS TO BE";
    }

    ui->oddsDescription->setText(description);
}

