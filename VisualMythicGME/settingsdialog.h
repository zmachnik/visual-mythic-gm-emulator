#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QCheckBox>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog {

    Q_OBJECT

public:

    explicit SettingsDialog(QWidget *parent = nullptr, bool detailCheckEnabled = false,
                            bool behaviorCheckEnabled = false, bool eventCheckEnabled = false,
                            bool statisticCheckEnabled = false, bool RPGCheckEnabled = false,
                            bool offCameraStoriesEnabled = false, bool locationCrafterEnabled = false,
                            bool randomDescriptionEnabled = false, int globalFontSize = 0);

    virtual ~SettingsDialog() override;

    bool detailCheckEnabled() const;
    bool behaviorCheckEnabled() const;
    bool eventCheckEnabled() const;
    bool statisticCheckEnabled() const;
    bool RPGCheckEnabled() const;
    bool offCameraStoriesEnabled() const;
    bool locationCrafterEnabled() const;
    bool randomDescriptionEnabled() const;
    int  globalFontSize() const;

protected slots:

    virtual void accept() override;

private:

    Ui::SettingsDialog *ui;

    bool m_detailCheckEnabled;
    bool m_behaviorCheckEnabled;
    bool m_eventCheckEnabled;
    bool m_statisticCheckEnabled;
    bool m_RPGCheckEnabled;
    bool m_offCameraStoriesEnabled;
    bool m_locationCrafterEnabled;
    bool m_randomDescriptionEnabled;
    int  m_globalFontSize;

    void setBlockedCheckbox(QCheckBox * checkBox);

private slots:

    void changeFontSize();
};

#endif // SETTINGSDIALOG_H
