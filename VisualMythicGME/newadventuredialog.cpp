#include "newadventuredialog.h"
#include "ui_newadventuredialog.h"

#include "utils.h"

NewAdventureDialog::NewAdventureDialog(QWidget *parent) :
    QDialog { parent },
    ui { new Ui::NewAdventureDialog } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);

    {
        // Options disabled until universal solution for all platforms implemented.
        ui->addPlayerCharactersChB->setVisible(false);
        ui->addThreadsChB->setVisible(false);
        ui->addNPCsChB->setVisible(false);
        ui->createFirstSceneChB->setVisible(false);
    }
}

NewAdventureDialog::~NewAdventureDialog() {

    delete ui;
}

const QString &NewAdventureDialog::adventure() const {

    return m_newAdventure;
}

const QString &NewAdventureDialog::RPGSystem() const {

    return m_RPGSystem;
}

bool NewAdventureDialog::createFirstScene() const {

    return m_createFirstScene;
}

bool NewAdventureDialog::addPlayerCharacters() const {

    return m_addPlayers;
}

bool NewAdventureDialog::addNPCs() const {

    return m_addNPCs;
}

bool NewAdventureDialog::addThreads() const {

    return m_addThreads;
}

void NewAdventureDialog::accept() {

    m_newAdventure = ui->adventureNameLE->text();
    m_RPGSystem = ui->RPGSystemLE->text();
    m_createFirstScene = ui->createFirstSceneChB->isChecked();
    m_addPlayers = ui->addPlayerCharactersChB->isChecked();
    m_addNPCs = ui->addNPCsChB->isChecked();
    m_addThreads = ui->addThreadsChB->isChecked();

    QDialog::accept();
}

void NewAdventureDialog::checkInputs() {

    ui->createPB->setDisabled(ui->adventureNameLE->text().isEmpty() || ui->RPGSystemLE->text().isEmpty());
}
