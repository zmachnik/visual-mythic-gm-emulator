#include "newregionelementdialog.h"
#include "ui_newregionelementdialog.h"

#include <QIcon>

#include "utils.h"

NewRegionElementDialog::NewRegionElementDialog(QWidget *parent, const Region::ElementCategory category) :
    QDialog(parent),
    ui(new Ui::NewRegionElementDialog) {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);

    switch (category) {
    case Region::ElementCategory::Location:
        setWindowTitle("NEW LOCATION");
        ui->completeRB->setEnabled(true);
        ui->noneRB->setEnabled(false);
        break;
    case Region::ElementCategory::Encounter:
        setWindowTitle("NEW ENCOUNTER");
        ui->completeRB->setEnabled(false);
        ui->noneRB->setEnabled(true);
        break;
    case Region::ElementCategory::Object:
        setWindowTitle("NEW OBJECT");
        ui->completeRB->setEnabled(false);
        ui->noneRB->setEnabled(true);
        break;

    default:;
    }

    m_element.category = category;
}

NewRegionElementDialog::~NewRegionElementDialog() {

    delete ui;
}

Region::Element NewRegionElementDialog::element() const {

    return m_element;
}

void NewRegionElementDialog::accept() {

    m_element.type = (ui->expectedRB->isChecked() ? Region::ElementType::Expected :
                     (ui->specialRB->isChecked()  ? Region::ElementType::Special  :
                     (ui->randomRB->isChecked()   ? Region::ElementType::Random   :
                     (ui->noneRB->isChecked()     ? Region::ElementType::None     :
                     (ui->completeRB->isChecked() ? Region::ElementType::Complete :
                                                    Region::ElementType::Custom   )))));

    if (m_element.type == Region::ElementType::Custom) {
        m_element.name = ui->customLE->text();
        m_element.unique = ui->uniqueCB->isChecked();
    }

    QDialog::accept();
}

void NewRegionElementDialog::updateUI() {

    if (ui->customRB->isChecked()) {

        ui->customLE->setEnabled(true);
        ui->uniqueCB->setEnabled(true);

        ui->addPB->setEnabled(!ui->customLE->text().isEmpty());

    } else {

        ui->customLE->setEnabled(false);
        ui->uniqueCB->setEnabled(false);
        ui->addPB->setEnabled(true);
    }
}
