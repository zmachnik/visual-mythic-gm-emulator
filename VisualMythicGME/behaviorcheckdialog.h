#ifndef BEHAVIORCHECKDIALOG_H
#define BEHAVIORCHECKDIALOG_H

#include <QDialog>
#include <memory>
#include "npcdescriptors.h"

namespace Ui {
class BehaviorCheckDialog;
}

class BehaviorCheckDialog : public QDialog {

    Q_OBJECT

public:

    explicit BehaviorCheckDialog(QWidget *parent = nullptr,
                                 bool descriptorsEdited = false,
                                 QString currentNPC = {});
    virtual ~BehaviorCheckDialog() override;

    bool NPCDetailsChangeRequested() const;
    QString currentNPC() const;
    QString behavior() const;

protected slots:

    virtual void accept() override;
    virtual void reject() override;

private:

    Ui::BehaviorCheckDialog *ui;
    bool m_NPCDetailsChangeRequested { false };
    QString m_currentNPC;
    QString m_behavior;
    std::shared_ptr<NPCDescriptors> m_modifiedDescriptors { nullptr };

    void setupCharacterList();
    void setDispositionBar(const QString & NPC);

private slots:

    void loadNPCDetails(const QString & NPC);
    void newInteraction();
    void continueInteraction();
    void updateDisposition();
    void checkBehavior();
    void changeNPCDetails();
    void rollDescription();
    void rollDescriptionForDescriptor();
    void rollAction();
    void rollActionForDescriptor();
    void randomCharacter();
    void checkInterpretation();

};

#endif // BEHAVIORCHECKDIALOG_H
