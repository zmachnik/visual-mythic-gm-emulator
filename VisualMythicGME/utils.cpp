#include "utils.h"

#include <QApplication>
#include <QMessageBox>
#include <QIcon>
#include <QAbstractButton>
#include <QScroller>
#include <QFile>
#include <QTextStream>
#include <QStandardPaths>

#include <algorithm>
#include <iterator>
#include "lists.h"

const QString Utils::version { "1.1.0" };
Dice Utils::s_d100{100, true};
QSettings Utils::settings{ QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) +"/vmgme_settings.ini", QSettings::IniFormat };
QApplication * Utils::application{nullptr};

void Utils::maximizeOnPhone([[maybe_unused]] QDialog * window) {
#ifdef Q_OS_ANDROID
    window->setWindowState(Qt::WindowMaximized);
#endif
}

void Utils::loadApplicationStylesheet() {

    QFile stylesheetFile { ":visual/stylesheet.qss" };
    if (stylesheetFile.open(QFile::ReadOnly | QFile::Text)) {

        QString styleSheet;

        int globalFontSize { Utils::settings.value("global_font_size").toInt() };
        if (globalFontSize) {
            styleSheet += "QWidget { font-size: ";
            styleSheet += QString::number(globalFontSize);
            styleSheet += "px; }\n";
        }

        QTextStream ts { &stylesheetFile };
        styleSheet += ts.readAll();
        application->setStyleSheet(styleSheet);
    }
}

void Utils::scrollWithSwipe(QAbstractScrollArea *scrollArea)
{
#ifdef Q_OS_ANDROID
    QScroller::grabGesture(scrollArea->viewport(), QScroller::TouchGesture);
#else
    QScroller::grabGesture(scrollArea->viewport(), QScroller::LeftMouseButtonGesture);
#endif

    const auto overshootPolicy { QVariant::fromValue<QScrollerProperties::OvershootPolicy>(QScrollerProperties::OvershootAlwaysOff) };
    auto scrollerProperties { QScroller::scroller(scrollArea->viewport())->scrollerProperties() };
    scrollerProperties.setScrollMetric(QScrollerProperties::VerticalOvershootPolicy, overshootPolicy);
    scrollerProperties.setScrollMetric(QScrollerProperties::HorizontalOvershootPolicy, overshootPolicy);
    QScroller::scroller(scrollArea->viewport())->setScrollerProperties(scrollerProperties);
}

void Utils::waitUntilTrue(bool & flag) {

    do QCoreApplication::processEvents();
    while (!flag);
}

QString Utils::randomElementInSet(const std::set<QString> & set) {

    QString element;

    const auto setSize { set.size() };

    if (setSize > 0) {
        Dice d{static_cast<unsigned>(setSize), false};
        auto position { set.begin() };
        std::advance(position, d.getInt());
        element = *position;
    }

    return element;
}

void Utils::removeFromSet(std::set<QString> & set1, const std::set<QString> & set2) {

    std::set<QString> result;

    std::set_difference(set1.begin(), set1.end(), set2.begin(), set2.end(),
                        std::inserter(result, result.end()));

    set1 = std::move(result);
}

QString Utils::randomNPC() {

    auto NPC { randomElementInSet(Lists::NPCs()) };
    if (NPC.isEmpty()) NPC =  randomElementInSet(Lists::allPersonalNPCs());
    if (NPC.isEmpty()) NPC = "<NO NPCS AVAILABLE>";

    return NPC;
}

QString Utils::randomThread() {

    auto thread { randomElementInSet(Lists::threads()) };
    if (thread.isEmpty()) thread = randomElementInSet(Lists::allPersonalThreads());
    if (thread.isEmpty()) thread = "<NO THREADS AVAILABLE>";

    return thread;
}

QString Utils::randomPlayerCharacter() {

    auto playerCharacter { randomElementInSet(Lists::playerCharacters()) };
    if (playerCharacter.isEmpty()) playerCharacter = "<NO PLAYER CHARACTERS AVAILABLE>";

    return playerCharacter;
}

void Utils::endAllNPCActions() {

    for (const auto & NPC : Lists::NPCs()) {
        auto descriptors { Lists::descriptors(NPC) };
        if (descriptors) {
            descriptors->setAction("");
            descriptors->setInteractionOngoing(false);
            descriptors->setActiveIdentity(0);
            descriptors->setActivePersonality(0);
            descriptors->setActiveActivity(0);
        }
    }
}

void Utils::showErrorMessage(QString errorMessage) {

    auto msgBox { new QMessageBox };

    msgBox->setAttribute(Qt::WA_DeleteOnClose, true);
    msgBox->setModal(true);
    msgBox->setWindowFlags(msgBox->windowFlags() | Qt::WindowMinimizeButtonHint);
    msgBox->setWindowIcon(QIcon(":visual/icon.ico"));

    QFont bold;
    bold.setBold(true);
#ifndef Q_OS_ANDROID
    bold.setPointSize(10);
#endif

    QFont normal;
#ifndef Q_OS_ANDROID
    normal.setPointSize(10);
#endif

    msgBox->setFont(bold);

#ifdef Q_OS_ANDROID
    errorMessage = "ERROR\n\n" + errorMessage;
#else
    msgBox->setWindowTitle("ERROR");
#endif

    msgBox->setText(errorMessage + "\n");

    msgBox->addButton("OK", QMessageBox::YesRole);

    msgBox->buttons()[0]->setFont(bold);

    msgBox->exec();
}

void Utils::showYesNoQuestionMessage(QString question, const QString & title,
                                     std::function<void(bool)> action) {

    auto msgBox { new QMessageBox };
    msgBox->setAttribute(Qt::WA_DeleteOnClose, true);
    msgBox->setModal(true);
    msgBox->setWindowFlags(msgBox->windowFlags() | Qt::WindowMinimizeButtonHint);
    msgBox->setWindowIcon(QIcon(":visual/icon.ico"));

    QFont bold;
    bold.setBold(true);
    bold.setPointSize(11);

    QFont normal;
    normal.setPointSize(11);

    msgBox->setFont(bold);

#ifdef Q_OS_ANDROID
    question = title + "\n\n" + question;
#else
    msgBox->setWindowTitle(title);
#endif
    msgBox->setText(question + "\n");

    msgBox->addButton("YES", QMessageBox::YesRole);
    msgBox->addButton("NO", QMessageBox::NoRole);

    msgBox->buttons()[0]->setFont(bold);
    msgBox->buttons()[1]->setFont(normal);

    connect(msgBox, &QDialog::finished, [&](int dc) {

        if (action) {
            if (dc == 0) return action(true);
            if (dc == 1) return action(false);
        }
    });

    msgBox->exec();
}

