#ifndef DICE_H
#define DICE_H

#include <random>

class Dice {

public:

    Dice(unsigned sides = 6, bool oneBased = false);
    Dice(int sides = 6, bool oneBased = false);
    unsigned get();
    int getInt();

private:

    std::default_random_engine m_dre;
    std::uniform_int_distribution<unsigned> m_uid;
};

#endif // DICE_H
