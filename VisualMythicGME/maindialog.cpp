#include "maindialog.h"
#include "ui_maindialog.h"

#include <QFileDialog>
#include <QJsonArray>

#include <array>

#include "settingsdialog.h"
#include "newadventuredialog.h"
#include "fatechartdialog.h"
#include "newscenedialog.h"
#include "textlineinputdialog.h"
#include "infodialog.h"
#include "rpgsystemcheckdialog.h"
#include "backstorydialog.h"
#include "tweenstorydialog.h"
#include "dicedialog.h"
#include "randomdescriptiondialog.h"
#include "randomeventdialog.h"
#include "detailcheckdialog.h"
#include "behaviorcheckdialog.h"
#include "statisticcheckdialog.h"
#include "eventcheckdialog.h"
#include "newregionelementdialog.h"
#include "newregionscenedialog.h"
#include "lists.h"
#include "utils.h"

MainDialog::MainDialog(QWidget *parent) :
    QDialog { parent },
    ui { new Ui::MainDialog } {

    ui->setupUi(this);

#ifdef Q_OS_WASM
    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint);
    ui->quitPB->setVisible(false);
    showFullScreen();
#else
    setWindowFlags(windowFlags() | Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint);
#endif
    setWindowIcon(QIcon(":visual/icon.ico"));
    setWindowTitle("VISUAL MYTHIC GM EMULATOR " + Utils::version);

#ifdef Q_OS_ANDROID
    ui->quitPB->setVisible(false);
#endif

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->scenesLW);
    Utils::scrollWithSwipe(ui->storyPTE);
    Utils::scrollWithSwipe(ui->NPCsLW);
    Utils::scrollWithSwipe(ui->threadsLW);
    Utils::scrollWithSwipe(ui->playerCharactersLW);
    Utils::scrollWithSwipe(ui->personalNPCsLW);
    Utils::scrollWithSwipe(ui->personalThreadsLW);
    Utils::scrollWithSwipe(ui->backstoryPTE);
    Utils::scrollWithSwipe(ui->behaviorSA);
    Utils::scrollWithSwipe(ui->regionsLW);
    Utils::scrollWithSwipe(ui->locationsLW);
    Utils::scrollWithSwipe(ui->encountersLW);
    Utils::scrollWithSwipe(ui->objectsLW);
    Utils::scrollWithSwipe(ui->regionSA);
    Utils::scrollWithSwipe(ui->regionSceneSA);
    Utils::scrollWithSwipe(ui->mapTW);

    setAdventureActive(false);

    hideAndShowFeatures();

    //ui->chaosFactorPrB->setTextVisible(true);
    //ui->chaosFactorPrB->setTextDirection(QProgressBar::BottomToTop);

    ui->backToBehaviorCheckW->setVisible(false);
    ui->dispositionHS->setEnabled(false);

    connect(ui->themePB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::Theme); });
    connect(ui->actionPB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::Action); });
    connect(ui->priIdentityPB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::PrimaryIdentity); });
    connect(ui->secIdentityPB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::SecondaryIdentity); });
    connect(ui->priPersonalityPB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::PrimaryPersonality); });
    connect(ui->secPersonalityPB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::SecondaryPersonality); });
    connect(ui->priActivityPB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::PrimaryActivity); });
    connect(ui->secActivityPB, &QPushButton::clicked, [this]{ setNPCDescriptor(NPCDescriptor::SecondaryActivity); });

    connect(ui->priIdentityActiveCB, &QCheckBox::clicked, [this]{ setNPCDescriptorActive(NPCDescriptor::PrimaryIdentity); });
    connect(ui->secIdentityActiveCB, &QCheckBox::clicked, [this]{ setNPCDescriptorActive(NPCDescriptor::SecondaryIdentity); });
    connect(ui->priPersonalityActiveCB, &QCheckBox::clicked, [this]{ setNPCDescriptorActive(NPCDescriptor::PrimaryPersonality); });
    connect(ui->secPersonalityActiveCB, &QCheckBox::clicked, [this]{ setNPCDescriptorActive(NPCDescriptor::SecondaryPersonality); });
    connect(ui->priActivityActiveCB, &QCheckBox::clicked, [this]{ setNPCDescriptorActive(NPCDescriptor::PrimaryActivity); });
    connect(ui->secActivityActiveCB, &QCheckBox::clicked, [this]{ setNPCDescriptorActive(NPCDescriptor::SecondaryActivity); });

    connect(ui->addLocationPB, &QPushButton::clicked, [this] { addRegionElement(Region::ElementCategory::Location); });
    connect(ui->addEncounterPB, &QPushButton::clicked, [this] { addRegionElement(Region::ElementCategory::Encounter); });
    connect(ui->addObjectPB, &QPushButton::clicked, [this] { addRegionElement(Region::ElementCategory::Object); });

    connect(ui->locationUpPB, &QPushButton::clicked, [this] { moveRegionElement(Region::ElementCategory::Location, true); });
    connect(ui->locationDownPB, &QPushButton::clicked, [this] { moveRegionElement(Region::ElementCategory::Location, false); });
    connect(ui->encounterUpPB, &QPushButton::clicked, [this] { moveRegionElement(Region::ElementCategory::Encounter, true); });
    connect(ui->encounterDownPB, &QPushButton::clicked, [this] { moveRegionElement(Region::ElementCategory::Encounter, false); });
    connect(ui->objectUpPB, &QPushButton::clicked, [this] { moveRegionElement(Region::ElementCategory::Object, true); });
    connect(ui->objectDownPB, &QPushButton::clicked, [this] { moveRegionElement(Region::ElementCategory::Object, false); });

    connect(ui->removeLocationPB, &QPushButton::clicked, [this] { removeRegionElement(Region::ElementCategory::Location); });
    connect(ui->removeEncounterPB, &QPushButton::clicked, [this] { removeRegionElement(Region::ElementCategory::Encounter); });
    connect(ui->removeObjectPB, &QPushButton::clicked, [this] { removeRegionElement(Region::ElementCategory::Object); });

    ui->mainTW->setCurrentIndex(0);
    ui->scenesAndStoryTB->setCurrentIndex(0);
    ui->peopleAndThreadsTB->setCurrentIndex(2);
    ui->locationsTB->setCurrentIndex(0);

    ui->mapTW->setCornerButtonEnabled(false);
    ui->mapTW->horizontalHeader()->setHighlightSections(false);
    ui->mapTW->verticalHeader()->setHighlightSections(false);

    ui->storyPTE->setWindowFlags(Qt::SubWindow);
    ui->storyEntryGB->setWindowFlags(Qt::SubWindow);

    ui->toolsAndMenusTB->setCurrentIndex(0);

    //(new QGridLayout(ui->storyPTE))->addWidget(new QSizeGrip(ui->storyPTE), 0,0,1,1,Qt::AlignBottom);
    //(new QGridLayout(ui->storyEntryGB))->addWidget(new QSizeGrip(ui->storyEntryGB), 0,0,1,1,Qt::AlignTop);
}

MainDialog::~MainDialog() {

    delete ui;
}

void MainDialog::closeEvent(QCloseEvent * event) {

    Utils::showYesNoQuestionMessage("QUIT VISUAL MYTHIC GAME MASTER EMULATOR?", "MYTHIC VISUAL GME",
            [=](bool yes) {
        if (yes) event->accept();
        else event->ignore();
    });
}

void MainDialog::keyPressEvent(QKeyEvent * event) {

    if (event->key() != Qt::Key_Escape) QDialog::keyPressEvent(event);
    else setWindowState(Qt::WindowMinimized);
}

void MainDialog::writeSave(QJsonObject & adventure) {

    adventure["name"] = ui->adventureNameLE->text();
    adventure["RPGSystem"] = ui->RPGSystemLE->text();
    adventure["chaosFactor"] = ui->chaosFactorPrB->value();
    adventure["story"] = ui->storyPTE->toPlainText();

    QJsonArray scenes;

    for (int i=0; i<ui->scenesLW->count(); ++i)
        scenes.append(ui->scenesLW->item(i)->text());

    adventure["scenes"] = scenes;

    QJsonArray NPCs;

    //for (const auto & NPC : Lists::NPCs()) NPCs.append(NPC);

    for (const auto & NPCName : Lists::NPCs()) {

        QJsonObject NPC;
        NPC["name"] = NPCName;
        NPC["details"] = "no";

        NPCDescriptors * descriptors { Lists::descriptors(NPCName) };

        if (descriptors) {

            NPC["details"] = "yes";
            NPC["theme"] = descriptors->theme();
            NPC["action"] = descriptors->action();
            NPC["interactionOngoing"] = descriptors->interactionOngoing();
            NPC["disposition"] = descriptors->dispositionBaseValue();
            NPC["primaryIdentity"] = descriptors->identity().primary;
            NPC["secondaryIdentity"] = descriptors->identity().secondary;
            NPC["activeIdentity"] = descriptors->identity().active;
            NPC["identityDispositionInc"] = descriptors->identityIncDisp();
            NPC["primaryPersonality"] = descriptors->personality().primary;
            NPC["secondaryPersonality"] = descriptors->personality().secondary;
            NPC["personalityDispositionInc"] = descriptors->personalityIncDisp();
            NPC["activePersonality"] = descriptors->personality().active;
            NPC["primaryActivity"] = descriptors->activity().primary;
            NPC["secondaryActivity"] = descriptors->activity().secondary;
            NPC["activeActivity"] = descriptors->activity().active;
            NPC["activityDispositionInc"] = descriptors->activityIncDisp();
        }

        NPCs.append(NPC);
    }

    adventure["NPCs"] = NPCs;

    QJsonArray threads;

    for (const auto & thread : Lists::threads()) threads.append(thread);

    adventure["threads"] = threads;

    QJsonArray playerCharacters;

    for (const auto & playerCharacterName : Lists::playerCharacters()) {

        QJsonObject player;
        player["name"] = playerCharacterName;

        QJsonArray playerNPCs;

        for (const auto & personalNPC : *(Lists::personalNPCs(playerCharacterName)))
            playerNPCs.append(personalNPC);

        player["personalNPCs"] = playerNPCs;

        QJsonArray playerThreads;

        for (const auto & personalThread : *(Lists::personalThreads(playerCharacterName)))
            playerThreads.append(personalThread);

        player["personalThreads"] = playerThreads;

        player["backstory"] = Lists::PCBackstory(playerCharacterName);

        playerCharacters.append(player);
    }

    adventure["playerCharacters"] = playerCharacters;


    QJsonArray regions;

    for (const auto & r : Lists::regions()) {

        QJsonObject region;
        region["name"] = r.name();

        auto writeElement { [](const auto & e, auto & element) {

                element["name"] = e.name;
                element["category"] = Region::ElementCategoryText(e.category);
                element["type"] = Region::ElementTypeText(e.type);
                element["isUnique"] = e.unique ? "yes" : "no";
        } };

        auto writeElements { [&writeElement](const auto & ev, auto & array) {

             for (auto e : ev) {

                QJsonObject element;
                writeElement(e, element);
                array.append(element);
             }
        } };

        QJsonArray locations;
        writeElements(r.locations(), locations);
        region["locations"] = locations;

        QJsonArray encounters;
        writeElements(r.encounters(), encounters);
        region["encounters"] = encounters;

        QJsonArray objects;
        writeElements(r.objects(), objects);
        region["objects"] = objects;

        region["locationProgressPoints"] = static_cast<int>(r.locationPPs());
        region["encounterProgressPoints"] = static_cast<int>(r.encounterPPs());
        region["objectProgressPoints"] = static_cast<int>(r.objectPPs());

        QJsonObject regionScene;

        if (r.constScene().active) {

            regionScene["active"] = "yes";

            QJsonObject location;
            writeElement(r.constScene().location, location);
            regionScene["location"] = location;

            QJsonObject encounter;
            writeElement(r.constScene().encounter, encounter);
            regionScene["encounter"] = encounter;

            QJsonObject object;
            writeElement(r.constScene().object, object);
            regionScene["object"] = object;

            regionScene["locationExactType"] = r.constScene().locationExactType;
            regionScene["encounterExactType"] = r.constScene().encounterExactType;
            regionScene["objectExactType"] = r.constScene().objectExactType;

        } else {

            regionScene["active"] = "no";
        }

        region["scene"] = regionScene;

        QJsonArray mapConnections;

        for (const auto & mapConnection : r.regionMap()) {

            QJsonObject mc;

            mc["location"] = mapConnection.first;
            mc["connection"] = mapConnection.second;

            mapConnections.append(mc);
        }

        region["map"] = mapConnections;

        region["currentMapLocation"] = r.currentMapLocation();

        regions.append(region);
    }

    adventure["regions"] = regions;
}

void MainDialog::loadSave(const QJsonObject & adventure) {

    clearAll();
    setAdventureActive(false);

    ui->adventureNameLE->setText(adventure["name"].toString());
    ui->RPGSystemLE->setText(adventure["RPGSystem"].toString());

    FateChart::setChaosFactor(adventure["chaosFactor"].toInt());
    updateChaosFactorDisplay();

    ui->storyPTE->setPlainText(adventure["story"].toString());

    for (QJsonValueRef scene : adventure["scenes"].toArray())
        ui->scenesLW->addItem(scene.toString());

    for (QJsonValueRef NPCValueRef : adventure["NPCs"].toArray()) {

        const auto NPC { NPCValueRef.toObject() };

        auto name { NPC["name"].toString() };

        ui->NPCsLW->addItem(name);
        Lists::addNPC(name);

        auto details { NPC["details"].toString() };

        if (details == "yes") {

            NPCDescriptors descriptors;

            descriptors.setTheme(NPC["theme"].toString());
            descriptors.setAction(NPC["action"].toString());
            descriptors.setInteractionOngoing(NPC["interactionOngoing"].toBool());
            descriptors.setDispositionBaseValue(NPC["disposition"].toInt());

            descriptors.setIdentity(
                        NPCDescriptors::Descriptor{
                            NPC["primaryIdentity"].toString(),
                            NPC["secondaryIdentity"].toString(),
                            NPC["activeIdentity"].toInt()} );

            descriptors.setPersonality(
                        NPCDescriptors::Descriptor{
                            NPC["primaryPersonality"].toString(),
                            NPC["secondaryPersonality"].toString(),
                            NPC["activePersonality"].toInt()} );

            descriptors.setActivity(
                        NPCDescriptors::Descriptor{
                            NPC["primaryActivity"].toString(),
                            NPC["secondaryActivity"].toString(),
                            NPC["activeActivity"].toInt()} );

            descriptors.setIdentityIncDisp(NPC["identityDispositionInc"].toBool());
            descriptors.setPersonalityIncDisp(NPC["personalityDispositionInc"].toBool());
            descriptors.setActivityIncDisp(NPC["activityDispositionInc"].toBool());

            Lists::setNPCDescriptors(name, descriptors);
        }
    }

    for (QJsonValueRef thread : adventure["threads"].toArray()) {
        ui->threadsLW->addItem(thread.toString());
        Lists::addThread(thread.toString());
    }

    for (QJsonValueRef playerValueRef : adventure["playerCharacters"].toArray()) {

        const auto playerCharacter { playerValueRef.toObject() };

        auto name { playerCharacter["name"].toString() };
        ui->playerCharactersLW->addItem(name);
        Lists::addPlayerCharacter(name);

        for (QJsonValueRef personalNPC : playerCharacter["personalNPCs"].toArray() )
            Lists::addPersonalNPC(name, personalNPC.toString());

        for (QJsonValueRef personalThread : playerCharacter["personalThreads"].toArray())
            Lists::addPersonalThread(name, personalThread.toString());

        auto backstory { playerCharacter["backstory"].toString() };
        Lists::setPCBackstory(name, backstory);
    }

    for (QJsonValueRef regionValueRef : adventure["regions"].toArray()) {

        Region region;

        const auto regionObject { regionValueRef.toObject() };
        region.setName(regionObject["name"].toString());

        auto loadElement { [](const auto & value, auto & element) {

            const auto object { value.toObject() };

            element.name = object["name"].toString();
            element.category = Region::parseElementCategory(object["category"].toString());
            element.type = Region::parseElementType(object["type"].toString());
            element.unique = object["isUnique"].toString() == "yes";
        } };

        auto loadElements { [&loadElement, &region](const auto & elements) {

             Region::Element element;
             for (auto locationValue : elements) {
                    loadElement(locationValue, element);
                    region.addElement(element);
                }
        } };

        loadElements(regionObject["locations"].toArray());
        loadElements(regionObject["encounters"].toArray());
        loadElements(regionObject["objects"].toArray());

        region.setLocationPPs(regionObject["locationProgressPoints"].toInt());
        region.setEncounterPPs(regionObject["encounterProgressPoints"].toInt());
        region.setObjectPPs(regionObject["objectProgressPoints"].toInt());

        auto regionSceneObject { regionObject["scene"].toObject()};

        Region::Scene & scene { region.scene() };

        if (regionSceneObject["active"].toString() == "yes") {

            scene.active = true;

            loadElement(regionSceneObject["location"], scene.location);
            loadElement(regionSceneObject["encounter"], scene.encounter);
            loadElement(regionSceneObject["object"], scene.object);

            scene.locationExactType = regionSceneObject["locationExactType"].toString();
            scene.encounterExactType = regionSceneObject["encounterExactType"].toString();
            scene.objectExactType = regionSceneObject["objectExactType"].toString();

        } else {
            scene.active = false;
        }

        for (QJsonValueRef mapConnection : regionObject["map"].toArray()) {

            auto mapItem { mapConnection.toObject() };

            region.addMapConnection(mapItem["location"].toString(), mapItem["connection"].toString());
        }

        region.setCurrentMapLocation(regionObject["currentMapLocation"].toString());

        ui->regionsLW->addItem(region.name());
        Lists::insertRegion(std::move(region));
    }

    setAdventureActive(true);
}

void MainDialog::appendStory(const QString &text) {

    ui->storyPTE->appendPlainText(text);

    ui->storyPTE->moveCursor (QTextCursor::End) ;
    ui->storyPTE->ensureCursorVisible() ;
}

void MainDialog::clearAll() {

    ui->adventureNameLE->clear();
    ui->chaosFactorPrB->setValue(5);
    ui->scenesLW->clear();
    ui->storyPTE->clear();
    ui->NPCsLW->clear();
    ui->threadsLW->clear();
    ui->playerCharactersLW->clear();
    ui->personalNPCsLW->clear();
    ui->personalThreadsLW->clear();

    ui->regionsLW->clear();
    ui->mapTW->setRowCount(0);
    ui->mapTW->setColumnCount(0);

    Lists::clear();
}

QString MainDialog::replaceEmpty(QString str) {

    if (str.isEmpty()) return m_buttonEmptyLabel;
    return str;
}

QListWidget * MainDialog::elementWidget(Region::ElementCategory category) {

    return (category == Region::ElementCategory::Location ? ui->locationsLW :
           (category == Region::ElementCategory::Encounter ? ui->encountersLW : ui->objectsLW));
}

void MainDialog::setAdventureActive(bool active) {

    ui->renameAdventurePB->setEnabled(active);
    ui->changeRPGSystemPB->setEnabled(active);
    ui->exportStoryTemplatePB->setEnabled(active);
    ui->chaosFactorL->setVisible(active);
    ui->chaosFactorPrB->setVisible(active);
    ui->scenesAndStoryTB->setEnabled(active);
    ui->adventureL->setEnabled(active);
    ui->adventureNameLE->setEnabled(active);
    ui->nextScenePB->setEnabled(active);
    ui->chaosFactorL->setEnabled(active);
    ui->chaosFactorPrB->setEnabled(active);
    ui->toolsPage->setEnabled(true);
    ui->peopleAndThreadsTB->setEnabled(active);
    ui->savePB->setEnabled(active);
    ui->locationsTB->setEnabled(active);
}

void MainDialog::loadAdventure() {

#ifndef Q_OS_WASM
    auto fd { new QFileDialog };
    fd->setAttribute(Qt::WA_DeleteOnClose, true);
    fd->setModal(true);
    fd->setWindowTitle("LOAD ADVENTURE");
    fd->setNameFilter("JSON FILE (*.json)");
    fd->setFileMode(QFileDialog::ExistingFile);
    fd->setAcceptMode(QFileDialog::QFileDialog::AcceptOpen);
    fd->setViewMode(QFileDialog::List);
    fd->setOption(QFileDialog::DontUseNativeDialog);
#ifdef Q_OS_ANDROID
    fd.setWindowState(fd.windowState() | Qt::WindowMaximized);
#endif

    connect(fd, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        QString fileName = fd->selectedFiles()[0];

        QFile loadFile { fileName };

        if (!loadFile.open(QIODevice::ReadOnly)) return;

        auto saveData { loadFile.readAll() };

        QJsonParseError parseError;
        m_adventureSave = QJsonDocument::fromJson(saveData, &parseError);
        if (parseError.error != QJsonParseError::NoError) {
            Utils::showErrorMessage(parseError.errorString());
            return;
        }

        loadSave(m_adventureSave.object());
    });

    fd->exec();

#else
    auto fileContentReady { [&](const QString & fileName, const QByteArray & fileContent) {
        if (!fileName.isEmpty() && !fileContent.isEmpty()) {
            m_adventureSave = QJsonDocument::fromJson(fileContent);
            loadSave(m_adventureSave.object());
        }
    }};

    QFileDialog::getOpenFileContent("JSON FILE (*.json)",  fileContentReady);
#endif
}

void MainDialog::saveAdventure() {

    QJsonObject adventureObject;
    writeSave(adventureObject);

    m_adventureSave = QJsonDocument { adventureObject };

#ifndef Q_OS_WASM

    auto fd { new QFileDialog };
    fd->setAttribute(Qt::WA_DeleteOnClose, true);
    fd->setModal(true);
    fd->setWindowTitle("SAVE ADVENTURE");
    fd->setNameFilter("JSON FILE (*.json)");
    fd->setFileMode(QFileDialog::AnyFile);
    fd->setAcceptMode(QFileDialog::QFileDialog::AcceptSave);
    fd->setViewMode(QFileDialog::List);
    fd->setOption(QFileDialog::DontUseNativeDialog);
#ifdef Q_OS_ANDROID
    fd.setWindowState(fd.windowState() | Qt::WindowMaximized);
#endif

    connect(fd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        QString fileName = fd->selectedFiles()[0];

        if (!fileName.endsWith(".json", Qt::CaseInsensitive)) fileName += ".json";

        QFile saveFile(fileName);

        if (!saveFile.open(QIODevice::WriteOnly)) return;

        saveFile.write(m_adventureSave.toJson());
    });

    fd->exec();

#else
    QFileDialog::saveFileContent(m_adventureSave.toJson(), ui->adventureNameLE->text() + ".json");
#endif
}

void MainDialog::changeRPGSystem() {

    const auto system { ui->RPGSystemLE->text() };

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("CHANGE RPG SYSTEM");
    tlid->setLabel("RPG SYSTEM:");
    tlid->setDefaultText(system);

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        ui->RPGSystemLE->setText(tlid->inputText());
        appendStory("\nCHANGING RPG SYSTEM FROM " + system + " TO " + tlid->inputText() + "\n");
    });

    tlid->exec();
}

void MainDialog::exportStoryTemplate() {

    m_adventureTextExport = ui->adventureNameLE->text() + "\n" + ui->RPGSystemLE->text()
                            + "\n\n" + ui->storyPTE->toPlainText();

#ifndef Q_OS_WASM

    auto fd { new QFileDialog };
    fd->setAttribute(Qt::WA_DeleteOnClose, true);
    fd->setModal(true);
    fd->setWindowTitle("EXPORT STORY TEMPLATE");
    fd->setNameFilter("TEXT FILE (*.txt)");
    fd->setFileMode(QFileDialog::AnyFile);
    fd->setAcceptMode(QFileDialog::QFileDialog::AcceptSave);
    fd->setViewMode(QFileDialog::List);
    fd->setOption(QFileDialog::DontUseNativeDialog);
#ifdef Q_OS_ANDROID
    fd.setWindowState(fd.windowState() | Qt::WindowMaximized);
#endif

    connect(fd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        QString fileName = fd->selectedFiles()[0];

        if (!fileName.endsWith(".txt", Qt::CaseInsensitive)) fileName += ".txt";

        QFile saveFile(fileName);

        if (!saveFile.open(QIODevice::WriteOnly)) return;

        saveFile.write(m_adventureTextExport.toUtf8());
    });

    fd->exec();
#else
    QFileDialog::saveFileContent(m_adventureTextExport.toUtf8(), ui->adventureNameLE->text() + ".txt");
#endif
}

void MainDialog::showSettings() {

    auto sd { new SettingsDialog {nullptr,
                      Utils::settings.value("detail_check_enabled", true).toBool(),
                      Utils::settings.value("behavior_check_enabled", true).toBool(),
                      Utils::settings.value("event_check_enabled", true).toBool(),
                      Utils::settings.value("statistic_check_enabled", true).toBool(),
                      Utils::settings.value("rpg_check_enabled", true).toBool(),
                      Utils::settings.value("off_camera_stories_enabled", true).toBool(),
                      Utils::settings.value("location_crafter_enabled", true).toBool(),
                      Utils::settings.value("random_description_enabled", true).toBool(),
                      Utils::settings.value("global_font_size").toInt()} };

    connect(sd, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        Utils::settings.setValue("detail_check_enabled", sd->detailCheckEnabled());
        Utils::settings.setValue("behavior_check_enabled", sd->behaviorCheckEnabled());
        Utils::settings.setValue("event_check_enabled", sd->eventCheckEnabled());
        Utils::settings.setValue("statistic_check_enabled", sd->statisticCheckEnabled());
        Utils::settings.setValue("rpg_check_enabled", sd->RPGCheckEnabled());
        Utils::settings.setValue("off_camera_stories_enabled", sd->offCameraStoriesEnabled());
        Utils::settings.setValue("location_crafter_enabled", sd->locationCrafterEnabled());
        Utils::settings.setValue("random_description_enabled", sd->randomDescriptionEnabled());
        Utils::settings.setValue("global_font_size", sd->globalFontSize());

        Utils::settings.sync();

        hideAndShowFeatures();

        Utils::loadApplicationStylesheet();
    });

    sd->exec();
}

void MainDialog::newAdventure() {

    auto nad { new NewAdventureDialog };

    connect(nad, &QDialog::finished, this, [=](int dc) {
        if (dc == QDialog::Rejected || !nad->adventure().size()) return;

        clearAll();

        setAdventureActive(true);
        ui->adventureNameLE->setText(nad->adventure());
        ui->RPGSystemLE->setText(nad->RPGSystem());
        appendStory("ADVENTURE: " + nad->adventure() + " (" + nad->RPGSystem() + ")\n");

        /*
        Options disabled until universal solution for all platforms implemented.

        if (nad->addNPCs()) while (addNPC());

        if (nad->addThreads()) while (addThread());

        if (nad->addPlayerCharacters()) {
            while (addPlayerCharacter()) {
                while (addPersonalNPC());
                while (addPersonalThread());
            }
        }

        if (nad->createFirstScene()) nextScene();
        */
    });

    nad->exec();
}

void MainDialog::renameAdventure() {

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("RENAME ADVENTURE");
    tlid->setLabel("ADVENTURE:");
    tlid->setDefaultText(ui->adventureNameLE->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        ui->adventureNameLE->setText(tlid->inputText());
        appendStory("\nRENAMING ADVENTURE FROM " + ui->adventureNameLE->text() + " TO " + tlid->inputText() + "\n");
    });

    tlid->exec();
}

void MainDialog::nextScene() {

    auto nsd { new NewSceneDialog { nullptr, !ui->scenesLW->count() } };

    connect(nsd, &NewSceneDialog::chaosFactorChanged, this, &MainDialog::updateChaosFactorDisplay);

    connect(nsd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected || nsd->scene().isEmpty()) return;

        Utils::endAllNPCActions();
        ui->scenesLW->addItem(nsd->scene());

        ui->chaosFactorPrB->setValue(FateChart::chaosFactor());
        appendStory(nsd->newSceneEvent() + "\n");
    });

    nsd->exec();
}

void MainDialog::renameScene() {

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("RENAME SCENE");
    tlid->setLabel("SCENE DESCRIPTION:");
    tlid->setDefaultText(ui->scenesLW->currentItem()->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        ui->scenesLW->currentItem()->setText(tlid->inputText());
        appendStory("\nRENAMING SCENE " + ui->scenesLW->currentItem()->text() + " TO " + tlid->inputText() +"\n");
    });

    tlid->exec();
}

void MainDialog::askFateChart() {

    auto fcd { new FateChartDialog };

    connect(fcd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto fate { fcd->fate() };

        appendStory("FATE CHART QUESTION: \"" + fcd->question() + "\" » RESULT: " + FateChart::Fate::answerText(fate.answer));

        if (fate.event)
            appendStory("RANDOM EVENT:\n    [ EVENT MEANING » " + fate.event->meaningAction + " : " + fate.event->meaningSubject
                    +  " ]\n    [ EVENT FOCUS » " + fate.event->focusType + ", TARGET » " + fate.event->focusTarget + " ]");

        appendStory("\n");
    });

    fcd->exec();
}

void MainDialog::detailCheck() {

    auto dcd = new DetailCheckDialog;

    connect(dcd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto result { dcd->result() };

        appendStory("DETAIL CHECK QUESTION: \"" + dcd->question() + "\"\n» RESULT: [ "
                                           + result.tableResult + " : " + *result.target + " ]" );
        if (!result.meaning.first.isEmpty())
            appendStory("» MEANING: [ " + result.meaning.first + " : " + result.meaning.second + " ]\n");
    });

    dcd->exec();
}

void MainDialog::behaviorCheck(bool descriptorsEdited) {

    auto NPC { ui->NPCCB->currentText() };

    if (NPC.isEmpty()) {

        const auto & NPCs { Lists::NPCs() };
        if (NPCs.empty()) {
            Utils::showErrorMessage("NO NPC FOR BEHAVIOR CHECK!");
            return;
        } else {
            NPC = *NPCs.begin();
        }
    }

    auto bcd { new BehaviorCheckDialog { nullptr, descriptorsEdited, NPC } };

    connect(bcd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        if (bcd->NPCDetailsChangeRequested()) {
            ui->mainTW->setCurrentIndex(2);
            ui->NPCCB->setCurrentText(bcd->currentNPC());
            ui->NPCCB->setEnabled(false);
            ui->backToBehaviorCheckW->setVisible(true);
        } else {
            appendStory(bcd->behavior() + "\n");
        }
    });

    bcd->exec();
}

void MainDialog::statisticCheck() {

    auto scd { new StatisticCheckDialog };

    connect(scd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        appendStory(scd->statistic() + "\n");
    });

    scd->exec();
}

void MainDialog::eventCheck() {

    auto evd { new EventCheckDialog };

    connect(evd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto event { evd->randomEvent() };

        appendStory("EVENT CHECK QUESTION: \"" + evd->question()
                                           + "\"\n» RESULT:\n    [ EVENT MEANING » "
                                           + event.meaningAction + " : " + event.meaningSubject
                                           +  " ]\n    [ EVENT FOCUS » " + event.focusType
                                           + ", TARGET » " + event.focusTarget + " ]\n");
    });

    evd->exec();
}

void MainDialog::RPGSystemCheck() {

    auto scd { new RPGSystemCheckDialog };

    connect(scd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        appendStory("CHECK: " + scd->check() + " » RESULT: " + scd->result() + "\n");
    });

    scd->exec();
}

void MainDialog::createTweenStory() {

    auto tsd { new TweenStoryDialog };

    if (Lists::playerCharacters().empty())
        Utils::showErrorMessage("THERE ARE NO CHARACTERS TO CREATE TWEEN STORIES FOR!");
    else {

        connect(tsd, &QDialog::finished, this, [=](int dc) {

            if (dc == QDialog::Rejected) return;

            appendStory(tsd->tweenStory() + "\n");
        });

        tsd->exec();
    }
}

void MainDialog::generateCharacterBackstory() {

    auto bd { new BackstoryDialog { nullptr, ui->playerCharactersLW->currentItem()->text() } };

    connect(bd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        updatePersonalNPCsAndThreads(ui->playerCharactersLW->currentItem()->text());
        ui->backstoryPTE->appendPlainText("\n" + bd->backstory());
    });

    bd->exec();
}

void MainDialog::saveCharacterBackstory() {

    Lists::setPCBackstory(ui->playerCharactersLW->currentItem()->text(), ui->backstoryPTE->toPlainText());
}

void MainDialog::randomDescription()
{
    auto rdd { new RandomDescriptionDialog { nullptr, ui->adventureL->isEnabled() } };

    connect(rdd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        appendStory(rdd->randomDescription() + "\n");
    });

    rdd->exec();
}

void MainDialog::randomEvent() {

    auto red { new RandomEventDialog { nullptr, ui->adventureL->isEnabled() } };

    connect(red, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        appendStory(red->randomEvent() + "\n");
    });

    red->exec();
}

void MainDialog::dice() {

    auto dd { new DiceDialog };
    dd->exec();
}

void MainDialog::quit() {

    close();
}

void MainDialog::addNPC() {

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("NEW NPC");
    tlid->setLabel("NPC:");

    connect(tlid, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        else if (Lists::addNPC(tlid->inputText())) {
            ui->NPCsLW->addItem(tlid->inputText());
            appendStory("+ NPC: " + tlid->inputText() + "\n");
            updateRemoveButtons();
        }
    });

    tlid->exec();
}

void MainDialog::renameNPC() {

    if (ui->NPCsLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("RENAME NPC");
    tlid->setLabel("NPC:");
    tlid->setDefaultText(ui->NPCsLW->currentItem()->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto NPC { ui->NPCsLW->currentItem()->text() };
        const auto & newName { tlid->inputText() };
        if (Lists::renameNPC(NPC, newName)) {
            delete ui->NPCsLW->takeItem(ui->NPCsLW->currentRow());
            ui->NPCsLW->addItem(newName);
            appendStory("RENAMING NPC " + NPC + " TO " + newName + "\n");
            updateRemoveButtons();
            if (ui->playerCharactersLW->currentRow() >= 0) updatePersonalNPCsAndThreads(ui->playerCharactersLW->currentItem()->text());
        } else {
            Utils::showErrorMessage("NPC " + NPC + " CANNOT BE RENAMED TO " + tlid->inputText());
        }
    });

    tlid->exec();
}

void MainDialog::removeNPC() {

    if (ui->NPCsLW->currentRow() < 0) return;

    auto NPC { ui->NPCsLW->currentItem()->text() };
    if (Lists::removeNPC(NPC)) {
        delete ui->NPCsLW->takeItem(ui->NPCsLW->currentRow());
        appendStory("- NPC: " + NPC + "\n");
        updateRemoveButtons();
    }
}

void MainDialog::addThread() {

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("NEW THREAD");
    tlid->setLabel("THREAD:");

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        if (Lists::addThread(tlid->inputText())) {
            ui->threadsLW->addItem(tlid->inputText());
            appendStory("+ THREAD: " + tlid->inputText() + "\n");
            updateRemoveButtons();
        }
    });

    tlid->exec();
}

void MainDialog::renameThread() {

    if (ui->threadsLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("RENAME THREAD");
    tlid->setLabel("THREAD:");
    tlid->setDefaultText(ui->threadsLW->currentItem()->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto thread { ui->threadsLW->currentItem()->text() };
        const auto & newName { tlid->inputText() };
        if (Lists::renameThread(thread, newName)) {
            delete ui->threadsLW->takeItem(ui->threadsLW->currentRow());
            ui->threadsLW->addItem(newName);
            appendStory("RENAMING THREAD " + thread + " TO " + newName + "\n");
            updateRemoveButtons();
            if (ui->playerCharactersLW->currentRow() >= 0) updatePersonalNPCsAndThreads(ui->playerCharactersLW->currentItem()->text());
        } else {
            Utils::showErrorMessage("THREAD " + thread + " CANNOT BE RENAMED TO " + tlid->inputText());
        }
    });

    tlid->exec();
}

void MainDialog::removeThread() {

    if (ui->threadsLW->currentRow() < 0) return;

    auto thread { ui->threadsLW->currentItem()->text() };
    if (Lists::removeThread(thread)) {
        delete ui->threadsLW->takeItem(ui->threadsLW->currentRow());
        appendStory("- THREAD: " + thread + "\n");
        updateRemoveButtons();
    }
}

void MainDialog::addPlayerCharacter() {

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("NEW PLAYER CHARACTER");
    tlid->setLabel("PLAYER CHARACTER:");

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        if (Lists::addPlayerCharacter(tlid->inputText())) {
            ui->playerCharactersLW->addItem(tlid->inputText());
            ui->playerCharactersLW->setCurrentRow(ui->playerCharactersLW->count() - 1);
            appendStory("+ PC: " + tlid->inputText() + "\n");
            updateRemoveButtons();
        }
    });

    tlid->exec();
}

void MainDialog::renamePlayerCharacter() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("RENAME PLAYER CHARACTER");
    tlid->setLabel("PLAYER CHARACTER:");
    tlid->setDefaultText(ui->playerCharactersLW->currentItem()->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto playerCharacter { ui->playerCharactersLW->currentItem()->text() };
        const auto & newName { tlid->inputText() };
        if (Lists::renamePlayerCharacter(playerCharacter, newName)) {
            delete ui->playerCharactersLW->takeItem(ui->playerCharactersLW->currentRow());
            ui->playerCharactersLW->addItem(newName);
            appendStory("RENAMING PLAYER CHARACTER " + playerCharacter + " TO " + newName + "\n");
            updatePersonalNPCsAndThreads(newName);
        } else {
            Utils::showErrorMessage("PLAYER CHARACTER " + playerCharacter + " CANNOT BE RENAMED TO " + tlid->inputText());
        }
    });

    tlid->exec();
}

void MainDialog::removePlayerCharacter() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto playerCharacter { ui->playerCharactersLW->currentItem()->text() };
    if (Lists::removePlayerCharacter(ui->playerCharactersLW->currentItem()->text())) {
        delete ui->playerCharactersLW->takeItem(ui->playerCharactersLW->currentRow());
        appendStory("- PC: " + playerCharacter + "\n");
        updateRemoveButtons();
    }
}

void MainDialog::addPersonalNPC() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("NEW NPC FOR " + ui->playerCharactersLW->currentItem()->text());
    tlid->setLabel("NPC:");

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto playerCharacter { ui->playerCharactersLW->currentItem()->text() };

        if (Lists::addPersonalNPC(playerCharacter, tlid->inputText())) {
            ui->personalNPCsLW->addItem(tlid->inputText());
            appendStory("+ NPC: " + tlid->inputText() + " FOR: " + playerCharacter + "\n");
            updateRemoveButtons();
        }
    });

    tlid->exec();
}

void MainDialog::renamePersonalNPC() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("RENAME NPC FOR " + ui->playerCharactersLW->currentItem()->text());
    tlid->setLabel("NPC:");
    tlid->setDefaultText(ui->personalNPCsLW->currentItem()->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto NPC { ui->personalNPCsLW->currentItem()->text() };
        const auto & newName { tlid->inputText() };
        if (Lists::renameNPC(NPC, newName)) {
            delete ui->personalNPCsLW->takeItem(ui->personalNPCsLW->currentRow());
            ui->personalNPCsLW->addItem(newName);
            auto adventureNPC { ui->NPCsLW->findItems(NPC, Qt::MatchFlag::MatchExactly) };
            if (adventureNPC.size()) {
                delete ui->NPCsLW->takeItem(ui->NPCsLW->row(adventureNPC[0]));
                ui->NPCsLW->addItem(newName);
            }
            appendStory("RENAMING NPC " + NPC + " TO " + newName + "\n");
            updateRemoveButtons();
        } else {
            Utils::showErrorMessage("NPC " + NPC + " CANNOT BE RENAMED TO " + tlid->inputText());
        }
    });

    tlid->exec();
}

void MainDialog::removePersonalNPC() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto playerCharacter { ui->playerCharactersLW->currentItem()->text() };
    auto NPC { ui->personalNPCsLW->currentItem()->text() };

    if (Lists::removePersonalNPC(playerCharacter, NPC)) {
        delete ui->personalNPCsLW->takeItem(ui->personalNPCsLW->currentRow());
        appendStory("- NPC: " + NPC + " FOR: " + playerCharacter + "\n");
        updateRemoveButtons();
    }
}

void MainDialog::addPersonalThread() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("NEW THREAD FOR " + ui->playerCharactersLW->currentItem()->text());
    tlid->setLabel("THREAD:");

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto playerCharacter { ui->playerCharactersLW->currentItem()->text() };

        if (Lists::addPersonalThread(playerCharacter, tlid->inputText())) {
            ui->personalThreadsLW->addItem(tlid->inputText());
            appendStory("+ THREAD: " + tlid->inputText() + " FOR: " + playerCharacter + "\n");
            updateRemoveButtons();
        }
    });

    tlid->exec();
}

void MainDialog::renamePersonalThread() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };
    tlid->setTitle("RENAME PERSONAL THREAD FOR " + ui->playerCharactersLW->currentItem()->text());
    tlid->setLabel("THREAD:");
    tlid->setDefaultText(ui->personalThreadsLW->currentItem()->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto thread { ui->personalThreadsLW->currentItem()->text() };

        const auto & newName { tlid->inputText() };
        if (Lists::renameThread(thread, newName)) {
            delete ui->personalThreadsLW->takeItem(ui->personalThreadsLW->currentRow());
            ui->personalThreadsLW->addItem(newName);
            auto adventureThread { ui->threadsLW->findItems(thread, Qt::MatchFlag::MatchExactly) };
            if (adventureThread.size()) {
                delete ui->threadsLW->takeItem(ui->threadsLW->row(adventureThread[0]));
                ui->threadsLW->addItem(newName);
            }
            appendStory("RENAMING THREAD " + thread + " TO " + newName + "\n");
            updateRemoveButtons();
        } else {
            Utils::showErrorMessage("THREAD " + thread + " CANNOT BE RENAMED TO " + tlid->inputText());
        }
    });

    tlid->exec();
}

void MainDialog::removePersonalThread() {

    if (ui->playerCharactersLW->currentRow() < 0) return;

    auto playerCharacter { ui->playerCharactersLW->currentItem()->text() };
    auto thread { ui->personalThreadsLW->currentItem()->text() };

    if (Lists::removePersonalThread(playerCharacter, thread)) {
        delete ui->personalThreadsLW->takeItem(ui->personalThreadsLW->currentRow());
        appendStory("- THREAD: " + thread + " FOR: " + playerCharacter + "\n");
        updateRemoveButtons();
    }
}

void MainDialog::addRegion() {

    auto tlid { new TextLineInputDialog };

    tlid->setTitle("NEW REGION");
    tlid->setLabel("REGION:");

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        if (Lists::addRegion(tlid->inputText())) {
            updateRegions();
            ui->regionsLW->setCurrentRow(ui->regionsLW->count() - 1);
            appendStory("+ REGION: " + tlid->inputText() + "\n");
        } else {
            Utils::showErrorMessage("COULD NOT ADD REGION " + tlid->inputText());
        }
    });

    tlid->exec();
}

void MainDialog::renameRegion() {

    if (ui->regionsLW->currentRow() < 0) return;

    auto tlid { new TextLineInputDialog };

    tlid->setTitle("RENAME REGION");
    tlid->setLabel("REGION:");
    tlid->setDefaultText(ui->regionsLW->currentItem()->text());

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        const auto & region { ui->regionsLW->currentItem()->text() };

        if (Lists::renameRegion(region, tlid->inputText())) {
            updateRegions();
            ui->regionsLW->setCurrentRow(ui->regionsLW->currentRow());
            appendStory("RENAMING REGION " + region + " TO " + tlid->inputText() + "\n");
        }
        else
            Utils::showErrorMessage("REGION " + region + " CANNOT BE RENAMED TO " + tlid->inputText());
    });

    tlid->exec();
}

void MainDialog::removeRegion() {

    if (ui->regionsLW->currentRow() < 0) return;

    auto region { ui->regionsLW->currentItem()->text() };
    const int index { ui->regionsLW->currentRow() };

    if (Lists::removeRegion(region)) {
        delete ui->regionsLW->takeItem(index);
        if (ui->regionsLW->count()) ui->regionsLW->setCurrentRow(index-1);
        appendStory("- REGION: " + region + "\n");
    }
}

void MainDialog::newRegionScene() {

    Region * region { Lists::region(ui->regionsLW->currentItem()->text()) };
    if (!region) return;

    const auto & locations {region->locations()};
    if (locations.size() < 6) return; // Extra check, but this should never happen.
    QString msg;
    for (unsigned i {0}; i < 6 ; ++i) {
        if (locations[i].type == Region::ElementType::Complete) {
            msg += "WRONG REGION SETUP: A \"COMPLETE\" ELEMENT IS PRESENT WITHIN THE FIRST 6 LOCATIONS OF REGION " + region->name() +".\n";
            break;
        }
    }

    if (ui->locationTypeLE->text() == Region::ElementTypeText(Region::ElementType::Complete)) {
        msg += "FINAL REGION SCENE: THE CURRENT REGION SCENE HAS A \"COMPLETE\" LOCATION.\n";
    }

    if (msg.isEmpty())
        msg = "CREATE A NEW REGION SCENE?";

    Utils::showYesNoQuestionMessage(msg, "", [=](bool yes) {
        if (!yes) return;

        auto nrsd { new NewRegionSceneDialog {nullptr, std::array<const QListWidget *, 3> {ui->locationsLW, ui->encountersLW, ui->objectsLW}, *region } };

        connect(nrsd, &QDialog::finished, this, [=](int dc) {

            if (dc == QDialog::Rejected) return;

            updateElements();

            appendStory("NEXT REGION SCENE IN " + region->name() + ":\n" +
                "    LOCATION: [ " + ui->locationTypeLE->text() + " » " + ui->locationLE->text() +
                " ]\n    ENCOUNTER: [ " + ui->encounterTypeLE->text() + " » " + ui->encounterLE->text() +
                " ]\n    OBJECT: [ " + ui->objectTypeLE->text() + " » " + ui->objectLE->text() + " ]\n");
        });

        nrsd->exec();
    });
}

void MainDialog::leaveRegion() {

    ui->regionsLW->setCurrentRow(-1);
    updateRegionSceneUI();
}

void MainDialog::updateRegionButtons() {

    if (ui->regionsLW->currentRow() >= 0) {

        ui->renameRegionPB->setEnabled(true);
        ui->removeRegionPB->setEnabled(true);
        ui->locationsTB->setItemText(1, "REGION: " + ui->regionsLW->currentItem()->text());

        ui->addLocationPB->setEnabled(true);
        ui->addEncounterPB->setEnabled(true);
        ui->addObjectPB->setEnabled(true);

    } else {

        ui->renameRegionPB->setEnabled(false);
        ui->removeRegionPB->setEnabled(false);
        ui->locationsTB->setItemText(1, "REGION: NONE");

        ui->addLocationPB->setEnabled(false);
        ui->addEncounterPB->setEnabled(false);
        ui->addObjectPB->setEnabled(false);
    }

    updateElements();
}

void MainDialog::updateRegionElementUI() {

    int locationSelected { ui->locationsLW->currentRow()};
    int encounterSelected { ui->encountersLW->currentRow()};
    int objectSelected { ui->objectsLW->currentRow()};

    ui->removeLocationPB->setEnabled(locationSelected >= 0);
    ui->locationUpPB->setEnabled(locationSelected > 0 );
    ui->locationDownPB->setEnabled(locationSelected >= 0 && locationSelected < ui->locationsLW->count()-1);

    ui->removeEncounterPB->setEnabled(encounterSelected >= 0);
    ui->encounterUpPB->setEnabled(encounterSelected > 0 );
    ui->encounterDownPB->setEnabled(encounterSelected >= 0 && encounterSelected < ui->encountersLW->count()-1);

    ui->removeObjectPB->setEnabled(objectSelected >= 0);
    ui->objectUpPB->setEnabled(objectSelected > 0 );
    ui->objectDownPB->setEnabled(objectSelected >= 0 && objectSelected < ui->objectsLW->count()-1);
}

void MainDialog::updateRegionSceneUI() {

    Region * region { nullptr };

    auto selectedRegion { ui->regionsLW->currentItem() };
    if (selectedRegion) region = Lists::region(selectedRegion->text());

    if (!region) {

        ui->locationPPsLCD->display(0);
        ui->encounterPPsLCD->display(0);
        ui->objectPPsLCD->display(0);

        ui->createRegionScenePB->setEnabled(false);
        ui->currentRegionSceneP->setEnabled(false);

        ui->locationTypeLE->clear();
        ui->locationLE->clear();
        ui->encounterTypeLE->clear();
        ui->encounterLE->clear();
        ui->objectTypeLE->clear();
        ui->objectLE->clear();

        clearCurrentSceneUI();

        return;
    }

    ui->locationPPsLCD->display(static_cast<int>(region->locationPPs()));
    ui->encounterPPsLCD->display(static_cast<int>(region->encounterPPs()));
    ui->objectPPsLCD->display(static_cast<int>(region->objectPPs()));

    auto ne { [](const auto * lw) { return lw->count() > 0; } };

    ui->createRegionScenePB->setEnabled(ui->locationsLW->count() >= 6 && ui->encountersLW->count() >= 6 && ui->objectsLW->count() >= 6);
    ui->currentRegionSceneP->setEnabled((ui->regionsLW->currentRow() >= 0) && ne(ui->locationsLW) && ne(ui->encountersLW) && ne(ui->objectsLW));

    ui->regionSceneElementsGB->setEnabled(true);
    ui->locationL->setEnabled(true);
    ui->encounterL->setEnabled(true);
    ui->objectL->setEnabled(true);
    ui->locationTypeLE->setEnabled(true);
    ui->encounterTypeLE->setEnabled(true);
    ui->objectTypeLE->setEnabled(true);
    ui->colon1L->setEnabled(true);
    ui->colon2L->setEnabled(true);
    ui->colon3L->setEnabled(true);
    ui->locationLE->setEnabled(true);
    ui->encounterLE->setEnabled(true);
    ui->objectLE->setEnabled(true);

    ui->locationTypeLE->setText(region->scene().locationExactType);
    ui->encounterTypeLE->setText(region->scene().encounterExactType);
    ui->objectTypeLE->setText(region->scene().objectExactType);

    ui->locationLE->setText(region->scene().location.name);
    ui->encounterLE->setText(region->scene().encounter.name);
    ui->objectLE->setText(region->scene().object.name);

    updateMapUI(region);
}

void MainDialog::updateMapUI(Region * region) {

    clearCurrentSceneUI();

    auto locations { region->mapLocations() };
    std::vector<QString> locationsVector ( locations.begin(), locations.end() );

    int n { static_cast<int>(locationsVector.size() - 1) }; // Exclude #NULL# element.

    if (n <= 0) return;

    auto regionName { region->name() };

    ui->regionMapHeaderL->setEnabled(true);
    ui->connectionL->setEnabled(true);
    ui->noConnectionL->setEnabled(true);
    ui->mapTW->setEnabled(true);
    ui->mapToStoryPB->setEnabled(true);

    ui->mapTW->setRowCount(n);
    ui->mapTW->setColumnCount(n);

    for (int i{1}; i<=n; ++i) {
        ui->mapTW->setHorizontalHeaderItem(i-1, new QTableWidgetItem(locationsVector[static_cast<unsigned>(i)]));
        ui->mapTW->setVerticalHeaderItem(i-1, new QTableWidgetItem(locationsVector[static_cast<unsigned>(i)]));
    }

    for (int i{1}; i<=n; ++i) {
        const auto & from { ui->mapTW->verticalHeaderItem(i-1)->text() };
        auto connections { region->connectionsFrom(from) };
        for (int j{1}; j<=n; ++j) {
            if (i == j) {
                QTableWidgetItem * blocked = new QTableWidgetItem;
                blocked->setFlags(blocked->flags() & ~Qt::ItemIsEnabled);
                ui->mapTW->setItem(i-1, j-1, blocked);
                continue;
            }
            const auto & to { ui->mapTW->horizontalHeaderItem(j-1)->text() };
            bool isConnected { connections.find(to) != connections.end() };
            auto * pb { new QPushButton { isConnected ? "+" : "-" } };
            pb->setCheckable(true);
            pb->setChecked(isConnected);
            ui->mapTW->setCellWidget(i-1, j-1, pb);

            connect(pb, &QPushButton::toggled, [this, regionName, from, to] { toggleLocationConnection(regionName, from, to); });
        }
    }

    ui->currentLocationGB->setEnabled(true);
    ui->currentLocationL->setEnabled(true);
    ui->currentLocationLE->setEnabled(true);
    ui->currentLocationLE->setText(region->currentMapLocation());
    ui->bidirectionalCB->setEnabled(true);
}

void MainDialog::mapToStory() {

    Region * region { nullptr };

    auto selectedRegion { ui->regionsLW->currentItem() };
    if (selectedRegion) region = Lists::region(selectedRegion->text());

    QString description { "DESCRIPTION OF REGION " + selectedRegion->text() + ": \n" };

    for (const auto & mapLocation : region->mapLocations()) {

        if (mapLocation == "#NULL#") continue;

        description += "\n    " + mapLocation + " » [ ";

        bool noConnection { true };

        for (const auto & connection : region->connectionsFrom(mapLocation)) {
            if (connection == "#NULL#") continue;
            noConnection = false;
            description += connection + ", ";
        }

        if (noConnection) description += "<DISCONNECTED>";
        else description.truncate(description.size() - 2);

        description += " ]";
    }

    appendStory(description + "\n");
}

void MainDialog::clearCurrentSceneUI() {

    ui->mapTW->clear();
    ui->mapTW->setRowCount(0);
    ui->mapTW->setColumnCount(0);
    ui->regionMapHeaderL->setEnabled(false);
    ui->connectionL->setEnabled(false);
    ui->noConnectionL->setEnabled(false);
    ui->mapTW->setEnabled(false);

    ui->currentLocationLE->clear();
    ui->currentLocationGB->setEnabled(false);
}

void MainDialog::toggleLocationConnection(QString regionName, const QString & from, const QString & to) {

    auto region { Lists::region(regionName) };
    bool bidirectional { ui->bidirectionalCB->isChecked() };

    auto connections { region->connectionsFrom(from) };
    if (connections.find(to) != connections.end()) {
        region->removeMapConnection(from, to);
        if (bidirectional) region->removeMapConnection(to, from);
    } else {
        region->addMapConnection(from, to);
        if (bidirectional) region->addMapConnection(to, from);
    }

    updateMapUI(region);
}

void MainDialog::addRegionElement(Region::ElementCategory category) {

    auto nred { new NewRegionElementDialog { nullptr, category } };

    connect(nred, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        auto region { Lists::region(ui->regionsLW->currentItem()->text()) };
        if (region) {
            region->addElement(nred->element());
            updateElements();

            auto * widget { elementWidget(category) };
            widget->setCurrentRow(widget->count() - 1);

            appendStory(
                        "NEW " + Region::ElementCategoryText(category) + " FOR REGION " +
                        region->name() + ": " + widget->currentItem()->text() + "\n");
        }
    });

    nred->exec();
}

void MainDialog::removeRegionElement(Region::ElementCategory category) {

    auto region { Lists::region(ui->regionsLW->currentItem()->text()) };

    auto * widget { elementWidget(category) };

    const int index { widget->currentRow() };

    appendStory(
                "REMOVING " + Region::ElementCategoryText(category) + " FROM REGION " +
                region->name() + ": " + widget->currentItem()->text() + "\n");

    if (widget && region->removeElement(category, static_cast<unsigned>(index))) updateElements();

    if (widget->count()) widget->setCurrentRow(index);
}

void MainDialog::moveRegionElement(Region::ElementCategory category, bool up) {

    auto region { Lists::region(ui->regionsLW->currentItem()->text()) };

    auto * widget { elementWidget(category) };

    if (widget) {
        int index { region->moveElement(category, widget->currentRow(), up) };
        updateElements();
        widget->setCurrentRow(index);
    }
}

void MainDialog::updateRegions() {

    ui->regionsLW->clear();

    for (auto & region : Lists::regions())
        ui->regionsLW->addItem(region.name());

    updateRegionSceneUI();
}

void MainDialog::updateElements() {

    ui->locationsLW->clear();
    ui->encountersLW->clear();
    ui->objectsLW->clear();

    auto selectedRegion { ui->regionsLW->currentItem() };
    if (!selectedRegion) return;

    const auto region { Lists::region(selectedRegion->text()) };
    if (!region) return;

    auto makeLabel { [](const auto & element) {

       auto label { Region::ElementTypeText(element.type) };
            if (element.type == Region::ElementType::Custom) {
                label += ": " + element.name;
                if (element.unique) label += " (U)";
            }
       return label;

    }};

    for (const auto & location : region->locations())
        ui->locationsLW->addItem(makeLabel(location));

    for (const auto & encounter : region->encounters())
        ui->encountersLW->addItem(makeLabel(encounter));

    for (const auto & object : region->objects())
        ui->objectsLW->addItem(makeLabel(object));

    updateRegionSceneUI();
}

void MainDialog::updateRemoveButtons() {

    const bool sceneSelected { ui->scenesLW->count() && ui->scenesLW->currentRow() >= 0 };
    const bool NPCSelected { ui->NPCsLW->count() && ui->NPCsLW->currentRow() >= 0 };
    const bool threadSelected { ui->threadsLW->count() && ui->threadsLW->currentRow() >= 0 };
    const bool playerCharacterSelected { ui->playerCharactersLW->count() && ui->playerCharactersLW->currentRow() >= 0 };
    const bool personalNPCSelected { ui->personalNPCsLW->count() && ui->personalNPCsLW->currentRow() >= 0 };
    const bool personalThreadSelected { ui->personalThreadsLW->count() && ui->personalThreadsLW->currentRow() >= 0 };

    ui->renameScenePB->setEnabled(sceneSelected);
    ui->removeNPCPB->setEnabled(NPCSelected);
    ui->removeThreadPB->setEnabled(threadSelected);
    ui->removePlayerCharacterPB->setEnabled(playerCharacterSelected);
    ui->removePersonalNPCPB->setEnabled(personalThreadSelected);
    ui->removePersonalThreadPB->setEnabled(personalThreadSelected);

    ui->renameNPCPB->setEnabled(NPCSelected);
    ui->renameThreadPB->setEnabled(threadSelected);
    ui->renamePlayerCharacterPB->setEnabled(playerCharacterSelected);
    ui->renamePersonalNPCPB->setEnabled(personalNPCSelected);
    ui->renamePersonalThreadPB->setEnabled(personalThreadSelected);

    ui->addPersonalNPCPB->setEnabled(playerCharacterSelected);
    ui->addPersonalThreadPB->setEnabled(playerCharacterSelected);

    if (playerCharacterSelected) {
        const auto & PC {ui->playerCharactersLW->currentItem()->text()};
        ui->peopleAndThreadsTB->setItemText(3, "PERSONAL NPCS && THREADS: " + PC);
        ui->peopleAndThreadsTB->setItemText(4, "CHARACTER BACKSTORY: " + PC);
    }
    else {
        ui->peopleAndThreadsTB->setItemText(3, "PERSONAL NPCS && THREADS");
        ui->peopleAndThreadsTB->setItemText(4, "CHARACTER BACKSTORY");
    }

    ui->backstoryPTE->setEnabled(playerCharacterSelected);
    ui->generateBackstoryPB->setEnabled(playerCharacterSelected);
    ui->saveBackstoryPB->setEnabled(playerCharacterSelected);
}

void MainDialog::updatePersonalNPCsAndThreads(const QString & player) {

    ui->personalNPCsLW->clear();
    ui->personalThreadsLW->clear();
    ui->backstoryPTE->clear();

    if (player.isEmpty()) return;

    ui->backstoryPTE->setPlainText(Lists::PCBackstory(player));

    const auto personalNPCs { Lists::personalNPCs(player) };
    const auto personalThreads { Lists::personalThreads(player) };

    if (personalNPCs && personalThreads) {
        for (const auto & NPC : *personalNPCs) ui->personalNPCsLW->addItem(NPC);
        for (const auto & thread : *personalThreads) ui->personalThreadsLW->addItem(thread);
    } else
        ui->playerCharactersLW->setCurrentRow(-1);
}

void MainDialog::updateChaosFactorDisplay() {

    ui->chaosFactorPrB->setValue(FateChart::chaosFactor());
}

void MainDialog::updateNPCDescriptorCheckboxes() {

    auto descriptors { Lists::descriptors(ui->NPCCB->currentText()) };

    ui->priIdentityActiveCB->setEnabled(false);
    ui->secIdentityActiveCB->setEnabled(false);
    ui->priPersonalityActiveCB->setEnabled(false);
    ui->secPersonalityActiveCB->setEnabled(false);
    ui->priActivityActiveCB->setEnabled(false);
    ui->secActivityActiveCB->setEnabled(false);

    ui->priIdentityActiveCB->setChecked(false);
    ui->secIdentityActiveCB->setChecked(false);
    ui->priPersonalityActiveCB->setChecked(false);
    ui->secPersonalityActiveCB->setChecked(false);
    ui->priActivityActiveCB->setChecked(false);
    ui->secActivityActiveCB->setChecked(false);

    ui->dispByIdentityGB->setEnabled(false);
    ui->dispByPersonalityGB->setEnabled(false);
    ui->dispByActivityGB->setEnabled(false);

    if (!descriptors) return;

    if (descriptors->identity().primary.isEmpty())
        ui->priIdentityActiveCB->setChecked(false);
    else {
        ui->priIdentityActiveCB->setEnabled(true);
        if (descriptors->identity().active == 1) ui->priIdentityActiveCB->setChecked(true);
    }

    if (descriptors->identity().secondary.isEmpty())
        ui->secIdentityActiveCB->setChecked(false);
    else {
        ui->secIdentityActiveCB->setEnabled(true);
        if (descriptors->identity().active == 2) ui->secIdentityActiveCB->setChecked(true);
    }

    if (descriptors->personality().primary.isEmpty())
        ui->priPersonalityActiveCB->setChecked(false);
    else {
        ui->priPersonalityActiveCB->setEnabled(true);
        if (descriptors->personality().active == 1) ui->priPersonalityActiveCB->setChecked(true);
    }

    if (descriptors->personality().secondary.isEmpty())
        ui->secPersonalityActiveCB->setChecked(false);
    else {
        ui->secPersonalityActiveCB->setEnabled(true);
        if (descriptors->personality().active == 2) ui->secPersonalityActiveCB->setChecked(true);
    }

    if (descriptors->activity().primary.isEmpty())
        ui->priActivityActiveCB->setChecked(false);
    else {
        ui->priActivityActiveCB->setEnabled(true);
        if (descriptors->activity().active == 1) ui->priActivityActiveCB->setChecked(true);
    }

    if (descriptors->activity().secondary.isEmpty())
        ui->secActivityActiveCB->setChecked(false);
    else {
        ui->secActivityActiveCB->setEnabled(true);
        if (descriptors->activity().active == 2) ui->secActivityActiveCB->setChecked(true);
    }

    ui->dispByIdentityGB->setEnabled(descriptors->identity().active);
    ui->dispByPersonalityGB->setEnabled(descriptors->personality().active);
    ui->dispByActivityGB->setEnabled(descriptors->activity().active);
}

void MainDialog::updateDispositionModifiers(bool writeDescriptor) {

    const auto & NPC { ui->NPCCB->currentText() };
    auto descriptors { Lists::descriptors(NPC) };

    if (!descriptors) {
        NPCDescriptors emptyDescriptors;
        Lists::setNPCDescriptors(NPC, emptyDescriptors);
        descriptors = Lists::descriptors(NPC);
    }

    if (writeDescriptor) {

        descriptors->setIdentityIncDisp(ui->incDispositionByIdentityRB->isChecked());
        descriptors->setPersonalityIncDisp(ui->incDispositionByPersonalityRB->isChecked());
        descriptors->setActivityIncDisp(ui->incDispositionByActivityRB->isChecked());
    }

    updateDispositionDisplay();
}

void MainDialog::changeBaseDisposition(int value) {

    const auto & NPC { ui->NPCCB->currentText() };
    auto descriptors { Lists::descriptors(NPC) };

    descriptors->setDispositionBaseValue(value);
    updateDispositionDisplay();
}

void MainDialog::updateDispositionDisplay() {

    const auto & descriptors { Lists::descriptors(ui->NPCCB->currentText()) };

    ui->dispositionHS->setValue(descriptors->dispositionBaseValue());
    ui->dispositionPrB->setValue(descriptors->dispositionScore());

    ui->baseDispositionL->setText(QString::number(descriptors->dispositionBaseValue()));
    ui->dispositionL->setText( QString::number(descriptors->dispositionScore()) + ": " +
                NPCDescriptors::dispositionText(descriptors->totalDisposition()) );
}

void MainDialog::refreshTab(int tabIndex) {

    if (tabIndex == 2) fillNPCDetails();
}

void MainDialog::discardNPCDetails() {

    const auto & NPC { ui->NPCCB->currentText() };

    auto descriptors { Lists::descriptors(NPC) };

    if (descriptors) {
        Lists::removeNPCDescriptors(NPC);
        loadNPCDetails(NPC);
        appendStory(NPC + " DESCRIPTORS DISCARDED\n");
    }
}

void MainDialog::fillNPCDetails(int index) {

    ui->NPCCB->clear();

    if (Lists::NPCs().empty()) return;

    for (const auto & NPC : Lists::NPCs()) ui->NPCCB->addItem(NPC);
    auto NPCs { ui->NPCCB->count() };
    if (!NPCs) return;

    if (index > 0 && index < NPCs) ui->NPCCB->setCurrentIndex(index);
}

void MainDialog::loadNPCDetails(QString NPC) {

    static QString lastNPC { "" };
    if (NPC.isEmpty() && (Lists::NPCs().find(lastNPC) != Lists::NPCs().end())) NPC = lastNPC;

    bool load { !NPC.isEmpty() };

    ui->NPCL->setEnabled(load);
    ui->NPCCB->setEnabled(load);
    ui->discardNPCDetailsPB->setEnabled(load);
    ui->NPCDescriptorsGB->setEnabled(load);

    if (!load) return;

    auto descriptors { Lists::descriptors(NPC) };

    if (!descriptors) {
        NPCDescriptors emptyDescriptors;
        Lists::setNPCDescriptors(NPC, emptyDescriptors);
        descriptors = Lists::descriptors(NPC);
    }

    ui->themePB->setText(replaceEmpty(descriptors->theme()));
    ui->actionPB->setText(replaceEmpty(descriptors->action()));

    auto priIdentity { replaceEmpty(descriptors->identity().primary) };
    auto secIdentity { replaceEmpty(descriptors->identity().secondary) };
    auto priPersonality { replaceEmpty(descriptors->personality().primary) };
    auto secPersonality { replaceEmpty(descriptors->personality().secondary) };
    auto priActivity { replaceEmpty(descriptors->activity().primary) };
    auto secActivity { replaceEmpty(descriptors->activity().secondary) };

    ui->priIdentityPB->setText(priIdentity);
    ui->secIdentityPB->setText(secIdentity);
    ui->priPersonalityPB->setText(priPersonality);
    ui->secPersonalityPB->setText(secPersonality);
    ui->priActivityPB->setText(priActivity);
    ui->secActivityPB->setText(secActivity);

    descriptors->identityIncDisp() ? ui->incDispositionByIdentityRB->setChecked(true) : ui->decDispositionByIdentityRB->setChecked(true);
    descriptors->personalityIncDisp() ? ui->incDispositionByPersonalityRB->setChecked(true) : ui->decDispositionByPersonalityRB->setChecked(true);
    descriptors->activityIncDisp() ? ui->incDispositionByActivityRB->setChecked(true) : ui->decDispositionByActivityRB->setChecked(true);

    updateNPCDescriptorCheckboxes();
    updateSecondaryNPCDescriptorControls();
    updateDispositionModifiers(false);
}

void MainDialog::saveNPCDetails() {

}

void MainDialog::setNPCDescriptor(NPCDescriptor descriptor) {

    QString label;

    switch (descriptor) {
    case NPCDescriptor::Theme:                label = "THEME";                 break;
    case NPCDescriptor::Action:               label = "ACTION";                break;
    case NPCDescriptor::PrimaryIdentity:      label = "PRIMARY IDENTITY";      break;
    case NPCDescriptor::SecondaryIdentity:    label = "SECONDARY IDENTITY";    break;
    case NPCDescriptor::PrimaryPersonality:   label = "PRIMARY PERSONALITY";   break;
    case NPCDescriptor::SecondaryPersonality: label = "SECONDARY PERSONALITY"; break;
    case NPCDescriptor::PrimaryActivity:      label = "PRIMARY ACTIVITY";      break;
    case NPCDescriptor::SecondaryActivity:    label = "SECONDARY ACTIVITY";
    }

    auto tlid { new TextLineInputDialog { nullptr, true } };
    tlid->setTitle("SET NEW NPC DESCRIPTOR");
    tlid->setLabel(label);

    connect(tlid, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        const auto & NPC { ui->NPCCB->currentText() };

        auto descriptors { Lists::descriptors(NPC) };

        if (!descriptors) {
            NPCDescriptors emptyDescriptors;
            Lists::setNPCDescriptors(NPC, emptyDescriptors);
            descriptors = Lists::descriptors(NPC);
        }

        QString entry { NPC };

        const auto text { tlid->inputText() };
        const auto textUI { replaceEmpty(text) };

        switch (descriptor) {
        case NPCDescriptor::Theme:
            descriptors->setTheme(text);
            ui->themePB->setText(textUI);
            entry += " THEME: " + textUI;
            break;
        case NPCDescriptor::Action:
            descriptors->setAction(text);
            ui->actionPB->setText(textUI);
            entry += " ACTION: " + textUI;
            break;
        case NPCDescriptor::PrimaryIdentity: {
            auto identity { descriptors->identity() };
            identity.primary = text;
            if (text.isEmpty()) {
                identity.active = 0;
                identity.secondary.clear();
                ui->secIdentityPB->setText(replaceEmpty());
                entry += " PRIMARY & SECONDARY IDENTITY: ";
            } else {
                entry += " PRIMARY IDENTITY: ";
            }
            descriptors->setIdentity(identity);
            ui->priIdentityPB->setText(textUI);
            entry += textUI;
            break;
        }
        case NPCDescriptor::SecondaryIdentity: {
            auto identity { descriptors->identity() };
            identity.secondary = text;
            if (text.isEmpty() && descriptors->identity().active == 2)
                identity.active = 0;
            descriptors->setIdentity(identity);
            ui->secIdentityPB->setText(textUI);
            entry += " SECONDARY IDENTITY: " + textUI;
            break;
        }
        case NPCDescriptor::PrimaryPersonality: {
            auto personality { descriptors->personality() };
            personality.primary = text;
            if (text.isEmpty()) {
                personality.active = 0;
                personality.secondary.clear();
                ui->secPersonalityPB->setText(replaceEmpty());
                entry += " PRIMARY & SECONDARY PERSONALITY: ";
            } else {
                entry += " PRIMARY PERSONALITY: ";
            }
            descriptors->setPersonality(personality);
            ui->priPersonalityPB->setText(textUI);
            entry += textUI;
            break;
        }
        case NPCDescriptor::SecondaryPersonality: {
            auto personality { descriptors->personality() };
            personality.secondary = text;
            if (text.isEmpty() && descriptors->personality().active == 2)
                personality.active = 0;
            descriptors->setPersonality(personality);
            ui->secPersonalityPB->setText(textUI);
            entry += " SECONDARY PERSONALITY: " + textUI;
            break;
        }
        case NPCDescriptor::PrimaryActivity:{
            auto activity { descriptors->activity() };
            activity.primary = text;
            if (text.isEmpty()) {
                activity.active = 0;
                activity.secondary.clear();
                ui->secActivityPB->setText(replaceEmpty());
                entry += " PRIMARY & SECONDARY ACTIVITY: ";
            } else {
                entry += " PRIMARY ACTIVITY: ";
            }
            descriptors->setActivity(activity);
            ui->priActivityPB->setText(textUI);
            entry += textUI;
            break;
        }
        case NPCDescriptor::SecondaryActivity: {
            auto activity { descriptors->activity() };
            activity.secondary = text;
            if (text.isEmpty() && descriptors->activity().active == 2)
                activity.active = 0;
            descriptors->setActivity(activity);
            ui->secActivityPB->setText(textUI);
            entry += " SECONDARY ACTIVITY: " + textUI;
            break;
        }
        }

        Lists::setNPCDescriptors(NPC, *descriptors);

        updateNPCDescriptorCheckboxes();
        updateSecondaryNPCDescriptorControls();
        updateDispositionModifiers(false);

        appendStory(entry + "\n");
    });

    tlid->exec();
}

void MainDialog::setNPCDescriptorActive(MainDialog::NPCDescriptor descriptor) {

    const auto & NPC { ui->NPCCB->currentText() };
    auto descriptors { Lists::descriptors(NPC) };

    switch (descriptor) {
    case NPCDescriptor::PrimaryIdentity:
        descriptors->setActiveIdentity(
                    ui->priIdentityActiveCB->isChecked() ? 1 : (ui->secIdentityActiveCB->isChecked() ? 2 : 0));
        appendStory(NPC + " ACTIVE IDENTITY: " + descriptors->activeIdentity() + "\n");
        break;
    case NPCDescriptor::SecondaryIdentity:
        descriptors->setActiveIdentity(
                    ui->secIdentityActiveCB->isChecked() ? 2 : (ui->priIdentityActiveCB->isChecked() ? 1 : 0));
        appendStory(NPC + " ACTIVE IDENTITY: " + descriptors->activeIdentity() + "\n");
        break;
    case NPCDescriptor::PrimaryPersonality:
        descriptors->setActivePersonality(
                    ui->priPersonalityActiveCB->isChecked() ? 1 : (ui->secPersonalityActiveCB->isChecked() ? 2 : 0));
        appendStory(NPC + " ACTIVE PERSONALITY: " + descriptors->activePersonality() + "\n");
        break;
    case NPCDescriptor::SecondaryPersonality:
        descriptors->setActivePersonality(
                    ui->secPersonalityActiveCB->isChecked() ? 2 : (ui->priPersonalityActiveCB->isChecked() ? 1 : 0));
        appendStory(NPC + " ACTIVE PERSONALITY: " + descriptors->activePersonality() + "\n");
        break;
    case NPCDescriptor::PrimaryActivity:
        descriptors->setActiveActivity(
                    ui->priActivityActiveCB->isChecked() ? 1 : (ui->secActivityActiveCB->isChecked() ? 2 : 0));
        appendStory(NPC + " ACTIVE ACTIVITY: " + descriptors->activeActivity() + "\n");
        break;
    case NPCDescriptor::SecondaryActivity:
        descriptors->setActiveActivity(
                    ui->secActivityActiveCB->isChecked() ? 2 : (ui->priActivityActiveCB->isChecked() ? 1 : 0));
        appendStory(NPC + " ACTIVE ACTIVITY: " + descriptors->activeActivity() + "\n");
        break;
    default:;
    }

    updateNPCDescriptorCheckboxes();
    updateDispositionModifiers(false);
}

void MainDialog::updateSecondaryNPCDescriptorControls() {

    const auto & descriptors { Lists::descriptors(ui->NPCCB->currentText()) };

    ui->secIdentityPB->setDisabled(!descriptors || descriptors->identity().primary.isEmpty());
    ui->secPersonalityPB->setDisabled(!descriptors || descriptors->personality().primary.isEmpty());
    ui->secActivityPB->setDisabled(!descriptors || descriptors->activity().primary.isEmpty());
}

void MainDialog::backToBehaviorCheck() {
    ui->mainTW->setCurrentIndex(0);
    behaviorCheck(true);
}

void MainDialog::hideAndShowFeatures() {

    ui->detailCheckPB->setVisible(Utils::settings.value("detail_check_enabled", true).toBool());
    ui->eventCheckPB->setVisible(Utils::settings.value("event_check_enabled", true).toBool());
    ui->behaviorCheckPB->setVisible(Utils::settings.value("behavior_check_enabled", true).toBool());
    ui->statisticCheckPB->setVisible(Utils::settings.value("statistic_check_enabled", true).toBool());
    ui->RPGSystemCheckPB->setVisible(Utils::settings.value("rpg_check_enabled", true).toBool());
    ui->tweenStoryPB->setVisible(Utils::settings.value("off_camera_stories_enabled", true).toBool());
    ui->randomDescriptionPB->setVisible(Utils::settings.value("random_description_enabled", true).toBool());

    if (Utils::settings.value("off_camera_stories_enabled", true).toBool()) {
        if (ui->peopleAndThreadsTB->count() == 4) {
            ui->peopleAndThreadsTB->addItem(ui->backstoryPage, "CHARACTER BACKSTORY");
            updateRemoveButtons();
        }
    } else {
        if (ui->peopleAndThreadsTB->count() == 5) {
            ui->peopleAndThreadsTB->widget(4)->setVisible(false);
            ui->peopleAndThreadsTB->removeItem(4);
        }
    }

    ui->mainTW->removeTab(3);
    ui->mainTW->removeTab(2);

    if (Utils::settings.value("behavior_check_enabled", true).toBool())
        ui->mainTW->addTab(ui->NPCBehaviorTab, "NPC BEHAVIOR");

    if (Utils::settings.value("location_crafter_enabled", true).toBool())
        ui->mainTW->addTab(ui->locationsTab, "LOCATIONS");
}

void MainDialog::tabChanged(int) {

    ui->backToBehaviorCheckW->setVisible(false);
    if (Lists::NPCs().size()) ui->NPCCB->setEnabled(true);
}

