#ifndef BEHAVIORCHECKTABLES_H
#define BEHAVIORCHECKTABLES_H

#include <QString>
#include <memory>

#include "dice.h"

class BehaviorCheckTables
{
public:
    BehaviorCheckTables() = delete;

    enum class NPCAction1 {

        ThemeAction,
        NPCContinues,
        NPCContinuesPlus2,
        NPCContinuesMinus2,
        NPCAction,
        NPCActionMinus4,
        NPCActionPlus4
    };

    enum class NPCAction2 {

        TalksOrExposition,
        PerformsAnAmbiguousAction,
        ActsOutOfPCInterest,
        GivesSomething,
        SeeksToEndTheEncounter,
        ChangesTheTheme,
        ChangesDescriptor,
        ActosOutOfSelfInterest,
        TakesSomething,
        CausesHarm
    };

    static QString NPCAction1Text(NPCAction1 action);
    static QString NPCAction2Text(NPCAction2 action);

    struct Result {
        NPCAction1 table1Result;
        std::shared_ptr<NPCAction2> table2Result = nullptr;
    };

    static Result behaviorCheckTableResult(int modifier = 0);
    static QString dispositionTable(int disposition);

private:

    static Dice s_d10;
};

#endif // BEHAVIORCHECKTABLES_H
