#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QWindow>
#include <QScrollArea>
#include <QSettings>

#include <set>

#include "dice.h"

class Utils : public QWidget
{
    Q_OBJECT

public:
    Utils() = delete;

    static const QString version;
    static void loadApplicationStylesheet();

    static void maximizeOnPhone(QDialog * window);
    static void scrollWithSwipe(QAbstractScrollArea *scrollArea);

    static void waitUntilTrue(bool & flag);

    static QString randomElementInSet(const std::set<QString> & set);
    static void removeFromSet(std::set<QString> & set1, const std::set<QString> & set2);

    static QString randomNPC();
    static QString randomThread();
    static QString randomPlayerCharacter();
    static void endAllNPCActions();

    template <class T, class U>
    static T substractNoNegative (T x, U y) {

        if (static_cast<unsigned>(y) > static_cast<unsigned>(x)) return 0;
        return static_cast<unsigned>(x) - static_cast<unsigned>(y);
    }

    static void showErrorMessage(QString errorMessage);
    static void showYesNoQuestionMessage(QString question,
                                         const QString &title = "",
                                         std::function<void(bool)> action = nullptr);


    static QSettings settings;
    static QApplication * application;

private:

    static Dice s_d100;
};

#endif // UTILS_H
