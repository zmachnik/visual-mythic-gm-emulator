QT      += core gui widgets

TARGET   = VisualMythicGME
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++17
QMAKE_CXXFLAGS += "-Wno-implicit-fallthrough"

QMAKE_WASM_PTHREAD_POOL_SIZE = 1

SOURCES += \
    backstorydialog.cpp \
    chooseregionelementdialog.cpp \
    main.cpp \
    maindialog.cpp \
    fatechart.cpp \
    dice.cpp \
    randomevent.cpp \
    newadventuredialog.cpp \
    fatechartdialog.cpp \
    newscenedialog.cpp \
    lists.cpp \
    infodialog.cpp \
    textlineinputdialog.cpp \
    rpgsystemcheckdialog.cpp \
    dicedialog.cpp \
    randomeventdialog.cpp \
    settingsdialog.cpp \
    detailcheckdialog.cpp \
    detailchecktable.cpp \
    tweenstorydialog.cpp \
    utils.cpp \
    meaningtables.cpp \
    eventcheckdialog.cpp \
    randomdescriptiondialog.cpp \
    npcdescriptors.cpp \
    behaviorchecktables.cpp \
    behaviorcheckdialog.cpp \
    statisticcheckdialog.cpp \
    region.cpp \
    newregionelementdialog.cpp \
    newregionscenedialog.cpp \
    specialelementdialog.cpp

HEADERS += \
    backstorydialog.h \
    chooseregionelementdialog.h \
    maindialog.h \
    fatechart.h \
    dice.h \
    randomevent.h \
    newadventuredialog.h \
    fatechartdialog.h \
    newscenedialog.h \
    lists.h \
    infodialog.h \
    textlineinputdialog.h \
    rpgsystemcheckdialog.h \
    dicedialog.h \
    randomeventdialog.h \
    settingsdialog.h \
    detailcheckdialog.h \
    detailchecktable.h \
    tweenstorydialog.h \
    utils.h \
    meaningtables.h \
    eventcheckdialog.h \
    randomdescriptiondialog.h \
    npcdescriptors.h \
    behaviorchecktables.h \
    behaviorcheckdialog.h \
    statisticcheckdialog.h \
    region.h \
    newregionelementdialog.h \
    newregionscenedialog.h \
    specialelementdialog.h

FORMS += \
    backstorydialog.ui \
    chooseregionelementdialog.ui \
    maindialog.ui \
    newadventuredialog.ui \
    fatechartdialog.ui \
    newscenedialog.ui \
    infodialog.ui \
    textlineinputdialog.ui \
    rpgsystemcheckdialog.ui \
    dicedialog.ui \
    randomeventdialog.ui \
    settingsdialog.ui \
    detailcheckdialog.ui \
    eventcheckdialog.ui \
    randomdescriptiondialog.ui \
    behaviorcheckdialog.ui \
    statisticcheckdialog.ui \
    newregionelementdialog.ui \
    newregionscenedialog.ui \
    specialelementdialog.ui \
    tweenstorydialog.ui

RESOURCES += \
    resources.qrc

