#ifndef STATISTICCHECKDIALOG_H
#define STATISTICCHECKDIALOG_H

#include <QDialog>

namespace Ui {
class StatisticCheckDialog;
}

class StatisticCheckDialog : public QDialog {

    Q_OBJECT

public:
    explicit StatisticCheckDialog(QWidget * parent = nullptr);
    virtual ~StatisticCheckDialog() override;

    const QString & statistic() const;

protected slots:

    virtual void accept() override;

private slots:

    void checkInputs();
    void updateModifier();
    void checkStatistic();

private:
    Ui::StatisticCheckDialog *ui;

    QString m_statistic;
    int m_modifier = 0;
    bool m_initialize = true;
};

#endif // STATISTICCHECKDIALOG_H
