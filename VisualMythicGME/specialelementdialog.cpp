#include "specialelementdialog.h"
#include "ui_specialelementdialog.h"

#include <QIcon>
#include <algorithm>
#include "dice.h"
#include "randomdescriptiondialog.h"
#include "randomeventdialog.h"
#include "utils.h"

SpecialElementDialog::SpecialElementDialog(QWidget *parent, const Region & region, Region::Element & element) :
    QDialog(parent),
    ui(new Ui::SpecialElementDialog),
    r_region { region },
    m_region { region },
    r_element { element },
    m_element { element } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->specialElementSA);
    Utils::scrollWithSwipe(ui->action1PTE);
    Utils::scrollWithSwipe(ui->action2PTE);
    Utils::scrollWithSwipe(ui->interpretationPTE);

    if (element.type == Region::ElementType::Random) {
        ui->specialElementTypeLE->setText(Region::ElementTypeText(element.type));
        ui->rollSpecialElementPB->setEnabled(false);
        ui->inspirationGB->setEnabled(true);
        ui->firstElementRB->setEnabled(true);
        ui->description1LE->setEnabled(true);
        ui->action1PTE->setEnabled(true);
    }
}


SpecialElementDialog::~SpecialElementDialog() {

    delete ui;
}

QString SpecialElementDialog::interpretation() const {

    return m_interpretation;
}

QString SpecialElementDialog::specialType() const {

    return m_specialType;
}

QString SpecialElementDialog::returnLocation() const {

    return m_returnLocation;
}

void SpecialElementDialog::accept() {

    if (r_element.type == Region::ElementType::Random) m_specialType = "RANDOM";
    else if (m_element.secondElement)
        m_specialType = "SPECIAL: MULTI-ELEMENT » ["
                + Region::ElementTypeText(m_element.type, true) + "] & ["
                + Region::ElementTypeText(m_element.secondElement->type, true) + "]";
    else m_specialType = "SPECIAL: " + ui->specialElementTypeLE->text();

    if (m_element.specialType == Region::SpecialElementType::RemoveElement) m_element.unique = true;

    r_element = m_element;

    m_interpretation = ui->interpretationPTE->toPlainText();
    m_returnLocation = ui->locationLE->text();

    QDialog::accept();
}

void SpecialElementDialog::rollElement(bool isSecond) {

    Dice d6 {6};

    Region::Element e;
    const bool multi { m_element.specialType == Region::SpecialElementType::MultiElement };

    if (r_element.category == Region::ElementCategory::Location) {
        auto locationProgress { d6.get() + r_region.locationPPs() };
        if (m_region.locations().size() > locationProgress) {
            e = m_region.locations().at(locationProgress);
            if (!isSecond) m_region.setLocationPPs(m_region.locationPPs() + 1);
        } else {
            e = Region::Element { Region::ElementCategory::Location };
            if (!isSecond) m_region.setLocationPPs(Utils::substractNoNegative(m_region.locationPPs(), 5));
        }
    }

    else if (r_element.category == Region::ElementCategory::Encounter) {
        auto encounterProgress { d6.get() + r_region.encounterPPs() };
        if (m_region.encounters().size() > encounterProgress) {
            e = m_region.encounters().at(encounterProgress);
            if (!isSecond) m_region.setEncounterPPs(m_region.encounterPPs() + 1);
        } else {
            e = Region::Element { Region::ElementCategory::Encounter };
            if (!isSecond) m_region.setEncounterPPs(Utils::substractNoNegative(m_region.encounterPPs(), 5));
        }
    }

    else if (r_element.category == Region::ElementCategory::Object) {
        auto objectProgress { d6.get() + r_region.objectPPs() };
        if (m_region.objects().size() > objectProgress) {
            e = m_region.objects().at(objectProgress);
            if (!isSecond) m_region.setObjectPPs(m_region.objectPPs() + 1);
        } else {
            e = Region::Element { Region::ElementCategory::Object };
            if (!isSecond) m_region.setObjectPPs(Utils::substractNoNegative(m_region.objectPPs(), 5));
        }
    }

    auto text { Region::ElementTypeText(e.type, true) };
    if (e.type == Region::ElementType::Custom) text += ": " + e.name;

    if (!isSecond) {
        ui->newElementLE->setText(text);
        e.specialType = m_element.specialType;
        m_element = e;
        if (!multi) {
            ui->interpretationGB->setEnabled(true);
            ui->labelL->setEnabled(true);
            ui->interpretationPTE->setEnabled(true);
        } else {
            ui->rollSecondPB->setEnabled(true);
        }
    } else {
        ui->secondElementLE->setText(text);
        m_element.secondElement = std::make_shared<Region::Element>(e);

        if (m_element.type != Region::ElementType::Random
            || ui->description1LE->text().size() || ui->action1PTE->toPlainText().size()) {
            ui->interpretationGB->setEnabled(true);
            ui->labelL->setEnabled(true);
            ui->interpretationPTE->setEnabled(true);
        }
    }

    if (e.type == Region::ElementType::Random) {

        ui->inspirationGB->setEnabled(true);
        ui->interpretationGB->setEnabled(false);

        if (!isSecond) {
            ui->firstElementRB->setEnabled(true);
            ui->description1LE->setEnabled(true);
            ui->action1PTE->setEnabled(true);
        } else {
            ui->secondElementRB->setEnabled(true);
            ui->secondElementRB->setChecked(true);
            ui->description2LE->setEnabled(true);
            ui->action2PTE->setEnabled(true);
        }
    }

    checkInput();
}

void SpecialElementDialog::rollsMade() {

    ui->interpretationGB->setEnabled(true);
    ui->labelL->setEnabled(true);
    ui->interpretationPTE->setEnabled(true);
}

void SpecialElementDialog::rollSpecialElement() {

    ui->rollSpecialElementPB->setEnabled(false);
    ui->labelL->setEnabled(true);

    Dice d100 {100, true};

    auto n { d100.get() };
    Region::SpecialElementType type;

    if      (n <  6) type = Region::SpecialElementType::SuperSize;
    else if (n < 11) type = Region::SpecialElementType::BarelyThere;
    else if (n < 16) type = Region::SpecialElementType::RemoveElement;
    else if (n < 26) type = Region::SpecialElementType::AddElement;
    else if (n < 31) type = Region::SpecialElementType::ThisIsBad;
    else if (n < 36) type = Region::SpecialElementType::ThisIsGood;
    else if (n < 51) type = Region::SpecialElementType::MultiElement;
    else if (n < 61) type = Region::SpecialElementType::ExitHere;
    else if (n < 71) type = Region::SpecialElementType::Return;
    else if (n < 76) type = Region::SpecialElementType::GoingDeeper;
    else if (n < 81) type = Region::SpecialElementType::CommonGround;
    else             type = Region::SpecialElementType::RandomElement;

    m_element.specialType = type;

    bool locationApplicable { (m_element.category == Region::ElementCategory::Location)
                              && m_region.mapLocations().size() };

    ui->specialElementTypeLE->setText(Region::SpecialElementText(type, !locationApplicable));

    if (type == Region::SpecialElementType::SuperSize
        || type == Region::SpecialElementType::BarelyThere
        || type == Region::SpecialElementType::RemoveElement
        || type == Region::SpecialElementType::ThisIsBad
        || type == Region::SpecialElementType::ThisIsGood) {
        ui->newElementGB->setEnabled(true);
        ui->elementL->setEnabled(true);
        ui->newElementLE->setEnabled(true);
    } else if (type == Region::SpecialElementType::MultiElement) {
        ui->newElementGB->setEnabled(true);
        ui->elementL->setEnabled(true);
        ui->newElementLE->setEnabled(true);
        ui->secondElementL->setEnabled(true);
        ui->secondElementLE->setEnabled(true);
    } else if (type == Region::SpecialElementType::ExitHere) {
        ui->interpretationGB->setEnabled(true);
    } else if (type == Region::SpecialElementType::Return) {
        if (locationApplicable) {
            ui->returnGB->setEnabled(true);
            ui->rollLocationPB->setEnabled(true);
        } else {
            ui->interpretationGB->setEnabled(true);
        }
    } else if (type == Region::SpecialElementType::GoingDeeper || type == Region::SpecialElementType::CommonGround) {
        ui->interpretationGB->setEnabled(true);
    } else if (type == Region::SpecialElementType::RandomElement || type == Region::SpecialElementType::AddElement) {
        ui->inspirationGB->setEnabled(true);
        ui->firstElementRB->setEnabled(true);
        ui->description1LE->setEnabled(true);
        ui->action1PTE->setEnabled(true);
    }

    ui->rollAgainPB->setEnabled(true);
}

void SpecialElementDialog::rollAgain() {

    rollElement(false);
    ui->rollAgainPB->setEnabled(false);
}

void SpecialElementDialog::rollSecond() {

    rollElement(true);
    ui->rollSecondPB->setEnabled(false);
}


void SpecialElementDialog::rollLocation() {

    auto locations {r_region.mapLocations()};
    const unsigned n {static_cast<unsigned>(locations.size())};

    Dice d{n-2, false}; // Omit <EXIT> nad #NULL# elements.

    auto it {locations.begin()};
    std::advance(it, d.get() + 2);

    ui->locationLE->setText(*it);

    ui->rollLocationPB->setEnabled(false);
    ui->makesSenseGB->setEnabled(true);
    ui->makesSenseL->setEnabled(true);
    ui->yesPB->setEnabled(true);
    ui->noRerollPB->setEnabled(true);
    ui->noExpectedPB->setEnabled(true);
}

void SpecialElementDialog::yesMakesSense() {

    ui->returnGB->setEnabled(false);
    ui->interpretationGB->setEnabled(true);
}

void SpecialElementDialog::makesNoSenseReroll() {

    rollLocation();
}

void SpecialElementDialog::treatAsExpected() {

    ui->returnGB->setEnabled(false);
    ui->specialElementTypeLE->setText("RETURN » EXPECTED");
    ui->interpretationGB->setEnabled(true);
    r_element.type = Region::ElementType::Expected;
    r_element.specialType = Region::SpecialElementType::Null;
}

void SpecialElementDialog::rollDescription() {

    auto rdd { new RandomDescriptionDialog { nullptr, true, false } };

    connect(rdd, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        if (ui->firstElementRB->isChecked()) {

            ui->description1LE->setText(rdd->randomDescription());

            if (!m_element.secondElement || m_element.secondElement->type != Region::ElementType::Random
                || ui->description2LE->text().size() || ui->action2PTE->toPlainText().size()) {
                ui->rollDescriptionPB->setEnabled(false);
                rollsMade();
            }
        } else if (ui->secondElementRB->isChecked()) {
            ui->description2LE->setText(rdd->randomDescription());

            if (m_element.type != Region::ElementType::Random
                    || ui->description1LE->text().size() || ui->action1PTE->toPlainText().size()) {
                ui->rollDescriptionPB->setEnabled(false);
                rollsMade();
            }
        }
    });

    rdd->exec();
}

void SpecialElementDialog::rollAction() {

    auto red { new RandomEventDialog { nullptr, true, false } };

    connect(red, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        if (ui->firstElementRB->isChecked()) {

            ui->action1PTE->setPlainText(red->randomEvent());

            if (!m_element.secondElement || m_element.secondElement->type != Region::ElementType::Random
                || ui->description2LE->text().size() || ui->action2PTE->toPlainText().size()) {
                ui->rollActionPB->setEnabled(false);
                rollsMade();
            }
        } else if (ui->secondElementRB->isChecked()) {
            ui->action2PTE->setPlainText(red->randomEvent());

            if (m_element.type != Region::ElementType::Random
                    || ui->description1LE->text().size() || ui->action1PTE->toPlainText().size()) {
                ui->rollActionPB->setEnabled(false);
                rollsMade();
            }
        }
    });

    red->exec();
}

void SpecialElementDialog::checkInput() {

    ui->acceptPB->setEnabled(ui->interpretationPTE->toPlainText().count());
}

void SpecialElementDialog::updateButtons() {

    ui->rollDescriptionPB->setEnabled((ui->firstElementRB->isChecked() && ui->description1LE->text().isEmpty())
                                      || (ui->secondElementRB->isChecked() && ui->description2LE->text().isEmpty()));

    ui->rollActionPB->setEnabled((ui->firstElementRB->isChecked() && ui->action1PTE->toPlainText().isEmpty())
                                 || (ui->secondElementRB->isChecked() && ui->action2PTE->toPlainText().isEmpty()));
}
