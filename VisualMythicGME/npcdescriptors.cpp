#include "npcdescriptors.h"

NPCDescriptors::NPCDescriptors() {

}

QString NPCDescriptors::activeIdentity(bool noEmpty) const {

    QString ai { noEmpty ? "<NO ACTIVE IDENTITY>" : "" };

    if (m_identity.active) {
        if (m_identity.active == 1) ai = m_identity.primary;
        else ai = m_identity.secondary;
    }

    return ai;
}

QString NPCDescriptors::activePersonality(bool noEmpty) const {

    QString ap { noEmpty ? "<NO ACTIVE PERSONALITY>" : "" };

    if (m_personality.active) {
        if (m_personality.active == 1) ap = m_personality.primary;
        else ap = m_personality.secondary;
    }

    return ap;
}

QString NPCDescriptors::activeActivity(bool noEmpty) const {

    QString aa { noEmpty ? "<NO ACTIVE ACTIVITY>" : "" };

    if (m_activity.active) {
        if (m_activity.active == 1) aa = m_activity.primary;
        else aa = m_activity.secondary;
    }

    return aa;
}

void NPCDescriptors::setActiveIdentity(int index) {

    m_identity.active = index;
    applyDispositionModifiers();
}

void NPCDescriptors::setActivePersonality(int index) {

    m_personality.active = index;
    applyDispositionModifiers();
}

void NPCDescriptors::setActiveActivity(int index) {

    m_activity.active = index;
    applyDispositionModifiers();
}

void NPCDescriptors::setInteractionOngoing(bool yes) {

    m_interactionOngoing = yes;
}

bool NPCDescriptors::interactionOngoing() const {

    return m_interactionOngoing;
}

int NPCDescriptors::dispositionBaseValue() const {

    return m_dispositionValue;
}

QString NPCDescriptors::theme(bool noEmpty) const {

    return (noEmpty && m_theme.isEmpty()) ? "<NO ACTIVE THEME>" : m_theme;
}

QString NPCDescriptors::action(bool noEmpty) const {

    return (noEmpty && m_action.isEmpty()) ? "<NO ACTIVE ACTION>" : m_action;
}

NPCDescriptors::Descriptor NPCDescriptors::identity() const {

    return m_identity;
}

NPCDescriptors::Descriptor NPCDescriptors::personality() const {

    return m_personality;
}

NPCDescriptors::Descriptor NPCDescriptors::activity() const {

    return m_activity;
}

bool NPCDescriptors::identityIncDisp() const {

    return m_identityIncDisp;
}

bool NPCDescriptors::personalityIncDisp() const {

    return m_personalityIncDisp;
}

bool NPCDescriptors::activityIncDisp() const {

    return m_activityIncDisp;
}

void NPCDescriptors::setTheme(const QString & theme) {

    m_theme = theme;
}

void NPCDescriptors::setAction(const QString & action) {

    m_action = action;
}

void NPCDescriptors::setIdentity(NPCDescriptors::Descriptor identity) {

    m_identity = identity;
}

void NPCDescriptors::setPersonality(NPCDescriptors::Descriptor personality) {

    m_personality = personality;
}

void NPCDescriptors::setActivity(NPCDescriptors::Descriptor activity) {

    m_activity = activity;
}

void NPCDescriptors::setIdentityIncDisp(bool yes) {

    m_identityIncDisp = yes;
    applyDispositionModifiers();
}

void NPCDescriptors::setPersonalityIncDisp(bool yes) {

    m_personalityIncDisp = yes;
    applyDispositionModifiers();
}

void NPCDescriptors::setActivityIncDisp(bool yes) {

    m_activityIncDisp = yes;
    applyDispositionModifiers();
}

void NPCDescriptors::setDispositionBaseValue(int disposition) {

    if (disposition < 2) disposition = 2;
    if (disposition > 20) disposition = 20;

    m_dispositionValue = disposition;
}

void NPCDescriptors::applyDispositionModifiers() {

    m_dispositionModifiers = 0;

    if (m_identity.active) m_dispositionModifiers += ( m_identityIncDisp ? 2 : -2);
    if (m_personality.active) m_dispositionModifiers += ( m_personalityIncDisp ? 2 : -2);
    if (m_activity.active) m_dispositionModifiers += ( m_activityIncDisp ? 2 : -2);
}

int NPCDescriptors::dispositionScore() const {

    return m_dispositionValue + m_dispositionModifiers;
}

NPCDescriptors::Disposition NPCDescriptors::totalDisposition() const {

    const auto total { dispositionScore() };

    if (total <= 5) return Disposition::Passive;
    else if (total <= 10) return Disposition::Moderate;
    else if (total <= 15) return Disposition::Active;
    else return Disposition::Aggressive;
}

QString NPCDescriptors::dispositionText(NPCDescriptors::Disposition disposition) {

    switch (disposition) {
    case Disposition::Passive:    return "PASSIVE (-2)";
    case Disposition::Moderate:   return "MODERATE (0)";
    case Disposition::Active:     return "ACTIVE (+2)";
    case Disposition::Aggressive: return "AGGRESSIVE (+4)";
    }

    return "";
}

int NPCDescriptors::dispositionModifier(NPCDescriptors::Disposition disposition) {

    switch (disposition) {
    case Disposition::Passive:    return -2;
    case Disposition::Moderate:   return  0;
    case Disposition::Active:     return +2;
    case Disposition::Aggressive: return +4;
    }

    return 0;
}
