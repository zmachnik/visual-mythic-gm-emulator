#include "rpgsystemcheckdialog.h"
#include "ui_rpgsystemcheckdialog.h"

#include "dicedialog.h"
#include "utils.h"

RPGSystemCheckDialog::RPGSystemCheckDialog(QWidget *parent) :
    QDialog { parent },
    ui { new Ui::RPGSystemCheckDialog } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
}

RPGSystemCheckDialog::~RPGSystemCheckDialog() {

    delete ui;
}

void RPGSystemCheckDialog::accept() {

    m_checkText = ui->checkLE->text();
    m_resultText = ui->resultLE->text();

    QDialog::accept();
}

QString RPGSystemCheckDialog::check() const {

    return m_checkText;
}

QString RPGSystemCheckDialog::result() const {

    return m_resultText;
}

void RPGSystemCheckDialog::checkInputs() {

    ui->acceptPB->setDisabled(ui->checkLE->text().isEmpty() || ui->resultLE->text().isEmpty());
}

void RPGSystemCheckDialog::dice()
{
    auto dd { new DiceDialog };
    dd->exec();
}
