#ifndef BACKSTORYDIALOG_H
#define BACKSTORYDIALOG_H

#include <QDialog>
#include <set>
#include "dice.h"

namespace Ui {
class BackstoryDialog;
}

class BackstoryDialog : public QDialog {

    Q_OBJECT

public:
    explicit BackstoryDialog(QWidget *parent, const QString & character);
    virtual ~BackstoryDialog() override;

    const QString & backstory() const;

protected slots:

    virtual void accept() override;

private slots:

    void rollEventNumber();
    void rollNextEventType();
    void rollRandomEvent();
    void rollNPC();
    void rollThread();
    void okNPC();
    void okThread();
    void updateUI();

private:
    Ui::BackstoryDialog *ui;

    static Dice s_d100;

    QString m_character;
    int m_eventNumber {0};
    int m_currentEvent {0};
    bool m_addNPC {false};
    bool m_addThread {false};
    bool m_NPCRolled {false};
    bool m_threadRolled {false};

    // Items to be added to a character's personal lists once accept() is triggered.
    std::set<QString> m_addedNPCs;
    std::set<QString> m_addedThreads;

    QString m_backstory;

    void nextEvent();
};

#endif // BACKSTORYDIALOG_H
