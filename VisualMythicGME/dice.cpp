#include "dice.h"

#include <chrono>

Dice::Dice(unsigned sides, bool oneBased)
    : m_dre { static_cast<unsigned long>(std::chrono::system_clock::now().time_since_epoch().count()) },
      m_uid { oneBased, sides - !oneBased } {}

Dice::Dice(int sides, bool oneBased)
    : Dice (static_cast<unsigned>(sides), oneBased) {}

unsigned Dice::get() {
    return m_uid(m_dre);
}

int Dice::getInt() {
    return static_cast<int>(get());
}
