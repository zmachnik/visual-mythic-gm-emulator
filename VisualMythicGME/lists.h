#ifndef LISTS_H
#define LISTS_H

#include <set>
#include <map>
#include <vector>
#include <QString>

#include "npcdescriptors.h"
#include "region.h"

class Lists {

public:

    static bool addPlayerCharacter(const QString & player);
    static bool renamePlayerCharacter(const QString & player, const QString & newName);
    static bool removePlayerCharacter(const QString & player);
    static bool addNPC(const QString & NPC);
    static bool renameNPC(const QString & NPC, const QString & newName);
    static bool removeNPC(const QString & NPC);
    static bool addThread(const QString & thread);
    static bool renameThread(const QString & thread, const QString & newName);
    static bool removeThread(const QString & thread);
    static bool addPersonalNPC(const QString & player, const QString & NPC);
    static bool removePersonalNPC(const QString & player, const QString & NPC);
    static bool addPersonalThread(const QString & player, const QString & thread);
    static bool removePersonalThread(const QString & player, const QString &thread);
    static bool setNPCDescriptors(const QString & NPC, const NPCDescriptors & descriptors);
    static bool removeNPCDescriptors(const QString & NPC);

    static bool addRegion(const QString & region);
    static bool insertRegion(const Region & region);
    static bool renameRegion(const QString & region, const QString & newName);
    static bool removeRegion(const QString & region);

    static const std::set<QString> & playerCharacters();
    static const std::set<QString> & NPCs();
    static const std::set<QString> & threads();
    static const std::vector<Region> & regions();
    static Region * region(const QString regionName);

    static std::set<QString> allPersonalNPCs();
    static std::set<QString> allNPCs();
    static std::set<QString> allPersonalThreads();
    static std::set<QString> allThreads();

    static const std::set<QString> * personalNPCs(const QString & player);
    static const std::set<QString> * personalThreads(const QString & player);

    static QString PCBackstory(const QString & player);
    static void setPCBackstory(const QString & player, const QString & story);

    static NPCDescriptors * descriptors(const QString & NPC); // Not being const simplifies applying frequent changes.

    static void clear();

private:

    static std::set<QString> s_playerCharacters;
    static std::set<QString> s_NPCs;
    static std::set<QString> s_threads;
    static std::map<QString,std::set<QString>> s_playerNPCs;
    static std::map<QString,std::set<QString>> s_playerThreads;
    static std::map<QString,NPCDescriptors> s_NPCDescriptors;
    static std::map<QString,QString> s_PCBackstories;
    static std::vector<Region> s_regions;
};

#endif // LISTS_H
