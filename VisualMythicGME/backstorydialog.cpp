#include "backstorydialog.h"
#include "ui_backstorydialog.h"

#include <QIcon>
#include "randomeventdialog.h"
#include "lists.h"
#include "utils.h"

Dice BackstoryDialog::s_d100 { Dice {100, true } };

BackstoryDialog::BackstoryDialog(QWidget *parent, const QString & character) :
    QDialog(parent),
    ui(new Ui::BackstoryDialog),
    m_character {character} {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->randomEventPTE);
    Utils::scrollWithSwipe(ui->backstoryPTE);
}

BackstoryDialog::~BackstoryDialog() {

    delete ui;
}

const QString & BackstoryDialog::backstory() const {

    return m_backstory;
}

void BackstoryDialog::accept() {

    m_backstory = ui->backstoryPTE->toPlainText();

    for (const auto & NPC : m_addedNPCs) Lists::addPersonalNPC(m_character, NPC);
    for (const auto & thread : m_addedThreads) Lists::addPersonalThread(m_character, thread);

    return QDialog::accept();
}

void BackstoryDialog::rollEventNumber() {

    const auto n { s_d100.get() };

    if (n < 9) m_eventNumber = 1;
    else if (n < 27) m_eventNumber = 2;
    else if (n < 52) m_eventNumber = 3;
    else if (n < 72) m_eventNumber = 4;
    else if (n < 87) m_eventNumber = 5;
    else if (n < 97) m_eventNumber = 6;
    else m_eventNumber = 7;

    ui->numberLCD->display(m_eventNumber);

    m_currentEvent = 1;

    ui->eventsPrB->setRange(0, m_eventNumber);
    ui->eventsPrB->setValue(1);
    ui->eventsPrB->setFormat("%v/" + QString::number(m_eventNumber));

    ui->rollNumberPB->setEnabled(false);
    ui->backstoryGB->setEnabled(true);
    ui->NPCGB->setEnabled(false);
    ui->threadGB->setEnabled(false);
    ui->eventsPrB->setEnabled(true);
    ui->rollNextPB->setEnabled(true);
}

void BackstoryDialog::rollNextEventType() {

    m_addNPC = m_addThread = false;

    const auto n { s_d100.get() };

    if (n < 45) {
        ui->backstoryFocusLE->setText("NEW PC CHARACTER");
        m_addNPC = true;
    }
    else if (n < 69) {
        ui->backstoryFocusLE->setText("NEW PC THREAD");
        m_addThread = true;
    }
    else if (n < 85) {
        ui->backstoryFocusLE->setText("PC NEGATIVE");
    }
    else {
        ui->backstoryFocusLE->setText("PC POSITIVE");
    }

    ui->rollNextPB->setEnabled(false);
    ui->randomEventGB->setEnabled(true);
    ui->focusL->setEnabled(true);
    ui->backstoryFocusLE->setEnabled(true);
    ui->rollRandomEventPB->setEnabled(true);
    ui->backstoryPTE->setEnabled(false);
}

void BackstoryDialog::rollRandomEvent() {

    auto red { new RandomEventDialog {nullptr, true, false} };

    connect(red, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        ui->backstoryPTE->appendPlainText("\n" + QString::number(m_currentEvent) +  ". BACKSTORY FOCUS: " + ui->backstoryFocusLE->text());

        ui->rollRandomEventPB->setEnabled(false);
        ui->randomEventPTE->setEnabled(true);
        ui->randomEventPTE->setPlainText(red->randomEvent());

        if (m_addNPC) {

            ui->NPCGB->setEnabled(true);

            auto NPCs { Lists::allNPCs() };
            const auto personalNPCs { Lists::personalNPCs(m_character) };

            if (personalNPCs) {
                Utils::removeFromSet(NPCs, *personalNPCs);
                Utils::removeFromSet(NPCs, m_addedNPCs);
            }

            if (!NPCs.empty()) {
                for (const auto & NPC : NPCs) ui->NPCCB->addItem(NPC);
                ui->NPCCB->setEnabled(true);
                ui->rollNPCPB->setEnabled(true);
                ui->okNPCPB->setEnabled(true);
            }
            else {
                ui->existingNPCRB->setEnabled(false);
                ui->newNPCRB->setChecked(true);
            }
        } else if (m_addThread) {

            ui->threadGB->setEnabled(true);

            auto threads { Lists::allThreads() };
            const auto personalThreads { Lists::personalThreads(m_character) };

            if (personalThreads) {
                Utils::removeFromSet(threads, *personalThreads);
                Utils::removeFromSet(threads, m_addedThreads);
            }

            if (!threads.empty()) {
                for (const auto & thread : threads) ui->threadCB->addItem(thread);
                ui->threadCB->setEnabled(true);
                ui->rollThreadPB->setEnabled(true);
                ui->okThreadPB->setEnabled(true);
            }
            else {
                ui->existingThreadRB->setEnabled(false);
                ui->newThreadRB->setChecked(true);
            }
        } else {

            nextEvent();
        }

        ui->backstoryPTE->appendPlainText("\n" + ui->randomEventPTE->toPlainText());
    });

    red->exec();
}

void BackstoryDialog::rollNPC() {

    m_NPCRolled = true;

    int n { ui->NPCCB->count() };
    Dice dn { n, false };

    ui->NPCCB->setCurrentIndex(dn.getInt());
    ui->NPCCB->setEnabled(false);
    ui->rollNPCPB->setEnabled(false);
}

void BackstoryDialog::rollThread() {

    m_threadRolled = true;

    int n { ui->threadCB->count() };
    Dice dn { n, false };

    ui->threadCB->setCurrentIndex(dn.getInt());
    ui->threadCB->setEnabled(false);
    ui->rollThreadPB->setEnabled(false);
}

void BackstoryDialog::okNPC() {

    QString NPC;

    if (ui->existingNPCRB->isChecked())
        NPC = ui->NPCCB->currentText();
    else
        NPC = ui->newNPCLE->text();

    ui->backstoryPTE->appendPlainText("\nADDING PERSONAL NPC: " + NPC);
    m_addedNPCs.insert(NPC);

    ui->NPCGB->setEnabled(false);
    ui->existingNPCRB->setChecked(true);
    ui->NPCCB->clear();
    ui->newNPCLE->clear();

    nextEvent();
}

void BackstoryDialog::okThread() {

    QString thread;

    if (ui->existingThreadRB->isChecked())
        thread = ui->threadCB->currentText();
    else
        thread = ui->newThreadLE->text();

    ui->backstoryPTE->appendPlainText("\nADDING PERSONAL THREAD: " + thread);
    m_addedThreads.insert(thread);

    ui->threadGB->setEnabled(false);
    ui->existingThreadRB->setChecked(true);
    ui->threadCB->clear();
    ui->newThreadLE->clear();

    nextEvent();
}

void BackstoryDialog::updateUI() {

    if (m_addNPC) {
        if (ui->existingNPCRB->isChecked() && !m_NPCRolled) {
            ui->NPCCB->setEnabled(true);
            ui->rollNPCPB->setEnabled(true);
            ui->newNPCLE->setEnabled(false);
        } else if (ui->newNPCRB->isChecked()) {
            ui->NPCCB->setEnabled(false);
            ui->rollNPCPB->setEnabled(false);
            ui->newNPCLE->setEnabled(true);
        }

        ui->okNPCPB->setEnabled(ui->existingNPCRB->isChecked() || !ui->newNPCLE->text().isEmpty());

    } else if (m_addThread) {
        if (ui->existingThreadRB->isChecked() && !m_threadRolled) {
            ui->threadCB->setEnabled(true);
            ui->rollThreadPB->setEnabled(true);
            ui->newThreadLE->setEnabled(false);
        } else if (ui->newThreadRB->isChecked()) {
            ui->threadCB->setEnabled(false);
            ui->rollThreadPB->setEnabled(false);
            ui->newThreadLE->setEnabled(true);
        }

        ui->okThreadPB->setEnabled(ui->existingThreadRB->isChecked() || !ui->newThreadLE->text().isEmpty());
    }

    ui->acceptPB->setEnabled(ui->backstoryPTE->isEnabled() && !ui->backstoryPTE->toPlainText().isEmpty());
}

void BackstoryDialog::nextEvent() {

    ui->eventsPrB->setValue(++m_currentEvent);

    if (m_currentEvent == m_eventNumber+1) {
        ui->backstoryPTE->setEnabled(true);
        ui->acceptPB->setEnabled(true);
    } else {
        ui->rollNextPB->setEnabled(true);
    }
}
