#ifndef RPGSYSTEMCHECKDIALOG_H
#define RPGSYSTEMCHECKDIALOG_H

#include <QDialog>

namespace Ui {
class RPGSystemCheckDialog;
}

class RPGSystemCheckDialog : public QDialog {

    Q_OBJECT

public:

    explicit RPGSystemCheckDialog(QWidget *parent = nullptr);
    virtual ~RPGSystemCheckDialog() override;
    QString check() const;
    QString result() const;

protected slots:

    virtual void accept() override;

private:
    Ui::RPGSystemCheckDialog *ui;
    QString m_checkText;
    QString m_resultText;

private slots:

    void checkInputs();
    void dice();
};

#endif // RPGSYSTEMCHECKDIALOG_H
