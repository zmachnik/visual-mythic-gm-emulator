#ifndef TEXTLINEINPUTDIALOG_H
#define TEXTLINEINPUTDIALOG_H

#include <QDialog>

namespace Ui {
class TextLineInputDialog;
}

class TextLineInputDialog : public QDialog {

    Q_OBJECT

public:

    explicit TextLineInputDialog(QWidget *parent = nullptr, bool allowEmpty = false);
    virtual ~TextLineInputDialog() override;

    void setTitle(const QString & title);
    void setLabel(const QString & label);
    void setDefaultText(const QString & text);
    QString inputText() const;

protected slots:

    virtual void accept() override;

private:

    Ui::TextLineInputDialog *ui;
    QString m_textLine;
    bool m_allowEmpty;

private slots:

    void checkInput(QString text);
};

#endif // TEXTLINEINPUTDIALOG_H
