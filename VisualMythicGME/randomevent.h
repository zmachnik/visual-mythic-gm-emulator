#ifndef RANDOMEVENT_H
#define RANDOMEVENT_H

#include <QString>
#include <vector>

#include "dice.h"

class RandomEvent {

public:

    struct Event {
        QString meaningAction;
        QString meaningSubject;
        QString focusType;
        QString focusTarget;
    };

    struct Description {
        QString adverb;
        QString adjective;
    };

    static Description getDescription();
    static Event getEvent();

private:

    static Dice s_d100;
};

#endif // RANDOMEVENT_H
