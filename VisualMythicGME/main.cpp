#include "maindialog.h"

#include <QApplication>
#include "utils.h"

int main(int argc, char *argv[]) {

    QApplication a(argc, argv);
    Utils::application = &a;

    a.setOrganizationName("ZM");
    a.setOrganizationDomain("-");
    a.setApplicationName("Visual Mythic GM Emulator");

    Utils::loadApplicationStylesheet();

    MainDialog w;
    w.show();

    return a.exec();
}
