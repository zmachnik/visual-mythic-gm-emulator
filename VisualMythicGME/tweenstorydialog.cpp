#include "tweenstorydialog.h"
#include "ui_tweenstorydialog.h"

#include <QIcon>
#include "lists.h"
#include "fatechart.h"
#include "randomeventdialog.h"
#include "utils.h"

TweenStoryDialog::TweenStoryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TweenStoryDialog) {
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->randomEventPTE);
    Utils::scrollWithSwipe(ui->interpretationPTE);

    for (const auto & character : Lists::playerCharacters())
        ui->characterCB->addItem(character);
}

TweenStoryDialog::~TweenStoryDialog() {

    delete ui;
}

const QString & TweenStoryDialog::tweenStory() const {

    return m_tweenStory;
}

void TweenStoryDialog::accept() {

    m_tweenStory += "\nSTORY: " + ui->interpretationPTE->toPlainText();

    QDialog::accept();
}

void TweenStoryDialog::checkEvent() {

    ui->characterL->setEnabled(false);
    ui->characterCB->setEnabled(false);
    ui->guessLE->setReadOnly(true);
    ui->checkPB->setEnabled(false);
    ui->resultGB->setEnabled(true);

    const auto fate { FateChart::askFateChart(FateChart::Odds::FiftyFifty , 5) };
    const auto answer { FateChart::Fate::answerText(fate.answer) };

    ui->resultLE->setText(answer);
    m_tweenStory = "TWEEN STORY FOR: " + ui->characterCB->currentText();
    m_tweenStory += "\nFATE: " + answer;
    // fate.event is ignored.

    if (fate.answer == FateChart::Fate::Answer::ExceptionalNo) {
        ui->randomEventGB->setEnabled(true);
        ui->randomEventL->setEnabled(true);
        ui->rollRandomEventPB->setEnabled(true);
        ui->randomEventPTE->setEnabled(true);
    } else {
        ui->interpretationGB->setEnabled(true);
        ui->interpretationL->setEnabled(true);
        ui->interpretationPTE->setEnabled(true);
        if (fate.answer == FateChart::Fate::Answer::Yes)
            ui->interpretationPTE->setPlainText(ui->guessLE->text());
    }
}

void TweenStoryDialog::rollRandomEvent() {

    auto red { new RandomEventDialog {nullptr, true, false} };

    connect(red, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        ui->rollRandomEventPB->setEnabled(false);
        ui->randomEventPTE->setPlainText(red->randomEvent());
        m_tweenStory += "\n" + red->randomEvent();
        ui->interpretationGB->setEnabled(true);
        ui->interpretationL->setEnabled(true);
        ui->interpretationPTE->setEnabled(true);
    });

    red->exec();
}

void TweenStoryDialog::updateUI() {

    ui->checkPB->setEnabled(!ui->guessLE->text().isEmpty() && ui->resultLE->text().isEmpty());
    ui->acceptPB->setEnabled(!ui->interpretationPTE->toPlainText().isEmpty());
}
