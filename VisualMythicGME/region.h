#ifndef REGION_H
#define REGION_H

#include <QString>
#include <vector>
#include <set>
#include <map>
#include <memory>

class Region {

public:
    enum class ElementType {

        Custom,
        Expected,
        Special,
        Random,
        None,
        Complete,
        Null
    };

    enum class SpecialElementType {

        SuperSize,
        BarelyThere,
        RemoveElement,
        AddElement,
        ThisIsBad,
        ThisIsGood,
        MultiElement,
        ExitHere,
        Return,
        GoingDeeper,
        CommonGround,
        RandomElement,
        Null
    };

    enum class ElementCategory {

        Location,
        Encounter,
        Object,
        Null
    };

    struct Element {

        ElementCategory category;
        ElementType type;
        SpecialElementType specialType;
        bool unique;
        QString name;
        std::shared_ptr<Element> secondElement;

        Element (const ElementCategory category = ElementCategory::Location,
                 const ElementType type = ElementType::Expected,
                 SpecialElementType specialType = SpecialElementType::Null, bool unique = false,
                 const QString & name = "");
    };

    struct Scene {

        Element location { ElementCategory::Location };
        Element encounter { ElementCategory::Encounter };
        Element object { ElementCategory::Object };

        bool active { false };

        QString locationExactType;
        QString encounterExactType;
        QString objectExactType;
    };

    Region(const QString & name = "");

    QString name() const;
    void setName(const QString & name);

    Scene & scene();
    const Scene & constScene() const;

    static QString ElementCategoryText(ElementCategory category);
    static ElementCategory parseElementCategory(const QString & str);
    static QString ElementTypeText(ElementType type, bool noSpecial = false);
    static ElementType parseElementType(const QString & str);
    static QString SpecialElementText(SpecialElementType type, bool notLocation = false);

    const std::vector<Element> & locations() const;
    const std::vector<Element> & encounters() const;
    const std::vector<Element> & objects() const;

    unsigned locationPPs() const;
    unsigned encounterPPs() const;
    unsigned objectPPs() const;

    void setLocationPPs(int value);
    void setEncounterPPs(int value);
    void setObjectPPs(int value);

    void setLocationPPs(unsigned value);
    void setEncounterPPs(unsigned value);
    void setObjectPPs(unsigned value);

    void addElement(ElementCategory category,  const ElementType type, const SpecialElementType specialType, bool unique, const QString & name);
    void addElement(Element element);
    bool removeElement(ElementCategory category, unsigned index);
    bool removeElement(ElementCategory category, const QString & name); // May be useful later. If not, delete.
    int moveElement(ElementCategory category, int index, bool up);

    const std::multimap<QString, QString> & regionMap() const;
    void addMapLocation(QString fromLocation, const QString & newLocation);
    void addMapConnection(QString from, const QString & to);
    void removeMapConnection(const QString & from, const QString & to);
    std::set<QString> mapLocations() const;
    std::set<QString> connectionsFrom(const QString &mapLocation) const;
    const QString & currentMapLocation() const;
    void setCurrentMapLocation(const QString & location);

    bool operator<(const Region & r2) const;

private:

    QString m_name;

    std::vector<Element> m_locations;
    std::vector<Element> m_encounters;
    std::vector<Element> m_objects;

    unsigned m_locationPPs { 0 };
    unsigned m_encounterPPs { 0 };
    unsigned m_objectPPs { 0 };

    std::multimap<QString, QString> m_regionMap;
    QString m_currentMapLocation;

    Scene m_scene;
};

#endif // REGION_H
