#ifndef EVENTCHECKDIALOG_H
#define EVENTCHECKDIALOG_H

#include <QDialog>

#include "randomevent.h"

namespace Ui {
class EventCheckDialog;
}

class EventCheckDialog : public QDialog {

    Q_OBJECT

public:

    explicit EventCheckDialog(QWidget *parent = nullptr);
    ~EventCheckDialog() override;

    const QString & question() const;
    RandomEvent::Event randomEvent() const;

private:

    Ui::EventCheckDialog *ui;

    QString m_question;
    RandomEvent::Event m_event;

private slots:

    void checkQuestion();
    void eventCheck();
};

#endif // EVENTCHECKDIALOG_H
