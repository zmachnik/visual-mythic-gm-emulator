#ifndef NPCDESCRIPTORS_H
#define NPCDESCRIPTORS_H

#include <QString>

class NPCDescriptors {

public:

    NPCDescriptors();

    struct Descriptor {
        QString primary;
        QString secondary;
        int active = 0; // 0: none, 1: first, 2: second
    };

    enum class Disposition {
        Passive,
        Moderate,
        Active,
        Aggressive
    };

    QString theme(bool noEmpty = false) const;
    QString action(bool noEmpty = false) const;

    Descriptor identity() const;
    Descriptor personality() const;
    Descriptor activity() const;

    bool identityIncDisp() const;
    bool personalityIncDisp() const;
    bool activityIncDisp() const;

    void setTheme(const QString & theme);
    void setAction(const QString & action);

    void setIdentity(Descriptor identity);
    void setPersonality(Descriptor personality);
    void setActivity(Descriptor activity);

    void setIdentityIncDisp(bool yes);
    void setPersonalityIncDisp(bool yes);
    void setActivityIncDisp(bool yes);

    void setDispositionBaseValue(int disposition);
    void applyDispositionModifiers();


    QString activeIdentity(bool noEmpty = false) const;
    QString activePersonality(bool noEmpty = false) const;
    QString activeActivity(bool noEmpty = false) const;

    void setActiveIdentity(int index);
    void setActivePersonality(int index);
    void setActiveActivity(int index);

    void setInteractionOngoing(bool yes);
    bool interactionOngoing() const;

    int dispositionBaseValue() const;
    int dispositionScore() const;


    Disposition totalDisposition() const;

    static QString dispositionText(Disposition disposition);
    static int dispositionModifier(Disposition disposition);

private:

    QString m_theme;
    QString m_action;

    Descriptor m_identity;
    Descriptor m_personality;
    Descriptor m_activity;

    bool m_identityIncDisp = true;
    bool m_personalityIncDisp = true;
    bool m_activityIncDisp = true;

    int m_dispositionValue = 11;
    int m_dispositionModifiers = 0;

    bool m_interactionOngoing = false;
};

#endif // NPCDESCRIPTORS_H
