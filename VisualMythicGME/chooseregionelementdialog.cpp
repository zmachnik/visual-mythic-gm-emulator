#include "chooseregionelementdialog.h"
#include "ui_chooseregionelementdialog.h"

#include <QIcon>

#include "utils.h"

ChooseRegionElementDialog::ChooseRegionElementDialog(QWidget * parent, const QListWidget * elementList, const Region & region, Region::ElementCategory category) :
    QDialog(parent),
    ui(new Ui::ChooseRegionElementDialog),
    r_region { region }, m_category { category } {

    ui->setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->elementsLW);

    if (category == Region::ElementCategory::Location) setWindowTitle("CHOOSE LOCATION");
    else if (category == Region::ElementCategory::Encounter) setWindowTitle("CHOOSE ENCOUNTER");
    else if (category == Region::ElementCategory::Object) setWindowTitle("CHOOSE OBJECT");

    for (int i {0}; i < elementList->count(); ++i) ui->elementsLW->addItem(elementList->item(i)->text());

    updateUI();
}

ChooseRegionElementDialog::~ChooseRegionElementDialog() {

    delete ui;
}

int ChooseRegionElementDialog::row() const {

    return m_row;
}

void ChooseRegionElementDialog::accept() {

    m_row = ui->elementsLW->currentRow();

    QDialog::accept();
}

void ChooseRegionElementDialog::updateUI() {

    ui->acceptPB->setEnabled(ui->elementsLW->currentRow() >= 0);
}
