#ifndef NEWADVENTUREDIALOG_H
#define NEWADVENTUREDIALOG_H

#include <QDialog>

namespace Ui {
class NewAdventureDialog;
}

class NewAdventureDialog : public QDialog {

    Q_OBJECT

public:

    explicit NewAdventureDialog(QWidget *parent = nullptr);
    virtual ~NewAdventureDialog() override;

    const QString & adventure() const;
    const QString & RPGSystem() const;
    bool createFirstScene() const;
    bool addPlayerCharacters() const;
    bool addNPCs() const;
    bool addThreads() const;

protected slots:

    virtual void accept() override;

private:

    Ui::NewAdventureDialog *ui;
    QString m_newAdventure;
    QString m_RPGSystem;
    bool m_createFirstScene;
    bool m_addPlayers;
    bool m_addNPCs;
    bool m_addThreads;

private slots:

    void checkInputs();
};

#endif // NEWADVENTUREDIALOG_H
