#ifndef INFODIALOG_H
#define INFODIALOG_H

#include <QDialog>

namespace Ui {
class InfoDialog;
}

class InfoDialog : public QDialog {

    Q_OBJECT

public:

    explicit InfoDialog(QWidget *parent = nullptr);
    virtual ~InfoDialog() override;

    void setTitle(const QString & title);
    void setMessage(const QString & messageText);

private:

    Ui::InfoDialog *ui;
};

#endif // INFODIALOG_H
