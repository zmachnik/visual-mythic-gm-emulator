#ifndef NEWREGIONELEMENTDIALOG_H
#define NEWREGIONELEMENTDIALOG_H

#include <QDialog>

#include "region.h"

namespace Ui {
class NewRegionElementDialog;
}

class NewRegionElementDialog : public QDialog {

    Q_OBJECT

public:

    explicit NewRegionElementDialog(QWidget *parent, const Region::ElementCategory category);
    virtual ~NewRegionElementDialog() override;

    Region::Element element() const;

protected slots:

    virtual void accept() override;

private slots:

    void updateUI();

private:
    Ui::NewRegionElementDialog *ui;
    Region::Element m_element;
};

#endif // NEWREGIONELEMENTDIALOG_H
