#include "eventcheckdialog.h"
#include "ui_eventcheckdialog.h"

#include "utils.h"

EventCheckDialog::EventCheckDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EventCheckDialog) {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->questionPTE);
}

EventCheckDialog::~EventCheckDialog() {

    delete ui;
}

const QString &EventCheckDialog::question() const
{
    return m_question;
}

RandomEvent::Event EventCheckDialog::randomEvent() const {

    return m_event;
}

void EventCheckDialog::checkQuestion()
{
    ui->checkPB->setEnabled(!ui->questionPTE->toPlainText().isEmpty());
}

void EventCheckDialog::eventCheck()
{
    m_question = ui->questionPTE->toPlainText();
    m_event = RandomEvent::getEvent();

    ui->eventFocusLE->setText(m_event.focusType);
    ui->targetLE->setText(m_event.focusTarget);
    ui->eventMeaningLE->setText(m_event.meaningAction + " : " + m_event.meaningSubject);

    ui->questionGB->setEnabled(false);
    ui->eventGB->setEnabled(true);
    ui->acceptPB->setEnabled(true);
}
