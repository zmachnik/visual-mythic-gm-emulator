#ifndef FATECHARTDIALOG_H
#define FATECHARTDIALOG_H

#include <QDialog>

#include "fatechart.h"

namespace Ui {
class FateChartDialog;
}

class FateChartDialog : public QDialog {

    Q_OBJECT

public:

    explicit FateChartDialog(QWidget *parent = nullptr);
    virtual ~FateChartDialog() override;

    QString question() const;
    FateChart::Fate fate() const;

protected slots:

    virtual void accept() override;

private:

    Ui::FateChartDialog * ui;

    QString m_question;
    FateChart::Fate m_fate;

private slots:

    void askFateChart();
    void checkQuestion();
    void updateOddsDescription(int oddsNumber);
};

#endif // FATECHARTDIALOG_H
