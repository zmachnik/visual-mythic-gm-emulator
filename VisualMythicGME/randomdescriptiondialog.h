#ifndef RANDOMDESCRIPTIONDIALOG_H
#define RANDOMDESCRIPTIONDIALOG_H

#include <QDialog>

namespace Ui {
class RandomDescriptionDialog;
}

class RandomDescriptionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RandomDescriptionDialog(QWidget *parent = nullptr, bool adventureActive = false, bool canCancelAfterRoll = true);
    ~RandomDescriptionDialog() override;

    QString randomDescription() const;

protected slots:

    virtual void accept() override;

private:
    Ui::RandomDescriptionDialog *ui;

    QString m_randomDescription;
    bool m_adventureActive = false;
    bool m_canCancelAfterRoll = true;

private slots:

    void rollRandomDescription();
};

#endif // RANDOMDESCRIPTIONDIALOG_H
