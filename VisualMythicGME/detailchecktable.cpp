#include "detailchecktable.h"

#include "utils.h"
#include "fatechart.h"

Dice DetailCheckTable::s_d10{10, true};

DetailCheckTable::Result DetailCheckTable::detailCheckTableResult() {

    auto diceValue { s_d10.get() + s_d10.get() };

    if (FateChart::chaosFactor() <= 3) diceValue += 2;
    else if (FateChart::chaosFactor() >= 6) diceValue -= 2;

    // An existing but empty QString means that a player character's name should be
    // assigned later in the UI.

    if (diceValue <= 4) return Result { "ANGER", std::make_shared<QString>("")};
    if (diceValue >= 18) return Result { "CALM", std::make_shared<QString>("") };

    switch (diceValue) {
    case 5: return Result { "SADNESS", std::make_shared<QString>("") };
    case 6: return Result { "FEAR", std::make_shared<QString>("") };
    case 7: return Result { "DISFAVORS THREAD", std::make_shared<QString>(Utils::randomThread()) };
    case 8: return Result { "DISFAVORS PC", std::make_shared<QString>(Utils::randomPlayerCharacter())};
    case 9: return Result { "FOCUS NPC", std::make_shared<QString>(Utils::randomNPC()) };
    case 10: return Result { "FAVORS NPC", std::make_shared<QString>(Utils::randomNPC()) };
    case 11: return Result { "FOCUS PC", std::make_shared<QString>(Utils::randomPlayerCharacter()) };
    case 12: return Result { "DISFAVORS NPC", std::make_shared<QString>(Utils::randomNPC()) };
    case 13: return Result { "FOCUS THREAD", std::make_shared<QString>(Utils::randomThread()) };
    case 14: return Result { "FAVORS PC", std::make_shared<QString>(Utils::randomPlayerCharacter()) };
    case 15: return Result { "FAVOURS THREAD", std::make_shared<QString>(Utils::randomThread()) };
    case 16: return Result { "COURAGE", std::make_shared<QString>("") };
    case 17: return Result { "HAPPINESS", std::make_shared<QString>("") };
    default:
        return Result { "" };;
    }

}
