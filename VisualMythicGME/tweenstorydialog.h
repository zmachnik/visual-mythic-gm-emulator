#ifndef TWEENSTORYDIALOG_H
#define TWEENSTORYDIALOG_H

#include <QDialog>

namespace Ui {
class TweenStoryDialog;
}

class TweenStoryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TweenStoryDialog(QWidget *parent = nullptr);
    virtual ~TweenStoryDialog() override;

    const QString & tweenStory() const;

protected slots:

    virtual void accept() override;

private slots:

    void checkEvent();
    void rollRandomEvent();
    void updateUI();

private:
    Ui::TweenStoryDialog *ui;

    QString m_tweenStory;
};

#endif // TWEENSTORYDIALOG_H
