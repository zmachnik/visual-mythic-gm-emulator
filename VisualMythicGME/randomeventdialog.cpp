#include "randomeventdialog.h"
#include "ui_randomeventdialog.h"

#include "randomevent.h"
#include "utils.h"

RandomEventDialog::RandomEventDialog(QWidget *parent, bool adventureActive, bool canCancelAfterRoll) :
    QDialog { parent },
    ui { new Ui::RandomEventDialog },
    m_adventureActive {adventureActive},
    m_canCancelAfterRoll {canCancelAfterRoll} {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    if (!canCancelAfterRoll) setWindowFlags(windowFlags() & ~Qt::WindowCloseButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
}

RandomEventDialog::~RandomEventDialog() {

    delete ui;
}

QString RandomEventDialog::randomEvent() const {

    return m_randomEvent;
}

void RandomEventDialog::accept() {

    if (m_adventureActive)
        m_randomEvent = "RANDOM EVENT:\n    [ EVENT MEANING » " + ui->eventMeaningLE->text()
            +  " ]\n    [ EVENT FOCUS » " + ui->eventFocusLE->text()
            + ", TARGET » " + ui->targetLE->text() + " ]";

    QDialog::accept();
}

void RandomEventDialog::rollRandomEvent() {

    const auto event { RandomEvent::getEvent() };

    ui->eventFocusLE->setText(event.focusType);
    ui->targetLE->setText(event.focusTarget);
    ui->eventMeaningLE->setText(event.meaningAction + " : " + event.meaningSubject);

    ui->acceptPB->setEnabled(m_adventureActive);
    ui->closePB->setEnabled(m_canCancelAfterRoll);

    ui->randomEventPB->setEnabled(false);
}
