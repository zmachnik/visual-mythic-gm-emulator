#include "fatechart.h"

const std::map<
               const FateChart::Odds,
               const std::map<
                              const FateChart::ChaosFactor,
                              const FateChart::Thresholds
                             >
              >

FateChart::s_thresholds = {
    {
        Odds::Impossible, {
            {1,{0,-20,77}},
            {2,{0,0,81}},
            {3,{0,0,81}},
            {4,{1,5,82}},
            {5,{1,5,82}},
            {6,{2,10,83}},
            {7,{3,15,84}},
            {8,{5,25,86}},
            {9,{10,50,91}}
        }
    },
    {
        Odds::NoWay, {
            {1,{0,0,81}},
            {2,{1,5,82}},
            {3,{1,5,82}},
            {4,{2,10,83}},
            {5,{3,15,84}},
            {6,{5,25,86}},
            {7,{7,35,88}},
            {8,{10,50,91}},
            {9,{15,75,96}}
        }
    },
    {
        Odds::VeryUnlikely, {
            {1,{1,5,82}},
            {2,{1,5,82}},
            {3,{2,10,83}},
            {4,{3,15,84}},
            {5,{5,25,86}},
            {6,{9,45,90}},
            {7,{10,50,91}},
            {8,{13,65,94}},
            {9,{16,85,97}}
        }
    },
    {
        Odds::Unlikely, {
            {1,{1,5,82}},
            {2,{2,10,83}},
            {3,{3,15,84}},
            {4,{4,20,85}},
            {5,{7,35,88}},
            {6,{10,50,91}},
            {7,{11,55,92}},
            {8,{15,75,96}},
            {9,{18,90,99}}
        }
    },
    {
        Odds::FiftyFifty, {
            {1,{2,10,83}},
            {2,{3,15,84}},
            {3,{5,25,86}},
            {4,{7,35,88}},
            {5,{10,50,91}},
            {6,{13,65,94}},
            {7,{15,75,96}},
            {8,{16,85,97}},
            {9,{19,95,100}}
        }
    },
    {
        Odds::SomewhatLikely, {
            {1,{4,20,85}},
            {2,{5,25,86}},
            {3,{9,45,90}},
            {4,{10,50,91}},
            {5,{13,65,94}},
            {6,{16,80,97}},
            {7,{16,85,97}},
            {8,{18,90,99}},
            {9,{19,95,100}}
        }
    },
    {
        Odds::Likely, {
            {1,{5,25,86}},
            {2,{7,35,88}},
            {3,{10,50,91}},
            {4,{11,55,92}},
            {5,{15,75,96}},
            {6,{16,85,97}},
            {7,{18,90,99}},
            {8,{19,95,100}},
            {9,{20,100,0}}
        }
    },
    {
        Odds::VeryLikely, {
            {1,{9,45,90}},
            {2,{10,50,91}},
            {3,{13,65,94}},
            {4,{15,75,96}},
            {5,{16,85,97}},
            {6,{18,90,99}},
            {7,{19,95,100}},
            {8,{19,95,100}},
            {9,{21,105,0}}
        }
    },
    {
        Odds::NearSureThing, {
            {1,{10,50,91}},
            {2,{11,55,92}},
            {3,{15,75,96}},
            {4,{16,85,97}},
            {5,{18,90,99}},
            {6,{19,95,100}},
            {7,{19,95,100}},
            {8,{20,100,0}},
            {9,{23,115,0}}
        }
    },
    {
        Odds::ASureThing, {
            {1,{11,55,92}},
            {2,{13,65,94}},
            {3,{16,80,97}},
            {4,{16,85,97}},
            {5,{18,90,99}},
            {6,{19,95,100}},
            {7,{19,95,100}},
            {8,{22,110,0}},
            {9,{25,125,0}}
        }
    },
    {
        Odds::HasToBe, {
            {1,{16,80,97}},
            {2,{16,85,97}},
            {3,{19,90,99}},
            {4,{19,95,100}},
            {5,{19,95,100}},
            {6,{20,100,0}},
            {7,{20,100,0}},
            {8,{26,130,0}},
            {9,{26,145,0}}
        }
    }
};

const std::set<int> FateChart::s_diceValuesForRandomEvent {11, 22, 33, 44, 55, 66, 77, 88, 99};

FateChart::ChaosFactor FateChart::s_chaosFactor = 5;
Dice FateChart::s_d100{100, true};

FateChart::Fate FateChart::askFateChart(Odds odds, ChaosFactor chaos) {

    int diceValue { s_d100.getInt() };

    const auto & threshold { s_thresholds.at(odds).at(chaos ? chaos : s_chaosFactor) };

    Fate::Answer answer;

    if (diceValue <= threshold.low) answer = Fate::Answer::ExceptionalYes;
    else if (diceValue <= threshold.middle) answer = Fate::Answer::Yes;
    else if (diceValue < threshold.high) answer = Fate::Answer::No;
    else answer = Fate::Answer::ExceptionalNo;

    std::shared_ptr<RandomEvent::Event> event;

    if (s_diceValuesForRandomEvent.count(diceValue))
         event = std::make_shared<RandomEvent::Event>(RandomEvent::getEvent());


    return Fate{answer, event};
}

int FateChart::increaseChaosFactor() {

    return (s_chaosFactor < 9) ? ++s_chaosFactor : s_chaosFactor;
}

int FateChart::decreaseChaosFactor() {

    return (s_chaosFactor > 1) ? --s_chaosFactor : s_chaosFactor;
}

void FateChart::setChaosFactor(int chaosFactor)
{
    s_chaosFactor = (chaosFactor >= 0) ? ((chaosFactor <= 9) ? chaosFactor : 9) : 1;
}

int FateChart::chaosFactor() {

    return s_chaosFactor;
}


QString FateChart::Fate::answerText(FateChart::Fate::Answer answer)
{

    switch (answer) {
    case Fate::Answer::ExceptionalYes:
        return "EXCEPTIONAL YES";
    case Fate::Answer::Yes:
        return "YES";
    case Fate::Answer::No:
        return "NO";
    case Fate::Answer::ExceptionalNo:
        return "EXCEPTIONAL NO";
    }

    return "";
}
