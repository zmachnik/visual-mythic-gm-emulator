#include "statisticcheckdialog.h"
#include "ui_statisticcheckdialog.h"

#include <QIcon>
#include <cmath>

#include "lists.h"
#include "dice.h"
#include "utils.h"

StatisticCheckDialog::StatisticCheckDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StatisticCheckDialog) {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);

    updateModifier();

    for (const auto & playerCharacter : Lists::playerCharacters())
        ui->playerCharacterCB->addItem(playerCharacter);

    for (const auto & NPC : Lists::allNPCs()) ui->NPCCB->addItem(NPC);

    checkInputs();
}

StatisticCheckDialog::~StatisticCheckDialog() {

    delete ui;
}

const QString &StatisticCheckDialog::statistic() const {

    return m_statistic;
}

void StatisticCheckDialog::accept() {

    return QDialog::accept();
}

void StatisticCheckDialog::checkInputs() {

    if (ui->playerCharacterCB->count()) {
        ui->playerCharacterRB->setEnabled(true);
        ui->playerCharacterCB->setEnabled(ui->playerCharacterRB->isChecked());
    }

    if (ui->NPCCB->count()) {
        if (m_initialize) {
            ui->NPCRB->setChecked(true);
            m_initialize = false;
        }
        ui->NPCCB->setEnabled(ui->NPCRB->isChecked());
        ui->NPCRB->setEnabled(true);
    }

    ui->otherThingLE->setEnabled(ui->otherThingRB->isChecked());
    ui->PCBaselineSB->setEnabled(ui->PCBaselineCB->isChecked());

    ui->checkGB->setEnabled(!(ui->statisticLE->text().isEmpty()
                              || (ui->otherThingRB->isChecked() && ui->otherThingLE->text().isEmpty())));

}

void StatisticCheckDialog::updateModifier() {

    m_modifier = 0;

    switch (ui->attributeStrengthHS->value()) {
    case 0:
        ui->attributeStrengthL->setText("WEAK (-2)");
        m_modifier = -2;
        break;
    case 1:
        ui->attributeStrengthL->setText("NORMAL (+0)");
        break;
    case 2:
        ui->attributeStrengthL->setText("STRONG (+2)");
        m_modifier = 2;
        break;
    case 3:
        ui->attributeStrengthL->setText("PRIME (+4)");
        m_modifier = 4;
        break;
    default:;
    }

    if (ui->importantCB->isChecked()) m_modifier += 2;
}

void StatisticCheckDialog::checkStatistic(){

    ui->subjectGB->setEnabled(false);
    ui->statisticLE->setReadOnly(true);
    ui->modifiersL->setEnabled(false);
    ui->modifiersGB->setEnabled(false);
    ui->checkPB->setEnabled(false);
    ui->expectedBaselineL->setEnabled(false);
    ui->expectedBaselineSB->setEnabled(false);
    ui->PCBaselineCB->setEnabled(false);
    ui->PCBaselineSB->setEnabled(false);
    ui->resultL->setEnabled(true);
    ui->resultLE->setEnabled(true);
    ui->valueL->setEnabled(true);
    ui->valueLCD->setEnabled(true);
    ui->acceptPB->setEnabled(true);

    Dice d10(10, true);

    auto diceValue { d10.getInt() + d10.getInt() + m_modifier };
    int value { 0 };
    int expectedBaseline { ui->expectedBaselineSB->value() };
    int PCBaseline { ui->PCBaselineCB->isChecked() ? ui->PCBaselineSB->value() : expectedBaseline };

    if (diceValue <= 2) {
        ui->resultLE->setText("VERY WEAK -75%");
        value = std::lround(expectedBaseline * 0.25);
    } else if (diceValue <= 4) {
        ui->resultLE->setText("WEAK -50%");
        value = std::lround(expectedBaseline * 0.5);
    } else if (diceValue <= 6) {
        ui->resultLE->setText("LESS -10%");
        value = std::lround(expectedBaseline * 0.9);
    } else if (diceValue <= 11) {
        ui->resultLE->setText("EXPECTED BASELINE");
        value = expectedBaseline;
    } else if (diceValue <= 14) {
        ui->resultLE->setText("MORE +10%");
        value = std::lround(expectedBaseline * 1.1);
    } else if (diceValue <= 16) {
        ui->resultLE->setText("STRONG +50%");
        value = std::lround(expectedBaseline * 1.5);
    } else if (diceValue <= 18) {
        ui->resultLE->setText("VERY STRONG +100%");
        value = std::lround(expectedBaseline * 2);
    } else if (diceValue <= 20) {
        ui->resultLE->setText("PC BASELINE");
        value = std::lround(PCBaseline);
    } else if (diceValue <= 22) {
        ui->resultLE->setText("PC MORE +10%");
        value = std::lround(PCBaseline * 1.1);
    } else if (diceValue <= 24) {
        ui->resultLE->setText("PC STRONG +50%");
        value = std::lround(PCBaseline * 1.5);
    } else {
        ui->resultLE->setText("PC VERY STRONG +100%");
        value = std::lround(PCBaseline * 2);
    }

    m_statistic = "STATISTIC CHECK FOR ";

    if (ui->playerCharacterRB->isChecked()) m_statistic += ui->playerCharacterCB->currentText();
    else if (ui->NPCRB->isChecked()) m_statistic += "NPC " + ui->NPCCB->currentText();
    else m_statistic += ui->otherThingLE->text();

    m_statistic += " (";
    if (ui->importantCB->isChecked()) m_statistic += "IMPORTANT (+2), ";
    m_statistic += ui->attributeStrengthL->text() + ", EB: " + QString::number(expectedBaseline);
    if (ui->PCBaselineCB->isChecked()) m_statistic += ", PCB: " + QString::number(PCBaseline);

    m_statistic += "): " + ui->resultLE->text() + " » " + QString::number(value);

    ui->valueLCD->display(value);
}
