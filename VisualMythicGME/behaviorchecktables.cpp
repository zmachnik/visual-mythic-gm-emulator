#include "behaviorchecktables.h"

Dice BehaviorCheckTables::s_d10{10, true};

QString BehaviorCheckTables::NPCAction1Text(BehaviorCheckTables::NPCAction1 action)
{
    switch (action) {
    case NPCAction1::ThemeAction:
        return "THEME ACTION";
    case NPCAction1::NPCContinues:
        return "NPC CONTINUES";
    case NPCAction1::NPCContinuesPlus2:
        return "NPC CONTINUES +2";
    case NPCAction1::NPCContinuesMinus2:
        return "NPC CONTINUES -2";
    case NPCAction1::NPCAction:
        return "NPC ACTION";
    case NPCAction1::NPCActionMinus4:
        return "NPC ACTION -4";
    case NPCAction1::NPCActionPlus4:
        return "NPC ACTION +4";
    }

    return "";
}

QString BehaviorCheckTables::NPCAction2Text(BehaviorCheckTables::NPCAction2 action) {

    switch (action) {
    case NPCAction2::TalksOrExposition:
        return "TALKS, EXPOSITION";
    case NPCAction2::PerformsAnAmbiguousAction:
        return "PERFORMS AN AMBIGUOUS ACTION";
    case NPCAction2::ActsOutOfPCInterest:
        return "ACTS OUT OF PC INTEREST";
    case NPCAction2::GivesSomething:
        return "GIVES SOMETHING";
    case NPCAction2::SeeksToEndTheEncounter:
        return "SEEKS TO END THE ENCOUNTER";
    case NPCAction2::ChangesTheTheme:
        return "CHANGES THE THEME";
    case NPCAction2::ChangesDescriptor:
        return "CHANGES DESCRIPTOR";
    case NPCAction2::ActosOutOfSelfInterest:
        return "ACTS OUT OF SELF INTEREST";
    case NPCAction2::TakesSomething:
        return "TAKES SOMETHING";
    case NPCAction2::CausesHarm:
        return "CAUSES HARM";
    }

    return "";
}

BehaviorCheckTables::Result BehaviorCheckTables::behaviorCheckTableResult(int modifier) {

    auto diceValue { s_d10.get() };

    BehaviorCheckTables::Result result;

    int table2DiceRollMod { 0 };
    bool rollOnTable2 { false };

    if (diceValue <= 3) {
        result.table1Result = NPCAction1::ThemeAction;
    } else if (diceValue <= 5) {
        result.table1Result = NPCAction1::NPCContinues;
    } else if (diceValue == 6) {
        result.table1Result = NPCAction1::NPCContinuesPlus2;
    } else if (diceValue == 7) {
        result.table1Result = NPCAction1::NPCContinuesMinus2;
    } else if (diceValue == 8) {
        result.table1Result = NPCAction1::NPCAction;
        rollOnTable2 = true;
    } else if (diceValue == 9) {
        result.table1Result = NPCAction1::NPCActionMinus4;
        rollOnTable2 = true;
        table2DiceRollMod = -4;
    } else if (diceValue >= 10) {
        result.table1Result = NPCAction1::NPCActionPlus4;
        rollOnTable2 = true;
        table2DiceRollMod = +4;
    }

    if (rollOnTable2) {
        auto table2Result { std::make_shared<NPCAction2>() };

        auto diceValue2 { s_d10.getInt() + s_d10.getInt() + modifier + table2DiceRollMod };

        if (diceValue2 <= 6) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::TalksOrExposition);
        } else if (diceValue2 <= 8) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::PerformsAnAmbiguousAction);
        } else if (diceValue2 <= 10) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::ActsOutOfPCInterest);
        } else if (diceValue2 == 11) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::GivesSomething);
        } else if (diceValue2 == 12) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::SeeksToEndTheEncounter);
        } else if (diceValue2 == 13) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::ChangesTheTheme);
        } else if (diceValue2 == 14) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::ChangesDescriptor);
        } else if (diceValue2 <= 17) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::ActosOutOfSelfInterest);
        } else if (diceValue2 == 18) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::TakesSomething);
        } else if (diceValue2 >= 19) {
            result.table2Result = std::make_shared<NPCAction2>(NPCAction2::CausesHarm);
        }

    }

    return result;
}

QString BehaviorCheckTables::dispositionTable(int disposition) {

    if (disposition <= 5)  return "PASSIVE";
    if (disposition <= 10) return "MODERATE";
    if (disposition <= 15) return  "ACTIVE";
    return "AGGRESSIVE";
}
