#include "infodialog.h"
#include "ui_infodialog.h"

#include "utils.h"

InfoDialog::InfoDialog(QWidget *parent) :
    QDialog { parent },
    ui { new Ui::InfoDialog } {
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
}

InfoDialog::~InfoDialog() {

    delete ui;
}

void InfoDialog::setTitle(const QString &title)
{
#ifdef Q_OS_ANDROID
    ui->titleL->setText(title);
#else
    setWindowTitle(title);
    ui->titleL->setVisible(false);
#endif
}

void InfoDialog::setMessage(const QString &messageText) {

    ui->messageL->setText(messageText);
}
