#ifndef NEWREGIONSCENEDIALOG_H
#define NEWREGIONSCENEDIALOG_H

#include <QDialog>
#include <QListWidget>
#include <array>
#include "region.h"

namespace Ui {
class NewRegionSceneDialog;
}

class NewRegionSceneDialog : public QDialog {

    Q_OBJECT

public:
    explicit NewRegionSceneDialog(QWidget *parent, const std::array<const QListWidget *, 3> elementLists,  Region & region);
    virtual ~NewRegionSceneDialog() override;

protected slots:

    virtual void accept() override;

private:

    Ui::NewRegionSceneDialog *ui;
    const std::array<const QListWidget *, 3> p_elementLists;
    Region m_region;
    Region & r_region;
    Region::Scene m_newScene;

    unsigned m_locationProgress {0};
    unsigned m_encounterProgress {0};
    unsigned m_objectProgress {0};

    QString m_returnLocation;

    int m_chosenLocation = -1;
    int m_chosenEncounter = -1;
    int m_chosenObject = -1;

private slots:

    void rollElements();
    void checkInputs();
    void resolve(Region::Element & element);
    void chooseOther(Region::ElementCategory category);
};

#endif // NEWREGIONSCENEDIALOG_H
