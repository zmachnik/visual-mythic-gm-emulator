#ifndef DICEDIALOG_H
#define DICEDIALOG_H

#include <map>
#include <QDialog>

namespace Ui {
class DiceDialog;
}

class DiceDialog : public QDialog {

    Q_OBJECT

public:

    explicit DiceDialog(QWidget *parent = nullptr);
    virtual ~DiceDialog() override;

private:

    Ui::DiceDialog * ui;
    std::map<unsigned,int> diceSet;

    void updateUI();

private slots:

    void addDice();
    void throwDice();
    void reset();
};

#endif // DICEDIALOG_H
