#include "lists.h"

#include <algorithm>

std::set<QString> Lists::s_playerCharacters;
std::set<QString> Lists::s_NPCs;
std::set<QString> Lists::s_threads;
std::map<QString,std::set<QString>> Lists::s_playerNPCs;
std::map<QString,std::set<QString>> Lists::s_playerThreads;
std::map<QString,QString> Lists::s_PCBackstories;
std::map<QString,NPCDescriptors> Lists::s_NPCDescriptors;
std::vector<Region> Lists::s_regions;

bool Lists::addPlayerCharacter(const QString & player) {

    if (s_playerCharacters.insert(player).second) {
        s_playerNPCs[player] = std::set<QString>{};
        s_playerThreads[player] = std::set<QString>{};
        return true;
    }

    return false;
}

bool Lists::renamePlayerCharacter(const QString & player, const QString & newName) {

    if (s_playerCharacters.find(newName) != s_playerCharacters.end()) return false;

    ////

    if (s_playerCharacters.erase(player)) s_playerCharacters.insert(newName);
    else return false;

    for (auto & NPCs : s_playerNPCs) {
        if (NPCs.first == player) {
            auto personalNPCs {NPCs.second};
            if (s_playerNPCs.erase(player)) {
                s_playerNPCs[newName] = personalNPCs;
            }
        }
    }

    for (auto & threads : s_playerThreads) {
        if (threads.first == player) {
            auto personalThreads {threads.second};
            if (s_playerThreads.erase(player)) {
                s_playerThreads[newName] = personalThreads;
            }
        }
    }

    return true;
}

bool Lists::removePlayerCharacter(const QString & player) {

    if (s_playerCharacters.erase(player)) {
        s_playerNPCs.erase(player);
        s_playerThreads.erase(player);
        return true;
    }

    return false;
}

bool Lists::addNPC(const QString & NPC) {

    return(s_NPCs.insert(NPC).second);
}

bool Lists::renameNPC(const QString & NPC, const QString & newName) {

    if (s_NPCs.find(newName) == s_NPCs.end()) {
        for (const auto & NPCs : s_playerNPCs)
            if (NPCs.second.find(newName) != NPCs.second.end()) return false;
    } else return false;

    ////

    bool renamed { false };

    if (s_NPCs.erase(NPC)) {
        s_NPCs.insert(newName);
        renamed = true;
    }

    for (auto & NPCs : s_playerNPCs) {
        if (NPCs.second.erase(NPC)) {
            NPCs.second.insert(newName);
            renamed = true;
        }
    }

    if (renamed) {
        auto it {s_NPCDescriptors.find(NPC)};
        if (it != s_NPCDescriptors.end()) {
            auto NPCDescriptors { it->second };
            s_NPCDescriptors.erase(NPC);
            s_NPCDescriptors[newName] = NPCDescriptors;
        }
    }

    return renamed;
}

bool Lists::removeNPC(const QString & NPC) {

    return(s_NPCs.erase(NPC));
}

bool Lists::addThread(const QString & thread) {

    return(s_threads.insert(thread).second);
}

bool Lists::renameThread(const QString & thread, const QString & newName) {

    if (s_threads.find(newName) == s_threads.end()) {
        for (const auto & threads : s_playerThreads)
            if (threads.second.find(newName) != threads.second.end()) return false;
    } else return false;

    ////

    bool renamed { false };

    if (s_threads.erase(thread)) {
        s_threads.insert(newName);
        renamed = true;
    }

    for (auto & threads : s_playerThreads) {
        if (threads.second.erase(thread)) {
            threads.second.insert(newName);
            renamed = true;
        }
    }

    return renamed;
}

bool Lists::removeThread(const QString & thread) {

    return(s_threads.erase(thread));
}

bool Lists::addPersonalNPC(const QString & player, const QString & NPC) {

    if (s_playerNPCs.find(player) == s_playerNPCs.end()) return false;
    return (s_playerNPCs[player].insert(NPC).second);
}

bool Lists::removePersonalNPC(const QString & player, const QString & NPC) {

    if (s_playerNPCs.find(player) == s_playerNPCs.end()) return false;
    return (s_playerNPCs[player].erase(NPC));
}

bool Lists::addPersonalThread(const QString & player, const QString & thread) {

    if (s_playerThreads.find(player) == s_playerThreads.end()) return false;
    return (s_playerThreads[player].insert(thread).second);
}

bool Lists::removePersonalThread(const QString & player, const QString & thread) {

    if (s_playerThreads.find(player) == s_playerThreads.end()) return false;
    return (s_playerThreads[player].erase(thread));
}

bool Lists::setNPCDescriptors(const QString & NPC, const NPCDescriptors & descriptors) {

    if (s_NPCs.find(NPC) == s_NPCs.end()) return false;

    s_NPCDescriptors[NPC] = descriptors;
    return true;
}

bool Lists::removeNPCDescriptors(const QString & NPC) {

    if (s_NPCs.find(NPC) == s_NPCs.end()) return false;

    return s_NPCDescriptors.erase(NPC);
}

bool Lists::addRegion(const QString &region) {

    for ( auto it { s_regions.begin() }; it != s_regions.end(); ++it )
        if (it->name() == region) return false;

    s_regions.emplace_back(Region { region });
    return true;
}

bool Lists::insertRegion(const Region &region) {

    for ( auto it { s_regions.begin() }; it != s_regions.end(); ++it )
        if (it->name() == region.name()) return false;

    s_regions.push_back( region );
    return true;
}

bool Lists::renameRegion(const QString & region, const QString & newName) {

    for ( auto it { s_regions.begin() }; it != s_regions.end(); ++it )
        if (it->name() == newName) return false;

    for ( auto it { s_regions.begin() }; it != s_regions.end(); ++it ) {

        if (it->name() == region) {
            it->setName(newName);
            return true;
        }
    }

    return false;
}

bool Lists::removeRegion(const QString & region) {

    for ( auto it { s_regions.begin() }; it != s_regions.end(); ++it ) {

        if (it->name() == region) {
            it = s_regions.erase(it);
            return true;
        }
    }

    return false;
}

const std::set<QString> & Lists::playerCharacters() {

    return s_playerCharacters;
}

const std::set<QString> & Lists::NPCs() {

    return s_NPCs;
}

const std::set<QString> & Lists::threads() {

    return s_threads;
}

const std::vector<Region> & Lists::regions() {

    return s_regions;
}

Region * Lists::region(const QString regionName) {

    for ( auto it { s_regions.begin() }; it != s_regions.end(); ++it )
        if (it->name() == regionName) return &(*it);

    return nullptr;
}

std::set<QString> Lists::allPersonalNPCs() {

    std::set<QString> NPCsSet;

    for (const auto & NPCs : s_playerNPCs)
        NPCsSet.insert(NPCs.second.begin(), NPCs.second.end());

    return NPCsSet;
}

std::set<QString> Lists::allNPCs() {

    auto NPCsSet { s_NPCs };
    auto personalNPCs { allPersonalNPCs() };

    NPCsSet.insert(personalNPCs.begin(), personalNPCs.end());

    return NPCsSet;
}

std::set<QString> Lists::allPersonalThreads() {

    std::set<QString> threadsSet;

    for (const auto & threads : s_playerThreads)
        threadsSet.insert(threads.second.begin(), threads.second.end());

    return threadsSet;
}

std::set<QString> Lists::allThreads() {

    auto threadsSet { s_threads };
    auto personalThreads { allPersonalThreads() };

    threadsSet.insert(personalThreads.begin(), personalThreads.end());

    return threadsSet;
}

const std::set<QString> * Lists::personalNPCs(const QString & player) {

    const auto & NPCsSet { s_playerNPCs.find(player) };

    if (NPCsSet != s_playerNPCs.end())
        return &(NPCsSet->second);
    else
        return nullptr;
}

const std::set<QString> * Lists::personalThreads(const QString & player) {

    const auto & threadsSet { s_playerThreads.find(player) };

    if (threadsSet != s_playerThreads.end())
        return &(threadsSet->second);
    else
        return nullptr;
}

QString Lists::PCBackstory(const QString & player) {

    auto story { s_PCBackstories.find(player) };
    if (story == s_PCBackstories.end()) return "";
    else return story->second;
}

void Lists::setPCBackstory(const QString & player, const QString & story) {

    s_PCBackstories[player] = story;
}

NPCDescriptors * Lists::descriptors(const QString & NPC) {
    if (s_NPCDescriptors.find(NPC) == s_NPCDescriptors.end()) return nullptr;
    return &s_NPCDescriptors[NPC];
}

void Lists::clear() {

    s_NPCs.clear();
    s_playerNPCs.clear();
    s_playerThreads.clear();
    s_playerCharacters.clear();
    s_threads.clear();
    s_NPCDescriptors.clear();
    s_regions.clear();
}
