#include "detailcheckdialog.h"
#include "ui_detailcheckdialog.h"

#include "utils.h"
#include "lists.h"
#include "fatechart.h"
#include "meaningtables.h"

DetailCheckDialog::DetailCheckDialog(QWidget *parent) :
    QDialog(parent),
    ui { new Ui::DetailCheckDialog },
    m_d100 { 100, true } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->questionPTE);
}

DetailCheckDialog::~DetailCheckDialog() {

    delete ui;
}

const QString &DetailCheckDialog::question() const
{
    return m_question;
}

const DetailCheckTable::Result & DetailCheckDialog::result() const
{
    return m_result;
}

void DetailCheckDialog::accept() {

    if (m_result.target->isEmpty()) {
        auto selectedCharacter { ui->charactersCB->currentText() };
        if ( selectedCharacter == "<RANDOM>")
            m_result.target = std::make_shared<QString>(Utils::randomPlayerCharacter());
        else
            m_result.target = std::make_shared<QString>(selectedCharacter);
    }

    return QDialog::accept();
}

void DetailCheckDialog::checkQuestion()
{
    ui->checkPB->setEnabled(!ui->questionPTE->toPlainText().isEmpty());
}

void DetailCheckDialog::detailCheck() {

    m_question = ui->questionPTE->toPlainText();
    m_result = DetailCheckTable::detailCheckTableResult();

    auto resultText { m_result.tableResult };

    if (*m_result.target == "") {

        ui->charactersCB->addItem("<RANDOM>");

        for (const auto & character : Lists::playerCharacters())
            ui->charactersCB->addItem(character);

        ui->characterGB->setEnabled(true);
        ui->characterL->setEnabled(true);
        ui->charactersCB->setEnabled(true);
    }
    else  {
        resultText += " : " + *m_result.target;

        ui->characterGB->setEnabled(false);
        ui->characterL->setEnabled(false);
        ui->charactersCB->setEnabled(false);
    }

    ui->detailCheckResultLE->setText(resultText);

    ui->questionGB->setEnabled(false);
    ui->resultGB->setEnabled(true);
    ui->descriptionGB->setEnabled(true);
    ui->actionGB->setEnabled(true);
    ui->acceptPB->setEnabled(true);
}

void DetailCheckDialog::rollDescription() {
    const auto & description1 { MeaningTables::descriptionTable1.at(m_d100.get() - 1) };
    const auto & description2 { MeaningTables::descriptionTable2.at(m_d100.get() - 1) };

    ui->descriptionLE->setText(description1 + " : " + description2);
    ui->actionGB->setEnabled(false);
    ui->rollDescriptionPB->setEnabled(false);

    m_result.meaning.first  = description1;
    m_result.meaning.second = description2;
}

void DetailCheckDialog::rollAction() {
    const auto & action1 { MeaningTables::actionTable1.at(m_d100.get() - 1) };
    const auto & action2 { MeaningTables::actionTable2.at(m_d100.get() - 1) };

    ui->actionLE->setText(action1 + " : " + action2);
    ui->descriptionGB->setEnabled(false);
    ui->rollActionPB->setEnabled(false);

    m_result.meaning.first  = action1;
    m_result.meaning.second = action2;
}
