#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include "utils.h"

SettingsDialog::SettingsDialog(QWidget *parent,
                               bool detailCheckEnabled,
                               bool behaviorCheckEnabled,
                               bool eventCheckEnabled,
                               bool statisticCheckEnabled,
                               bool RPGCheckEnabled,
                               bool offCameraStoriesEnabled,
                               bool locationCrafterEnabled,
                               bool randomDescriptionEnabled,
                               int globalFontSize) :
    QDialog { parent },
    ui { new Ui::SettingsDialog } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    QString version = "VERSION " + Utils::version;
#ifdef Q_OS_WASM
    version += " FOR WEBASSEMBLY";
#endif
    ui->versionL->setText(version);

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->booksSA);
    Utils::scrollWithSwipe(ui->aboutPTE);

    setBlockedCheckbox(ui->fateChartCB);
    setBlockedCheckbox(ui->randomEventsCB);
    setBlockedCheckbox(ui->scenesCB);
    setBlockedCheckbox(ui->diceCB);

    ui->detailCheckCB->setChecked(detailCheckEnabled);
    ui->behaviorCheckCB->setChecked(behaviorCheckEnabled);
    ui->eventCheckCB->setChecked(eventCheckEnabled);
    ui->statisticCheckCB->setChecked(statisticCheckEnabled);
    ui->RPGSystemCheckCB->setChecked(RPGCheckEnabled);
    ui->offCameraStoriesCB->setChecked(offCameraStoriesEnabled);
    ui->locationCrafterCB->setChecked(locationCrafterEnabled);
    ui->randomDescriptionCB->setChecked(randomDescriptionEnabled);

    if (!globalFontSize) {
        ui->defaultFontSizesRB->setChecked(true);
        ui->fontSizeS->setValue(12);
        ui->fontSizeGB->setEnabled(false);
    } else {
        ui->manualFontSizeRB->setChecked(true);
        ui->fontSizeS->setValue(globalFontSize);
        ui->fontSizeGB->setEnabled(true);
    }
}

SettingsDialog::~SettingsDialog() {

    delete ui;
}

bool SettingsDialog::detailCheckEnabled() const {

    return m_detailCheckEnabled;
}

bool SettingsDialog::behaviorCheckEnabled() const {

    return m_behaviorCheckEnabled;
}

bool SettingsDialog::eventCheckEnabled() const
{
    return m_eventCheckEnabled;
}

bool SettingsDialog::statisticCheckEnabled() const
{
    return m_statisticCheckEnabled;
}

bool SettingsDialog::RPGCheckEnabled() const {

    return m_RPGCheckEnabled;
}

bool SettingsDialog::offCameraStoriesEnabled() const {

    return m_offCameraStoriesEnabled;
}

bool SettingsDialog::locationCrafterEnabled() const {

    return m_locationCrafterEnabled;
}

bool SettingsDialog::randomDescriptionEnabled() const {

    return m_randomDescriptionEnabled;
}

int SettingsDialog::globalFontSize() const {

    return m_globalFontSize;
}

void SettingsDialog::accept() {

    m_detailCheckEnabled = ui->detailCheckCB->isChecked();
    m_behaviorCheckEnabled = ui->behaviorCheckCB->isChecked();
    m_eventCheckEnabled = ui->eventCheckCB->isChecked();
    m_statisticCheckEnabled = ui->statisticCheckCB->isChecked();
    m_RPGCheckEnabled = ui->RPGSystemCheckCB->isChecked();
    m_offCameraStoriesEnabled = ui->offCameraStoriesCB->isChecked();
    m_locationCrafterEnabled = ui->locationCrafterCB->isChecked();
    m_randomDescriptionEnabled = ui->randomDescriptionCB->isChecked();
    if (ui->manualFontSizeRB->isChecked()) m_globalFontSize = ui->fontSizeS->value();
    else m_globalFontSize = 0;

    QDialog::accept();
}

void SettingsDialog::setBlockedCheckbox(QCheckBox * checkBox) {

    checkBox->setCheckState(Qt::PartiallyChecked);
    checkBox->setAttribute(Qt::WA_TransparentForMouseEvents, true);
    checkBox->setFocusPolicy(Qt::NoFocus);
}

void SettingsDialog::changeFontSize() {

    ui->fontSizeL->setText(QString::number(ui->fontSizeS->value()));
}
