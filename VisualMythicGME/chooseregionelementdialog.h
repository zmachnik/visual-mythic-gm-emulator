#ifndef CHOOSEREGIONELEMENTDIALOG_H
#define CHOOSEREGIONELEMENTDIALOG_H

#include <QDialog>
#include <QListWidget>

#include <region.h>

namespace Ui {
class ChooseRegionElementDialog;
}

class ChooseRegionElementDialog : public QDialog {

    Q_OBJECT

public:

    explicit ChooseRegionElementDialog(QWidget * parent, const QListWidget * elementList, const Region & region, Region::ElementCategory category);
    virtual ~ChooseRegionElementDialog() override;

    int row() const;

protected slots:

    virtual void accept() override;

private:

    Ui::ChooseRegionElementDialog *ui;

    const Region & r_region;
    Region::ElementCategory m_category;

    int m_row { -1 };

private slots:

    void updateUI();
};

#endif // CHOOSEREGIONELEMENTDIALOG_H
