#include "region.h"

#include <algorithm>

Region::Region(const QString & name)
    : m_name { name } {

}

QString Region::name() const {

    return m_name;
}

Region::Element::Element(const ElementCategory category, const ElementType type,
                         SpecialElementType specialType, bool unique, const QString &name)
    : category { category }, type { type }, specialType { specialType }, unique { unique }, name { name } {}

void Region::setName(const QString & name) {

    m_name = name;
}

Region::Scene & Region::scene() {

    return m_scene;
}

const Region::Scene & Region::constScene() const {

    return m_scene;
}

QString Region::ElementCategoryText(ElementCategory category) {

    switch (category) {

    case ElementCategory::Location:  return "LOCATION";
    case ElementCategory::Encounter: return "ENCOUNTER";
    case ElementCategory::Object:    return "OBJECT";

    default: return "";
    }
}

Region::ElementCategory Region::parseElementCategory(const QString &str) {

    if (str == "LOCATION") return ElementCategory::Location;
    if (str == "ENCOUNTER") return ElementCategory::Encounter;
    if (str == "OBJECT") return ElementCategory::Object;

    return Region::ElementCategory::Null;
}

QString Region::ElementTypeText(ElementType type, bool noSpecial) {

    switch (type) {

    case ElementType::Expected: return "EXPECTED";
    case ElementType::Custom:   return "CUSTOM";
    case ElementType::Special:  if (!noSpecial) return "SPECIAL";
                                else return "SPECIAL » EXPECTED";
    case ElementType::Random:   return "RANDOM";
    case ElementType::None:     return "NONE";
    case ElementType::Complete: return "COMPLETE";

    default: return "";
    }
}

Region::ElementType Region::parseElementType(const QString & str) {

    if (str == "EXPECTED") return ElementType::Expected;
    if (str == "CUSTOM")   return ElementType::Custom;
    if (str == "SPECIAL")  return ElementType::Special;
    if (str == "RANDOM")   return ElementType::Random;
    if (str == "NONE")     return ElementType::None;
    if (str == "COMPLETE") return ElementType::Complete;

    return ElementType::Null;
}

QString Region::SpecialElementText(SpecialElementType type, bool notLocation) {

    switch (type) {

    case SpecialElementType::SuperSize:     return "SUPERSIZE";
    case SpecialElementType::BarelyThere:   return "BARELY THERE";
    case SpecialElementType::RemoveElement: return "REMOVE ELEMENT";
    case SpecialElementType::AddElement:    return "ADD ELEMENT";
    case SpecialElementType::ThisIsBad:     return "THIS IS BAD";
    case SpecialElementType::ThisIsGood:    return "THIS IS GOOD";
    case SpecialElementType::MultiElement:  return "MULTI ELEMENT";
    case SpecialElementType::ExitHere:      return notLocation ? "EXIT HERE » EXPECTED" : "EXIT HERE";
    case SpecialElementType::Return:        return notLocation ? "RETURN » EXPECTED" : "RETURN";
    case SpecialElementType::GoingDeeper:   return "GOING DEEPER";
    case SpecialElementType::CommonGround:  return "COMMON GROUND";
    case SpecialElementType::RandomElement: return "RANDOM ELEMENT";

    default: return "";
    }
}

const std::vector<Region::Element> & Region::locations() const {

    return m_locations;
}

const std::vector<Region::Element> & Region::encounters() const {

    return m_encounters;
}

const std::vector<Region::Element> & Region::objects() const {

    return m_objects;
}

unsigned Region::locationPPs() const {

    return m_locationPPs;
}

unsigned Region::encounterPPs() const {

    return m_encounterPPs;
}

unsigned Region::objectPPs() const {

    return(m_objectPPs);
}

void Region::setLocationPPs(int value) {

    m_locationPPs = static_cast<unsigned>(value);
}

void Region::setEncounterPPs(int value) {

    m_encounterPPs = static_cast<unsigned>(value);
}

void Region::setObjectPPs(int value) {

    m_objectPPs = static_cast<unsigned>(value);
}

void Region::setLocationPPs(unsigned value) {

    m_locationPPs = value;
}

void Region::setEncounterPPs(unsigned value) {

    m_encounterPPs = value;
}

void Region::setObjectPPs(unsigned value) {

    m_objectPPs = value;
}

void Region::addElement(ElementCategory category, const ElementType type,
                        const SpecialElementType specialType, bool unique, const QString &name) {

    Element element { category, type, specialType, unique, name };
    addElement(std::move(element));
}

void Region::addElement(Element element) {

    if (element.category == ElementCategory::Location) {
        m_locations.push_back(std::move(element));
    } else if (element.category == ElementCategory::Encounter) {
        m_encounters.push_back(std::move(element));
    } else {
        m_objects.push_back(std::move(element));
    }
}

bool Region::removeElement(ElementCategory category, unsigned index) {

    const auto offset { static_cast<unsigned>(index) };

    std::vector<Element> & elements {
        (category == ElementCategory::Location) ? m_locations :
        (category == ElementCategory::Encounter) ? m_encounters : m_objects };

    if (elements.size() <= offset) return false;
        elements.erase(elements.begin() + offset);

        return true;
    }

    bool Region::removeElement(Region::ElementCategory category, const QString &name) {

        std::vector<Element> & elements {
            (category == ElementCategory::Location) ? m_locations :
            (category == ElementCategory::Encounter) ? m_encounters : m_objects };

        for (unsigned i{0}; i<elements.size(); ++i) {
            if (elements[i].name == name) {
                elements.erase(elements.begin() + i);
                return true;
            }
        }

        return false;
    }

int Region::moveElement(Region::ElementCategory category, int index, bool up) {

    const auto offset { static_cast<unsigned>(index) };

    std::vector<Element> & elements {
        (category == ElementCategory::Location) ? m_locations :
        (category == ElementCategory::Encounter) ? m_encounters : m_objects };

    if ((up && offset < 1) || (!up && offset >= elements.size()-1)) return -1;

    const int shift { up ? -1 : +1 };

    std::swap(elements[offset], elements[static_cast<unsigned>(index + shift)]);

    return index + shift;
}

const std::multimap<QString, QString> & Region::regionMap() const {

   return m_regionMap;
}

void Region::addMapLocation(QString fromLocation, const QString & newLocation) {

    if (m_regionMap.empty()) {
        m_regionMap.insert({"#NULL#", "<EXIT>"});
        m_regionMap.insert({"<EXIT>", "#NULL#"});
    }

    m_regionMap.insert({"#NULL#", newLocation});
    m_regionMap.insert({newLocation, "#NULL#"});

    if (fromLocation.isEmpty()) fromLocation = "<EXIT>";

    m_regionMap.insert({fromLocation, newLocation});
    m_regionMap.insert({newLocation, fromLocation});
}

void Region::addMapConnection(QString from, const QString &to) {

    m_regionMap.insert({from, to});
}

void Region::removeMapConnection(const QString &from, const QString &to) {

    const auto range { m_regionMap.equal_range(from) };

    for (auto it {range.first}; it != range.second; ++it) {
        if (it->second == to) {
            m_regionMap.erase(it);
            break;
        }
    }
}

std::set<QString> Region::mapLocations() const {

    std::set<QString> locations;

    for (const auto & connection : m_regionMap) locations.insert(connection.first);

    return locations;
}

std::set<QString> Region::connectionsFrom(const QString & mapLocation) const {

    const auto range { m_regionMap.equal_range(mapLocation) };
    std::set<QString> locations;

    for (auto it { range.first }; it != range.second; ++it ) locations.insert(it->second);

    return locations;
}

const QString & Region::currentMapLocation() const {

    return m_currentMapLocation;
}

void Region::setCurrentMapLocation(const QString & location) {

    if (mapLocations().count(location)) m_currentMapLocation = location;
}

bool Region::operator<(const Region &r2) const {

    return m_name < r2.m_name;
}
