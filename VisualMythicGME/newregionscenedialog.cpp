#include "newregionscenedialog.h"
#include "ui_newregionscenedialog.h"

#include <QIcon>
#include "dice.h"
#include "specialelementdialog.h"
#include "chooseregionelementdialog.h"
#include "utils.h"

NewRegionSceneDialog::NewRegionSceneDialog(QWidget * parent, const std::array<const QListWidget *, 3> elementLists, Region & region) :
    QDialog(parent),
    ui(new Ui::NewRegionSceneDialog),
    p_elementLists { elementLists }, m_region { region }, r_region { region } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() | Qt::WindowMinimizeButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);
    Utils::scrollWithSwipe(ui->locationPTE);
    Utils::scrollWithSwipe(ui->encounterPTE);
    Utils::scrollWithSwipe(ui->objectPTE);

    connect(ui->resolveLocationPB, & QPushButton::clicked, [this] { resolve(m_newScene.location); });
    connect(ui->resolveEncounterPB, & QPushButton::clicked, [this] { resolve(m_newScene.encounter); });
    connect(ui->resolveObjectPB, & QPushButton::clicked, [this] { resolve(m_newScene.object); });

    connect(ui->otherLocationPB, & QPushButton::clicked, [this] { chooseOther(Region::ElementCategory::Location); });
    connect(ui->otherEncounterPB, & QPushButton::clicked, [this] { chooseOther(Region::ElementCategory::Encounter); });
    connect(ui->otherObjectPB, & QPushButton::clicked, [this] { chooseOther(Region::ElementCategory::Object); });
}

NewRegionSceneDialog::~NewRegionSceneDialog() {

    delete ui;
}

void NewRegionSceneDialog::accept() {

    const auto allLocations { r_region.mapLocations() };
    if (allLocations.find(ui->locationPTE->toPlainText()) != allLocations.end()) {
        Utils::showErrorMessage("LOCATION " + ui->locationPTE->toPlainText() + " DOES ALREADY EXIST IN REGION "
                                + r_region.name() + "!\nCHOOSE AN ANOTHER NAME.");
        return;
    }

    r_region = m_region;  

    m_newScene.location.name = ui->locationPTE->toPlainText();
    m_newScene.encounter.name = ui->encounterPTE->toPlainText();
    m_newScene.object.name = ui->objectPTE->toPlainText();

    m_newScene.locationExactType = ui->locationTypeLE->text();
    m_newScene.encounterExactType = ui->encounterTypeLE->text();
    m_newScene.objectExactType = ui->objectTypeLE->text();

    if (m_newScene.location.unique || m_newScene.location.specialType == Region::SpecialElementType::RemoveElement) r_region.removeElement(Region::ElementCategory::Location, m_locationProgress);
    if (m_newScene.encounter.unique || m_newScene.encounter.specialType == Region::SpecialElementType::RemoveElement) r_region.removeElement(Region::ElementCategory::Encounter, m_encounterProgress);
    if (m_newScene.object.unique || m_newScene.object.specialType == Region::SpecialElementType::RemoveElement) r_region.removeElement(Region::ElementCategory::Object, m_objectProgress);

    if (m_newScene.location.specialType == Region::SpecialElementType::AddElement)
        r_region.addElement(Region::ElementCategory::Location, Region::ElementType::Custom, Region::SpecialElementType::Null, false, m_newScene.location.name);
    if (m_newScene.encounter.specialType == Region::SpecialElementType::AddElement)
        r_region.addElement(Region::ElementCategory::Encounter, Region::ElementType::Custom, Region::SpecialElementType::Null, false, m_newScene.encounter.name);
    if (m_newScene.object.specialType == Region::SpecialElementType::AddElement)
        r_region.addElement(Region::ElementCategory::Object, Region::ElementType::Custom, Region::SpecialElementType::Null, false, m_newScene.object.name);

    if (m_newScene.location.specialType == Region::SpecialElementType::GoingDeeper)
        r_region.setLocationPPs(r_region.locationPPs() + 2);
    if (m_newScene.encounter.specialType == Region::SpecialElementType::GoingDeeper)
        r_region.setEncounterPPs(r_region.encounterPPs() + 2);
    if (m_newScene.location.specialType == Region::SpecialElementType::GoingDeeper)
        r_region.setObjectPPs(r_region.objectPPs() + 2);

    if (m_newScene.location.specialType == Region::SpecialElementType::CommonGround)
        r_region.setLocationPPs(Utils::substractNoNegative(r_region.locationPPs(), 4));
    if (m_newScene.encounter.specialType == Region::SpecialElementType::CommonGround)
        r_region.setEncounterPPs(Utils::substractNoNegative(r_region.encounterPPs(), 4));
    if (m_newScene.object.specialType == Region::SpecialElementType::CommonGround)
        r_region.setObjectPPs(Utils::substractNoNegative(r_region.objectPPs(), 4));

    if (m_newScene.location.specialType == Region::SpecialElementType::ExitHere) {
        r_region.addMapConnection(m_newScene.location.name, "<EXIT>");
        r_region.addMapConnection("<EXIT>", m_newScene.location.name);
    }

    if (m_newScene.location.specialType == Region::SpecialElementType::Return && !m_returnLocation.isEmpty()) {
        r_region.addMapConnection(m_newScene.location.name, m_returnLocation);
        r_region.addMapConnection(m_returnLocation, m_newScene.location.name);
    }

    const auto & currentLocation { ui->locationPTE->toPlainText() };
    r_region.addMapLocation(m_region.currentMapLocation(), currentLocation);
    r_region.setCurrentMapLocation(currentLocation);

    r_region.scene() = m_newScene;

    return QDialog::accept();
}

void NewRegionSceneDialog::rollElements() {

    ui->rollElementsPB->setEnabled(false);

    Dice d6 {6};
    m_newScene = Region::Scene{};
    m_newScene.active = true;

    m_locationProgress = (m_chosenLocation >= 0) ? static_cast<unsigned>(m_chosenLocation) : (d6.get() + m_region.locationPPs());
    if (m_region.locations().size() > m_locationProgress) {
        m_newScene.location = m_region.locations().at(m_locationProgress);
        m_region.setLocationPPs(m_region.locationPPs() + 1);
    } else {
        m_newScene.location = Region::Element { Region::ElementCategory::Location };
        m_region.setLocationPPs(Utils::substractNoNegative(m_region.locationPPs(), 5));
    }

    m_encounterProgress = (m_chosenEncounter >=0 ) ? static_cast<unsigned>(m_chosenEncounter) : ( d6.get() + m_region.encounterPPs());
    if (m_region.encounters().size() > m_encounterProgress) {
        m_newScene.encounter = m_region.encounters().at(m_encounterProgress);
        m_region.setEncounterPPs(m_region.encounterPPs() + 1);
    } else {
        m_newScene.location = Region::Element { Region::ElementCategory::Encounter };
        m_region.setEncounterPPs(Utils::substractNoNegative(m_region.encounterPPs(), 5));
    }

    m_objectProgress = (m_chosenObject >= 0) ? static_cast<unsigned>(m_chosenObject) : (d6.get() + m_region.objectPPs());
    if (m_region.objects().size() > m_objectProgress) {
        m_newScene.object = m_region.objects().at(m_objectProgress);
        m_region.setObjectPPs(m_region.objectPPs() + 1);
    } else {
        m_newScene.location = Region::Element { Region::ElementCategory::Object };
        m_region.setObjectPPs(Utils::substractNoNegative(m_region.objectPPs(), 5));
    }

    ui->locationTypeLE->setText(Region::ElementTypeText(m_newScene.location.type));
    ui->encounterTypeLE->setText(Region::ElementTypeText(m_newScene.encounter.type));
    ui->objectTypeLE->setText(Region::ElementTypeText(m_newScene.object.type));

    if (m_newScene.location.type == Region::ElementType::Expected
        || m_newScene.location.type == Region::ElementType::Complete) {
        ui->locationPTE->setEnabled(true);
        ui->locationPTE->setReadOnly(false);
    }
    else if (m_newScene.location.type == Region::ElementType::Complete) ui->locationPTE->setPlainText("COMPLETE");
    else if (m_newScene.location.type == Region::ElementType::None) {

        ui->locationPTE->setPlainText("NONE");
        ui->resolveLocationPB->setEnabled(false);
    }
    else if (m_newScene.location.type == Region::ElementType::Custom) {

        ui->locationPTE->setPlainText(m_newScene.location.name);
        ui->locationPTE->setEnabled(true);
        ui->locationPTE->setReadOnly(false);
    }
    else ui->resolveLocationPB->setEnabled(true);

    if (m_newScene.encounter.type == Region::ElementType::Expected) {

        ui->encounterPTE->setEnabled(true);
        ui->encounterPTE->setReadOnly(false);
    }
    else if (m_newScene.encounter.type == Region::ElementType::None) {

        ui->encounterPTE->setPlainText("NONE");
        ui->encounterPTE->setEnabled(false);

    } else if (m_newScene.encounter.type == Region::ElementType::Custom) {

        ui->encounterPTE->setPlainText(m_newScene.encounter.name);
        ui->encounterPTE->setEnabled(true);
        ui->encounterPTE->setReadOnly(false);

    } else ui->resolveEncounterPB->setEnabled(true);

    if (m_newScene.object.type == Region::ElementType::Expected) {

        ui->objectPTE->setEnabled(true);
        ui->objectPTE->setReadOnly(false);

    } else if (m_newScene.object.type == Region::ElementType::None) {

        ui->objectPTE->setPlainText("NONE");
        ui->objectPTE->setEnabled(false);
    } else if (m_newScene.object.type == Region::ElementType::Custom) {

        ui->objectPTE->setPlainText(m_newScene.object.name);
        ui->objectPTE->setEnabled(true);
        ui->objectPTE->setReadOnly(false);

    } else ui->resolveObjectPB->setEnabled(true);
}

void NewRegionSceneDialog::checkInputs() {

    ui->acceptPB->setEnabled(
            (m_newScene.location.type == Region::ElementType::None || !ui->locationPTE->toPlainText().isEmpty()) &&
            (m_newScene.encounter.type == Region::ElementType::None || !ui->encounterPTE->toPlainText().isEmpty()) &&
            (m_newScene.object.type == Region::ElementType::None || !ui->objectPTE->toPlainText().isEmpty()));
}

void NewRegionSceneDialog::resolve(Region::Element & element) {

    auto sed { new SpecialElementDialog { nullptr, m_region, element } } ;

    connect(sed, &QDialog::finished, this, [=](int dc) {

        if (dc == QDialog::Rejected) return;

        if (element.category == Region::ElementCategory::Location) {
            ui->resolveLocationPB->setEnabled(false);
            ui->locationPTE->setEnabled(true);
            ui->locationTypeLE->setText(sed->specialType());
            ui->locationPTE->setPlainText(sed->interpretation());
            m_returnLocation = sed->returnLocation();
        }
        else if (element.category == Region::ElementCategory::Encounter) {
            ui->resolveEncounterPB->setEnabled(false);
            ui->encounterPTE->setEnabled(true);
            ui->encounterTypeLE->setText(sed->specialType());
            ui->encounterPTE->setPlainText(sed->interpretation());
        }
        else {
            ui->resolveObjectPB->setEnabled(false);
            ui->objectPTE->setEnabled(true);
            ui->objectTypeLE->setText(sed->specialType());
            ui->objectPTE->setPlainText(sed->interpretation());
        }

        checkInputs();
    });

    sed->exec();
}

void NewRegionSceneDialog::chooseOther(Region::ElementCategory category) {

    const auto * elementList { (category == Region::ElementCategory::Location) ? p_elementLists[0] :
                               ((category == Region::ElementCategory::Encounter) ? p_elementLists[1]
                               : p_elementLists[2])};

    auto coed { new ChooseRegionElementDialog { nullptr, elementList, m_region, category } };

    connect(coed, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        if (category == Region::ElementCategory::Location) {
            ui->otherLocationPB->setEnabled(false);
            ui->locationTypeLE->setText(p_elementLists[0]->item(coed->row())->text());
            m_chosenLocation = coed->row();
        } else if (category == Region::ElementCategory::Encounter) {
            ui->otherEncounterPB->setEnabled(false);
            ui->encounterTypeLE->setText(p_elementLists[1]->item(coed->row())->text());
            m_chosenEncounter = coed->row();
        } else if (category == Region::ElementCategory::Object) {
            ui->otherObjectPB->setEnabled(false);
            ui->objectTypeLE->setText(p_elementLists[2]->item(coed->row())->text());
            m_chosenObject = coed->row();
        }
    });

    coed->exec();
}
