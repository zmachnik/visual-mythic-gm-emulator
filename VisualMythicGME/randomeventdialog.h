#ifndef RANDOMEVENTDIALOG_H
#define RANDOMEVENTDIALOG_H

#include <QDialog>

namespace Ui {
class RandomEventDialog;
}

class RandomEventDialog : public QDialog {

    Q_OBJECT

public:

    explicit RandomEventDialog(QWidget *parent = nullptr, bool adventureActive = false, bool canCancelAfterRoll = true);
    virtual ~RandomEventDialog() override;

    QString randomEvent() const;

protected slots:

    virtual void accept() override;

private:

    Ui::RandomEventDialog *ui;

    QString m_randomEvent;
    bool m_adventureActive = false;
    bool m_canCancelAfterRoll = true;

private slots:

    void rollRandomEvent();
};

#endif // RANDOMEVENTDIALOG_H
