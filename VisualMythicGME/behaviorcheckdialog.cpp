#include "behaviorcheckdialog.h"
#include "ui_behaviorcheckdialog.h"

#include <QIcon>

#include "lists.h"
#include "dice.h"
#include "utils.h"
#include "behaviorchecktables.h"
#include "randomdescriptiondialog.h"
#include "randomeventdialog.h"

BehaviorCheckDialog::BehaviorCheckDialog(QWidget * parent,
                                         bool descriptorsEdited,
                                         QString currentNPC) :
    QDialog(parent),
    ui { new Ui::BehaviorCheckDialog } {

    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setModal(true);
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setWindowIcon(QIcon(":visual/icon.ico"));

    Utils::maximizeOnPhone(this);

    //Utils::scrollWithSwipe(ui->behaviorCheckSA);
    Utils::scrollWithSwipe(ui->NPCActionsSA);
    Utils::scrollWithSwipe(ui->inspirationPTE);
    Utils::scrollWithSwipe(ui->newDescriptorInspirationPTE);

    ui->NPCCB->clear();

    for (const auto & NPC : Lists::NPCs()) ui->NPCCB->addItem(NPC);

    if (descriptorsEdited && !currentNPC.isEmpty()) {
        if (!currentNPC.isEmpty()) ui->NPCCB->setCurrentText(currentNPC);
        setDispositionBar(ui->NPCCB->currentText());
    } else {
        ui->dispositionPrB->setVisible(false);
    }

    loadNPCDetails(ui->NPCCB->currentText());
}

BehaviorCheckDialog::~BehaviorCheckDialog() {

    delete ui;
}

bool BehaviorCheckDialog::NPCDetailsChangeRequested() const {

    return m_NPCDetailsChangeRequested;
}

QString BehaviorCheckDialog::currentNPC() const {

    return m_currentNPC;
}

QString BehaviorCheckDialog::behavior() const {

    return m_behavior;
}

void BehaviorCheckDialog::accept() {

    const auto & NPC { ui->NPCCB->currentText() };
    const auto descriptors { Lists::descriptors(NPC) };

    m_behavior += "BEHAVIOR CHECK FOR " + NPC;
    m_behavior += "\n» THEME: " + ui->themeInfoLE->text();
    m_behavior += "\n» PREVIOUS ACTION: " + ui->actionInfoLE->text();
    m_behavior += "\n» IDENTITY: " + ui->identityLE->text();
    m_behavior += "\n» PERSONALITY: " + ui->personalityLE->text();
    m_behavior += "\n» ACTIVITY: " + ui->activityLE->text();

    m_behavior += "\n» TABLE 1: " + ui->table1ResultLE->text();

    const auto & table2Result { ui->table2ResultLE->text() };
    if (!table2Result.isEmpty()) m_behavior += "\n» TABLE 2: " + table2Result;

    const auto & inspiration { ui->inspirationPTE->toPlainText() };
    if (!inspiration.isEmpty()) m_behavior += "\n» INSPIRATION: " + inspiration;

    const auto & character { ui->characterCB->currentText() };
    if (!character.isEmpty()) m_behavior += "\n» CHARACTER: " + character;

    m_behavior += "\n» NPC ACTION: " + ui->interpretationLE->text();

    if (!table2Result.isEmpty()) {

        auto dispositionChange {
            ui->decreaseDispositionPB->isChecked() ? -2 : (ui->increaseDispositionPB->isChecked() ? +2 : 0) };

        if (dispositionChange)
            m_modifiedDescriptors->setDispositionBaseValue(descriptors->dispositionBaseValue() + dispositionChange);
    }

    if (m_modifiedDescriptors) {

        const auto & newValue { ui->newDescriptorLE->text() };

        if (ui->descriptorTypeLE->text() == "IDENTITY") {

            if (m_modifiedDescriptors->identity().primary.isEmpty()) {
                m_modifiedDescriptors->setIdentity(
                            NPCDescriptors::Descriptor {newValue, "", 1});
            } else {
                m_modifiedDescriptors->setIdentity(
                            NPCDescriptors::Descriptor {m_modifiedDescriptors->identity().primary, newValue, 2});
            }

            m_behavior += "\n» NEW ACTIVE IDENTITY: " + newValue;

        } else if (ui->descriptorTypeLE->text() == "PERSONALITY") {

            if (m_modifiedDescriptors->personality().primary.isEmpty()) {
                m_modifiedDescriptors->setPersonality(
                            NPCDescriptors::Descriptor {newValue, "", 1});
            } else {
                m_modifiedDescriptors->setPersonality(
                            NPCDescriptors::Descriptor {m_modifiedDescriptors->personality().primary, newValue, 2});
            }

            if (!ui->newDescriptorInspirationPTE->toPlainText().isEmpty())
                m_behavior += "\n» NEW PERSONALITY INSPIRATION: " + ui->newDescriptorInspirationPTE->toPlainText();

            m_behavior += "\n» NEW ACTIVE PERSONALITY: " + newValue;

        } else if (ui->descriptorTypeLE->text() == "ACTIVITY") {

            if (m_modifiedDescriptors->activity().primary.isEmpty()) {
                m_modifiedDescriptors->setActivity(
                            NPCDescriptors::Descriptor {newValue, "", 1});
            } else {
                m_modifiedDescriptors->setActivity(
                            NPCDescriptors::Descriptor {m_modifiedDescriptors->activity().primary, newValue, 2});
            }

            if (!ui->newDescriptorInspirationPTE->toPlainText().isEmpty())
                m_behavior += "\n» NEW ACTIVITY INSPIRATION: " + ui->newDescriptorInspirationPTE->toPlainText();

            m_behavior += "\n» NEW ACTIVE ACTIVITY: " + newValue;

        } else if (ui->descriptorTypeLE->text() == "THEME") {

            m_modifiedDescriptors->setTheme(newValue);

            m_behavior += "\n» NEW THEME: " + newValue;
        }

        Lists::setNPCDescriptors(NPC, *m_modifiedDescriptors);

        m_behavior += "\n» " + NPC + " DISPOSITION: " + QString::number(descriptors->dispositionScore())
                + " (" + descriptors->dispositionText(descriptors->totalDisposition()) + ")";
    }

    QDialog::accept();
}

void BehaviorCheckDialog::reject() {

    QDialog::reject();
}

void BehaviorCheckDialog::setupCharacterList() {

    ui->charactersGB->setEnabled(true);
    ui->randomCharacterPB->setEnabled(true);

    for (const auto & character : Lists::playerCharacters()) ui->characterCB->addItem(character);
}

void BehaviorCheckDialog::setDispositionBar(const QString &NPC) {

    const auto descriptors { Lists::descriptors(NPC) };
    if (!descriptors) {
        ui->dispositionPrB->setVisible(false);
        return;
    }

    ui->dispositionPrB->setValue(descriptors->dispositionScore());
    ui->dispositionPrB->setFormat("DISPOSITION %v: " + NPCDescriptors::dispositionText(descriptors->totalDisposition()));
    ui->dispositionPrB->setVisible(true);
}

void BehaviorCheckDialog::loadNPCDetails(const QString & NPC) {

    auto descriptors { Lists::descriptors(NPC) };

    if (!descriptors) {
        NPCDescriptors emptyDescriptors;
        Lists::setNPCDescriptors(NPC, emptyDescriptors);
        descriptors = Lists::descriptors(NPC);
    }

    if (descriptors->interactionOngoing()) {
        ui->continueInteractionPB->setEnabled(true);
        setDispositionBar(NPC);
    } else {
        ui->continueInteractionPB->setEnabled(false);
        ui->dispositionPrB->setVisible(false);
    }

    ui->themeInfoLE->setText(descriptors->theme(true));
    ui->actionInfoLE->setText(descriptors->action(true));
    ui->identityLE->setText(descriptors->activeIdentity(true));
    ui->personalityLE->setText(descriptors->activePersonality(true));
    ui->activityLE->setText(descriptors->activeActivity(true));

    ui->identityDispModLE->setText(
                (descriptors->identity().active) ? (descriptors->identityIncDisp() ? "+2" : "-2") : "0");

    ui->personalityDispModLE->setText(
                (descriptors->personality().active) ? (descriptors->personalityIncDisp() ? "+2" : "-2") : "0");

    ui->activityDispModLE->setText(
                (descriptors->activity().active) ? (descriptors->activityIncDisp() ? "+2" : "-2") : "0");

}

void BehaviorCheckDialog::newInteraction() {

    Dice d10 { 10, true };

    const auto & NPC { ui->NPCCB->currentText() };

    const auto descriptors { Lists::descriptors(NPC) };
    descriptors->setDispositionBaseValue(d10.getInt() + d10.getInt());
    descriptors->setInteractionOngoing(true);

    setDispositionBar(NPC);

    ui->NPCCB->setEnabled(false);
    ui->newInteractionPB->setEnabled(false);
    ui->continueInteractionPB->setEnabled(false);
    ui->changeDetailsPB->setEnabled(true);

    ui->checkBehaviorPB->setEnabled(true);
}

void BehaviorCheckDialog::continueInteraction() {

    setDispositionBar(ui->NPCCB->currentText());

    ui->NPCCB->setEnabled(false);
    ui->newInteractionPB->setEnabled(false);
    ui->continueInteractionPB->setEnabled(false);
    ui->changeDetailsPB->setEnabled(true);

    ui->checkBehaviorPB->setEnabled(true);
}

void BehaviorCheckDialog::updateDisposition() {


}

void BehaviorCheckDialog::checkBehavior() {

    static Dice d3{3, true};

    const auto & NPC { ui->NPCCB->currentText() };
    auto descriptors { Lists::descriptors(NPC) };

    if (!descriptors) {
        NPCDescriptors emptyDescriptors;
        Lists::setNPCDescriptors(NPC, emptyDescriptors);
        descriptors = Lists::descriptors(NPC);
    }

    m_modifiedDescriptors = std::make_shared<NPCDescriptors>(*descriptors);

    auto result {
        BehaviorCheckTables::behaviorCheckTableResult(
                    NPCDescriptors::dispositionModifier(descriptors->totalDisposition())) };

    const auto descriptorChange { [&]() {

        ui->changeDescriptorGB->setEnabled(true);

        ui->inspirationL->setEnabled(true);
        ui->newDescriptorLE->setEnabled(true);

        const auto value { d3.get() };

        if (value == 1) {

            ui->descriptorTypeLE->setText("IDENTITY");
            ui->descriptorTypeLE->setEnabled(true);

            if (descriptors->identity().secondary.isEmpty()) {
                ui->newDescriptorLE->setReadOnly(false);
            } else {
                ui->newDescriptorL->setText("SWITCHING TO PREVIOUS");
                if (descriptors->identity().active == 2) m_modifiedDescriptors->setActiveIdentity(1);
                else m_modifiedDescriptors->setActiveIdentity(2);
                ui->newDescriptorLE->setReadOnly(true);
                ui->newDescriptorLE->setText(m_modifiedDescriptors->activeIdentity());
            }

        } else if (value == 2) {

            ui->descriptorTypeLE->setText("PERSONALITY");
            ui->descriptorTypeLE->setEnabled(true);

            if (descriptors->personality().secondary.isEmpty()) {
                ui->descriptorInspirationGB->setEnabled(true);
                ui->rollDescriptionForDescriptorPB->setEnabled(true);
                ui->rollActionForDescriptorPB->setEnabled(false);
                ui->newDescriptorLE->setReadOnly(false);
            } else {
                ui->newDescriptorL->setText("SWITCHING TO PREVIOUS");
                if (descriptors->personality().active == 2) m_modifiedDescriptors->setActivePersonality(1);
                else m_modifiedDescriptors->setActivePersonality(2);
                ui->newDescriptorLE->setReadOnly(true);
                ui->newDescriptorLE->setText(m_modifiedDescriptors->activePersonality());
            }

        } else if (value == 3) {

            ui->descriptorTypeLE->setText("ACTIVITY");
            ui->descriptorTypeLE->setEnabled(true);

            if (descriptors->activity().secondary.isEmpty()) {
                ui->descriptorInspirationGB->setEnabled(true);
                ui->rollDescriptionForDescriptorPB->setEnabled(false);
                ui->rollActionForDescriptorPB->setEnabled(true);
                ui->newDescriptorLE->setReadOnly(false);
            } else {
                ui->newDescriptorL->setText("SWITCHING TO PREVIOUS");
                if (descriptors->activity().active == 2) m_modifiedDescriptors->setActiveActivity(1);
                else m_modifiedDescriptors->setActiveIdentity(2);
                ui->newDescriptorLE->setReadOnly(true);
                ui->newDescriptorLE->setText(m_modifiedDescriptors->activeActivity());
            }

        }
    }};

    ui->changeDetailsPB->setEnabled(false);
    ui->checkBehaviorPB->setEnabled(false);
    ui->resultGB->setEnabled(true);
    ui->changeDescriptorGB->setEnabled(false);
    ui->modifyDispositionGB->setEnabled(false);

    ui->table1ResultLE->setText(BehaviorCheckTables::NPCAction1Text(result.table1Result));

    if (result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinuesPlus2)
        m_modifiedDescriptors->setDispositionBaseValue(descriptors->dispositionBaseValue() + 2);
    else if (result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinuesMinus2)
        m_modifiedDescriptors->setDispositionBaseValue(descriptors->dispositionBaseValue() - 2);

    if (descriptors->action().isEmpty()) {
        if (result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinues
                || result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinuesPlus2
                || result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinuesMinus2) {

            result.table1Result = BehaviorCheckTables::NPCAction1::ThemeAction;
            ui->table1ResultLE->setText(ui->table1ResultLE->text() + " » "
                                        + BehaviorCheckTables::NPCAction1Text(result.table1Result));
        }
    }

    if (result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinues
            || result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinuesPlus2
            || result.table1Result == BehaviorCheckTables::NPCAction1::NPCContinuesMinus2 ) {

        ui->interpretationLE->setText(ui->actionInfoLE->text());
    }

    ui->rollDescriptionPB->setEnabled(false);
    ui->rollActionPB->setEnabled(false);
    ui->charactersGB->setEnabled(false);

    if (result.table2Result) {
        const auto & table2Result { *result.table2Result };
        ui->table2ResultL->setEnabled(true);
        ui->table2ResultLE->setEnabled(true);
        ui->table2ResultLE->setText(BehaviorCheckTables::NPCAction2Text(table2Result));
        ui->inspirationL->setEnabled(true);
        ui->descriptorInspirationGB->setEnabled(false);

        switch (table2Result) {
        case BehaviorCheckTables::NPCAction2::TalksOrExposition:
            ui->inspirationL->setEnabled(true);
            ui->rollDescriptionPB->setEnabled(true);
            break;
        case BehaviorCheckTables::NPCAction2::PerformsAnAmbiguousAction:
        case BehaviorCheckTables::NPCAction2::SeeksToEndTheEncounter:
        case BehaviorCheckTables::NPCAction2::ActosOutOfSelfInterest:
            ui->inspirationL->setEnabled(true);
            ui->rollActionPB->setEnabled(true);
            break;
        case BehaviorCheckTables::NPCAction2::ActsOutOfPCInterest:
            ui->inspirationL->setEnabled(true);
            ui->rollDescriptionPB->setEnabled(true);
            ui->rollActionPB->setEnabled(true);
            setupCharacterList();
            break;
        case BehaviorCheckTables::NPCAction2::ChangesTheTheme:
            ui->changeDescriptorGB->setEnabled(true);
            ui->descriptorTypeLE->setText("THEME");
            ui->descriptorTypeLE->setEnabled(true);
            ui->inspirationL->setEnabled(true);
            ui->rollActionPB->setEnabled(true);
            break;
        case BehaviorCheckTables::NPCAction2::GivesSomething:
        case BehaviorCheckTables::NPCAction2::TakesSomething:
            ui->inspirationL->setEnabled(true);
            ui->rollDescriptionPB->setEnabled(true);
            setupCharacterList();
            break;
        case BehaviorCheckTables::NPCAction2::CausesHarm:
            ui->inspirationL->setEnabled(true);
            ui->rollActionPB->setEnabled(true);
            break;
        case BehaviorCheckTables::NPCAction2::ChangesDescriptor:
            descriptorChange();
        }

    } else {
        ui->table2ResultL->setEnabled(false);
        ui->table2ResultLE->setEnabled(false);
        ui->inspirationL->setEnabled(false);
    }

}

void BehaviorCheckDialog::changeNPCDetails()
{
    m_NPCDetailsChangeRequested = true;
    m_currentNPC = ui->NPCCB->currentText();
    QDialog::accept();
}

void BehaviorCheckDialog::rollDescription() {

    auto rdd { new RandomDescriptionDialog { nullptr, true } };

    connect(rdd, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;
        ui->rollDescriptionPB->setEnabled(false);
        ui->rollActionPB->setEnabled(false);

        ui->inspirationPTE->setPlainText(rdd->randomDescription());
        ui->inspirationPTE->setEnabled(true);
    });

    rdd->exec();
}

void BehaviorCheckDialog::rollDescriptionForDescriptor() {

    auto rdd { new RandomDescriptionDialog { nullptr, true } };

    connect(rdd, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        ui->rollDescriptionForDescriptorPB->setEnabled(false);
        ui->rollActionForDescriptorPB->setEnabled(false);

        ui->newDescriptorInspirationPTE->setPlainText(rdd->randomDescription());
        ui->newDescriptorInspirationPTE->setEnabled(true);
    });

    rdd->exec();
}

void BehaviorCheckDialog::rollAction() {

    auto red { new RandomEventDialog { nullptr, true } };

    connect(red, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        ui->rollDescriptionPB->setEnabled(false);
        ui->rollActionPB->setEnabled(false);

        ui->inspirationPTE->setPlainText(red->randomEvent());
        ui->inspirationPTE->setEnabled(true);
    });

    red->exec();
}

void BehaviorCheckDialog::rollActionForDescriptor() {

    auto red { new RandomEventDialog { nullptr, true } };

    connect(red, &QDialog::finished, this, [=](int dc){

        if (dc == QDialog::Rejected) return;

        ui->rollDescriptionForDescriptorPB->setEnabled(false);
        ui->rollActionForDescriptorPB->setEnabled(false);

        ui->newDescriptorInspirationPTE->setPlainText(red->randomEvent());
        ui->newDescriptorInspirationPTE->setEnabled(true);
    });

    red->exec();
}

void BehaviorCheckDialog::randomCharacter() {

    Dice d { ui->characterCB->count() };
    ui->characterCB->setCurrentIndex(d.getInt());

    ui->charactersGB->setEnabled(false);
}

void BehaviorCheckDialog::checkInterpretation() { 

    ui->acceptPB->setEnabled(false);
    ui->modifyDispositionGB->setEnabled(false);

    if (!ui->interpretationLE->text().isEmpty()) {
        if (ui->table2ResultLE->text().isEmpty()) {
            ui->acceptPB->setEnabled(true);
        } else if (!ui->newDescriptorLE->isEnabled() || !ui->newDescriptorLE->text().isEmpty()) {
            ui->acceptPB->setEnabled(true);
            ui->modifyDispositionGB->setEnabled(true);
        }
    }

}
