#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>
#include <QListWidget>
#include <QJsonObject>
#include <QJsonDocument>

#include "region.h"
#include "utils.h"

namespace Ui {
class MainDialog;
}

class MainDialog : public QDialog {

    Q_OBJECT

public:

    explicit MainDialog(QWidget *parent = nullptr);
    virtual ~MainDialog() override;

protected:

    virtual void closeEvent(QCloseEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;

private:

    const QString m_buttonEmptyLabel { "<SET>" };
    QJsonDocument m_adventureSave;
    QString m_adventureTextExport;

    enum class NPCDescriptor {
        Theme,
        Action,
        PrimaryIdentity,
        SecondaryIdentity,
        PrimaryPersonality,
        SecondaryPersonality,
        PrimaryActivity,
        SecondaryActivity
    };

    Ui::MainDialog * ui;

    void writeSave(QJsonObject & adventure);
    void loadSave(const QJsonObject &adventure);

    void appendStory(const QString & text);

    void addRegionElement(Region::ElementCategory category);
    void removeRegionElement(Region::ElementCategory category);
    void moveRegionElement(Region::ElementCategory category, bool up);
    void updateRegions();
    void updateElements();

    void setNPCDescriptor(NPCDescriptor descriptor);
    void setNPCDescriptorActive(NPCDescriptor descriptor);
    void updateSecondaryNPCDescriptorControls();

    void clearAll();

    void hideAndShowFeatures();

    QString replaceEmpty(QString str = "");

    QListWidget * elementWidget(Region::ElementCategory category);

private slots:

    void setAdventureActive(bool active);
    void newAdventure();
    void renameAdventure();
    void loadAdventure();
    void saveAdventure();
    void changeRPGSystem();
    void exportStoryTemplate();

    void showSettings();

    void nextScene();
    void renameScene();

    void askFateChart();
    void detailCheck();
    void behaviorCheck(bool descriptorsEdited = false);
    void statisticCheck();
    void eventCheck();
    void RPGSystemCheck();
    void createTweenStory();
    void generateCharacterBackstory();
    void saveCharacterBackstory();

    void randomDescription();
    void randomEvent();

    void dice();

    void quit();

    void addNPC();
    void renameNPC();
    void removeNPC();
    void addThread();
    void renameThread();
    void removeThread();
    void addPlayerCharacter();
    void renamePlayerCharacter();
    void removePlayerCharacter();
    void addPersonalNPC();
    void renamePersonalNPC();
    void removePersonalNPC();
    void addPersonalThread();
    void renamePersonalThread();
    void removePersonalThread();

    void addRegion();
    void renameRegion();
    void removeRegion();
    void newRegionScene();
    void leaveRegion();
    void updateRegionButtons();
    void updateRegionElementUI();
    void updateRegionSceneUI();
    void updateMapUI(Region * region);
    void mapToStory();
    void clearCurrentSceneUI();
    void toggleLocationConnection(QString regionName, const QString & from, const QString & to);

    void updateRemoveButtons(); // TODO: rename this
    void updatePersonalNPCsAndThreads(const QString & player);
    void updateChaosFactorDisplay();
    void updateNPCDescriptorCheckboxes();

    void updateDispositionModifiers(bool writeDescriptor = true);
    void changeBaseDisposition(int value);

    void updateDispositionDisplay();

    void refreshTab(int tabIndex);

    void fillNPCDetails(int index = -1);
    void loadNPCDetails(QString NPC = {});
    void saveNPCDetails();

    void discardNPCDetails();
    void backToBehaviorCheck();

    void tabChanged(int);
};

#endif // MAINDIALOG_H
